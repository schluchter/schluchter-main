import tkinter as tk

window = tk.Tk()
window.title("Welcome to LikeGeeks app")
lbl = tk.Label(window, text="Hello")
#lbl = tk.Label(window, text="Hello", font=("Arial Bold", 50))
lbl.grid(column=0, row=0)
window.geometry('350x200')

# must be defined before button, since 'clicked' is used in Button
def clicked():
    lbl.configure(text="Button was clicked !!")

btn = tk.Button(window, text="Click Me", command=clicked)
#btn = tk.Button(window, text="Click Me", bg="orange", fg="red")
btn.grid(column=1, row=0)

window.mainloop()
