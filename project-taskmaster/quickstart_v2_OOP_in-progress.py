# Google API libs
from __future__ import print_function # I think this import lets you use print in Py3 form instead of Py2 :O but not sure
from googleapiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools
# Database and file operation libs
import pandas as pd
import os, csv
# Set directories
cwd = os.getcwd()
perCreds = cwd+os.sep+'project-taskmaster'+os.sep+'personal-creds'+os.sep
ceoCreds = cwd+os.sep+'project-taskmaster'+os.sep+'ceo-creds'+os.sep

# Set up the Tasks API
class setup_API():
    def __init__(self, changes, account):
        # set the scope
        if changes == True:
            self.SCOPES = 'https://www.googleapis.com/auth/tasks'
            print("Changes to tasks have been allowed: YES")
        else:
            self.SCOPES = 'https://www.googleapis.com/auth/tasks.readonly'
            print("Changes allowed: NO")
        # get credentials
        self.store = file.Storage(account+'credentials.json')
        self.creds = self.store.get()
        if not self.creds or self.creds.invalid:
            self.flow = client.flow_from_clientsecrets(account+'client_secret.json', SCOPES)
            self.creds = tools.run_flow(flow, store)
        self.service = build('tasks', 'v1', http=self.creds.authorize(Http()))
# Call the Tasks API and print the current lists

# Main program; only run if called locally
def main():
    # Set user options (debug only)
    allow_changes = False
    account_path = ceoCreds
    print("Account path: "+account_path)
    # Set up the Tasks API
    setupAPI = setup_API(allow_changes, account_path)
    # Call the Tasks API and print the current lists
    # get all the lists
    pandalist = []
    maintasklist = setupAPI.service.tasklists().list(maxResults=30).execute()
    mytasklists = maintasklist.get('items', [])
    if not mytasklists:
        print('No task lists found.')
    else:
        #print('Task lists:')
        for thislist in mytasklists:
            pandalist.append(thislist)
            print(thislist['title'])
            #print('{0} ({1})'.format("LIST: ", thislist['title']))
            # get all the tasks
            thesetasks = setupAPI.service.tasks().list(tasklist=thislist['id']).execute()
            try:
                for task in thesetasks['items']:
                    pandalist.append(task)
                    print("    "+task['title'])
                    #try: print("    Parent: "+task['parent'])
                    #except: print('    Parent: None')
            except: print("    [list is empty]") #pass #
    # Create a Pandas dataframe from the data.
    df = pd.DataFrame(pandalist)
    # Write out the pandalist to a CSV file
    #DISABLED IN FAVOR OF EXCEL OUTPUT: pd.write_csv(cwd+os.sep+'data_output.csv')
    # Create a Pandas Excel writer using XlsxWriter as the engine.
    writer = pd.ExcelWriter(cwd+os.sep+'pandalist_export.xlsx', engine='xlsxwriter')
    #writer = pd.ExcelWriter('pandas_simple.xlsx', engine='xlsxwriter')
    # Convert the dataframe to an XlsxWriter Excel object.
    df.to_excel(writer, sheet_name='Sheet1')
    # Close the Pandas Excel writer and output the Excel file.
    try:
        writer.save()
    except: print("Error saving file. Is it already open? Was the directory moved?")
    #  DEBUGGING ONLY
    #import sys
    #print(sys.getsizeof(df))
    #print(df)

# Execute main program
if __name__ == '__main__':
    main()
