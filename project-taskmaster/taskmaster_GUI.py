#!/usr/bin/python3
import tkinter as tk
# GUI classes
class MainWindow(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)
        # Debug
        #print("Class: ListWindow")
        #print("self: "+str(self))
        #print("parent: "+str(parent))
        parent.columnconfigure(0, weight=1)
        parent.rowconfigure(0, weight=1)
        # Placement and creation
        self.grid()
        # Make the button bar
        buttonbar = ButtonBar(self)
        buttonbar.grid(row=1, column=1, sticky=tk.E+tk.W+tk.N+tk.S)
        self.grid_columnconfigure(1, weight=1)
        # Make the second section, with the windows and boxes
        submainwindow = SubMainWindow(self)
        submainwindow.grid(row=2, column=1, sticky=tk.E+tk.W+tk.N+tk.S)
        self.grid_rowconfigure(2, minsize=300, weight=1)
        # null space 0
        null_space0 = tk.Frame(self) # , bg="red")
        null_space0.grid(row=0, column=0, columnspan=3, sticky=tk.E+tk.W+tk.N+tk.S)
        self.grid_rowconfigure(0, minsize=10)
        # null space 1
        null_space1 = tk.Frame(self) # , bg="red")
        null_space1.grid(row=0, rowspan=3, column=0, sticky=tk.E+tk.W+tk.N+tk.S)
        self.grid_columnconfigure(0, minsize=10)
        # null space 2
        null_space2 = tk.Frame(self) # , bg="red")
        null_space2.grid(row=0, rowspan=3, column=2, sticky=tk.E+tk.W+tk.N+tk.S)
        self.grid_columnconfigure(2, minsize=10)
        # null space 3
        null_space3 = tk.Frame(self) # , bg="red")
        null_space3.grid(row=3, column=0, columnspan=3, sticky=tk.E+tk.W+tk.N+tk.S)
        self.grid_rowconfigure(3, minsize=10)
        # Functions
        menubar = MenuBar(self)
        parent.config(menu=menubar)
class MenuBar(tk.Menu):
    def __init__(self, parent):
        tk.Menu.__init__(self, parent)
        # funtions:
        fileMenu = tk.Menu(self, tearoff=False)
        helpMenu = tk.Menu(self, tearoff=False)

        self.add_cascade(label="File",underline=0, menu=fileMenu)
        fileMenu.add_command(label='Save')
        fileMenu.add_command(label='Save Local')
        fileMenu.add_command(label='Sync')
        fileMenu.add_separator()
        fileMenu.add_command(label="Exit", underline=1, command=self.quit)

        self.add_cascade(label="Help",underline=1, menu=helpMenu)
        helpMenu.add_command(label='Get Help', underline=0, command=self.get_help)
    def quit(self):
        tk.sys.exit(0)
    def get_help(self):
        print("lol, you wish...")
class ButtonBar(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)
        self.grid()
        self.grid_rowconfigure(0, weight=1)
        # Button bar buttons
        #buttonundo=tk.Button(root,text="Undo", bg = 'red')
        buttonundo=tk.Button(self,text="Undo")
        buttonredo=tk.Button(self,text="Redo")
        #buttonaddlist=tk.Button(self,text="Add\nList")
        #buttonchangelist=tk.Button(self,text="Change\nList")
        #buttondeletelist=tk.Button(self,text="Delete\nList")
        #buttonaddtask=tk.Button(self,text="Add\nTask")
        #buttonchangetask=tk.Button(self,text="Change\nTask")
        #buttondeletetask=tk.Button(self,text="Delete\nTask")
        buttoncompletetask=tk.Button(self,text="Complete\nTask")
        buttonshowcomplete=tk.Button(self,text="Show\nComplete")
        buttonshowwaiting=tk.Button(self,text="Show\nWaiting")
        buttontasks=tk.Button(self,text="")
        buttonsync=tk.Button(self,text="Sync")
        buttonundo.grid(row=0, column=0, sticky=tk.N+tk.S)
        buttonredo.grid(row=0, column=1, sticky=tk.N+tk.S)
        #buttonaddlist.grid(row=0, column=3, sticky=tk.N+tk.S)
        #buttonchangelist.grid(row=0, column=2, sticky=tk.N+tk.S)
        #buttondeletelist.grid(row=0, column=3, sticky=tk.N+tk.S)
        #buttonaddtask.grid(row=0, column=4, sticky=tk.N+tk.S)
        #buttonchangetask.grid(row=0, column=5, sticky=tk.N+tk.S)
        #buttondeletetask.grid(row=0, column=6, sticky=tk.N+tk.S)
        buttoncompletetask.grid(row=0, column=7, sticky=tk.N+tk.S)
        buttonshowcomplete.grid(row=0, column=8, sticky=tk.N+tk.S)
        buttonshowwaiting.grid(row=0, column=9, sticky=tk.N+tk.S)
        # null space 1
        null_space1 = tk.Frame(self)
        null_space1.grid(row=0, column=10, sticky=tk.E+tk.W)
        self.grid_columnconfigure(10, weight=3)
        # null space 2
        null_space1 = tk.Frame(self)
        null_space1.grid(row=0, column=11, sticky=tk.E+tk.W)
        self.grid_columnconfigure(11, weight=3)
        buttonsync.grid(row=0, column=12)
class SubMainWindow(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)
        # create list window
        self.grid()
        listwindow = ListWindow(self)
        listwindow.grid(row=0, column=0, sticky=tk.E+tk.W+tk.N+tk.S)
        self.grid_columnconfigure(0, weight=1, minsize=200)
        # create task window
        taskwindow = TaskWindow(self)
        taskwindow.grid(row=0, column=1, sticky=tk.E+tk.W+tk.N+tk.S)
        self.grid_columnconfigure(1, weight=5, minsize=300)
        self.grid_rowconfigure(0, weight=3)
        # create waiting window
        waitingwindow = WaitingWindow(self)
        waitingwindow.grid(row=1, column=0, sticky=tk.E+tk.W+tk.N+tk.S)
        # create note window
        notewindow = NoteWindow(self)
        notewindow.grid(row=1, column=1, sticky=tk.E+tk.W+tk.N+tk.S)
        self.grid_rowconfigure(1, weight=1)
class ListWindow(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)
        # Placement
        self.grid()
        label = tk.Label(self, text="Lists")
        label.grid(row=0, column=0, sticky=tk.W+tk.S)
        # create the buttons
        buttonremovelist = tk.Button(self, text="-")
        buttonremovelist.grid(row=0, column=1) #, sticky=tk.E)
        buttonaddlist = tk.Button(self, text="+")
        buttonaddlist.grid(row=0, column=2) #, sticky=tk.E)
        buttonchangelist = tk.Button(self, text="=")
        buttonchangelist.grid(row=0, column=3) #, sticky=tk.E)
        # create the lists listbox
        listbox = tk.Listbox(self)
        listbox.grid(row=1, column=0, columnspan=5, sticky=tk.W+tk.E+tk.N+tk.S)
        self.grid_rowconfigure(1, weight=1)
        self.grid_columnconfigure(0, weight=1, minsize=30)
        self.grid_columnconfigure(4, weight=1)
        # Functions
        listbox.insert(tk.END, "List Window")
        for item in ["one", "two", "three", "four"]:
            listbox.insert(tk.END, item)
class TaskWindow(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)
        # Placement
        self.grid()
        label = tk.Label(self, text="Projects and Actions")
        label.grid(row=0, column=0, sticky=tk.W+tk.S)
        self.grid_columnconfigure(0, minsize=150)
        self.grid_columnconfigure(1, weight=1) #, minsize=150)
        # buttons
        buttondeletetask=tk.Button(self,text="-")
        buttondeletetask.grid(row=0, column=2, sticky=tk.W)
        buttonaddtask=tk.Button(self,text="+")
        buttonaddtask.grid(row=0, column=3, sticky=tk.W)
        buttonchangetask=tk.Button(self,text="=")
        buttonchangetask.grid(row=0, column=4, sticky=tk.W)
        self.grid_columnconfigure(5, weight=1) #, minsize=10)
        buttonprojectsactions = tk.Button(self, text="Projects Actions")
        buttonprojectsactions.grid(row=0, column=6, sticky=tk.W)
        buttonnextactions = tk.Button(self, text="Next Actions")
        buttonnextactions.grid(row=0, column=7, sticky=tk.W)
        buttonprojectslist = tk.Button(self, text="Projects List")
        buttonprojectslist.grid(row=0, column=8, sticky=tk.W)
        self.grid_columnconfigure(9, weight=7) #, minsize=10)
        # create projects actions listbox
        palistbox = tk.Listbox(self)
        palistbox.grid(row=1, column=0, columnspan=10, sticky=tk.E+tk.W+tk.N+tk.S)
        self.grid_rowconfigure(1, weight=1)
        palistbox.insert(tk.END, "PA Window")
        for item in ["one", "two", "three", "four"]:
            palistbox.insert(tk.END, item)
    # Functions
    # def select_projects_actions
    # def select_next_actions
    # def select_projects_list
    #class PAWindow(tk.Frame):
class WaitingWindow(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)
        # Placement
        self.grid()
        label = tk.Label(self, text="Waiting on: ")
        label.grid(row=0, column=0, sticky=tk.W+tk.S)
        waitlistbox = tk.Listbox(self)
        waitlistbox.grid(row=1, column=0, sticky=tk.W+tk.E+tk.N+tk.S)
        self.grid_rowconfigure(1, weight=1)
        self.grid_columnconfigure(0, weight=1)
        # Functions
        waitlistbox.insert(tk.END, "Waiting Window")
        for item in ["one", "two", "three", "four"]:
            waitlistbox.insert(tk.END, item)
class NoteWindow(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)
        # Placement
        self.grid()
        label = tk.Label(self, text="Notes: ")
        label.grid(row=0, column=0, sticky=tk.W+tk.S)
        notelistbox = tk.Listbox(self)
        notelistbox.grid(row=1, column=0, sticky=tk.E+tk.W+tk.N+tk.S)
        self.grid_rowconfigure(1, weight=1)
        self.grid_columnconfigure(0, weight=1)
        notelistbox.insert(tk.END, "Note Window")
        for item in ["one", "two", "three", "four"]:
            notelistbox.insert(tk.END, item)
# Main program; only run if called locally
def main():
    root = tk.Tk()
    root.geometry("1280x768")
    root.title("Taskmaster - Version 0.15")
    MainWindow(root).grid(sticky=tk.E+tk.W+tk.N+tk.S)
    root.mainloop()
# Execute main program
if __name__ == '__main__':
    main()

"""
# Initial list of classes or defs:
=MainWindow
   =ButtonBar
     ...Undo
     ...Redo
     ...AddList
     ...ModifyList
     ...DeleteList
     ...AddTask
     ...ModifyTask
     ...DeleteTask
     ...CompleteTask
  =ListsWindow
      Import all lists into listbox from the google account; show "Importing data..." when doing so
      AddMetadata
      AddList
      RemoveList
      ModifyList
      Selfdestruct
  =TaskWindow
    PAWindow
      Import all Tasks into Task listbox from the google account; show "Importing data..." when doing so
      AddMetadata
      AddProjectAction
      RemoveProjectAction
      ModifyProjectAction
      Selfdestruct
    NextActionsWindow
    ProjectsListWindow
  =WaitingWindow
      Import all WaitingOns (and Processings, etc., optional) into Waiting listbox from the selected projectaction
      ApplyMetadata
      Selfdestruct
  =NoteWindow
     Import notes into Notes listbox from the selected projectaction
     Selfdestruct
  ImportAllData
     ImportAllData
     TranscribeToFile(csv)
     ReadFromFile(csv)
"""
