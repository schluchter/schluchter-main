"""
Shows basic usage of the Tasks API. Outputs the first 10 task lists.
"""
# Google API libs
# I think this import lets you use print in Py3 form instead of Py2 :O but not sure
from __future__ import print_function
from googleapiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools

# Database and file operation libs
import pandas as pd
import os, csv
cwd = os.getcwd()
perCreds = cwd+os.sep+'project-taskmaster'+os.sep+'personal-creds'+os.sep
ceoCreds = cwd+os.sep+'project-taskmaster'+os.sep+'ceo-creds'+os.sep

# Setup the Tasks API
# To allow changing of tasks, change the line below to '/.../auth/tasks'
SCOPES = 'https://www.googleapis.com/auth/tasks.readonly'
store = file.Storage(ceoCreds+'credentials.json')
creds = store.get()
if not creds or creds.invalid:
    flow = client.flow_from_clientsecrets(ceoCreds+'client_secret.json', SCOPES)
    creds = tools.run_flow(flow, store)
service = build('tasks', 'v1', http=creds.authorize(Http()))

"""
# ORIGINAL CODE:
# Call the Tasks API alt: use original data print schema
results = service.tasklists().list(maxResults=10).execute()
items = results.get('items', [])
if not items:
    print('No task lists found.')
else:
    print('Task lists:')
    for item in items:
        #print(item['title'])
        print('{0} ({1})'.format(item['title'], item['id']))
"""

pandalist = []
cwd = os.getcwd()

# Call the Tasks API
# Get all the lists
maintasklist = service.tasklists().list(maxResults=30).execute()
mytasklists = maintasklist.get('items', [])
if not mytasklists:
    print('No task lists found.')
else:
    #print('Task lists:')
    for thislist in mytasklists:
        pandalist.append(thislist)
        print(thislist['title'])
        #print('{0} ({1})'.format("LIST: ", thislist['title']))
        # Get all the tasks
        thesetasks = service.tasks().list(tasklist=thislist['id']).execute()
        try:
            for task in thesetasks['items']:
                pandalist.append(task)
                print("    "+task['title'])
                #try: print("    Parent: "+task['parent'])
                #except: print('    Parent: None')
        except: print("    [list is empty]") #pass #
df = pd.DataFrame(pandalist)
#pd.write_csv(cwd+os.sep+'data_output.csv')
writer = pd.ExcelWriter(cwd+os.sep+'pandalist_export.xlsx', engine='xlsxwriter')

# Create a Pandas dataframe from the data.
#df = pd.DataFrame({'Data': [10, 20, 30, 20, 15, 30, 45]})
# Create a Pandas Excel writer using XlsxWriter as the engine.
#writer = pd.ExcelWriter('pandas_simple.xlsx', engine='xlsxwriter')
# Convert the dataframe to an XlsxWriter Excel object.
df.to_excel(writer, sheet_name='Sheet1')
# Close the Pandas Excel writer and output the Excel file.
try:
    writer.save()
except: print("Error saving file. Is it already open? Was the directory moved?")
#  DEBUGGING ONLY
#import sys
#print(sys.getsizeof(df))


"""
def save_df_to_excel(df, path, file_name, sheet_name):
  writer = pd.ExcelWriter(path+file_name+'.xlsx', engine='xlsxwriter')
  df.to_excel(writer, sheet_name=sheet_name)
  writer.save()
"""




#print(df)
