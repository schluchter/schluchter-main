#!/usr/bin/python3
# Use Tkinter for python 2, tkinter for python 3
import tkinter as tk

"""
def quit(event=None):
    sys.exit()
root = tk.Tk()
label = tk.Label(root, text="Hello, world")
label.pack()
label.bind("<1>", quit)
root.mainloop()
"""

class MyWindow(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)
        label = tk.Label(self, text="Hello, world")
        label.pack()
        label.bind("<1>", self.quit)
    def quit(self, event=None):
        tk.sys.exit()

root = tk.Tk()
MyWindow(root).pack()
root.mainloop()
