# Google API libs
from __future__ import print_function # I think this import lets you use print in Py3 form instead of Py2 :O but not sure
from googleapiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools
# Database and file operation libs
import pandas as pd
import os, csv
# Set directories
cwd = os.getcwd()
perCreds = cwd+os.sep+'project-taskmaster'+os.sep+'personal-creds'+os.sep
ceoCreds = cwd+os.sep+'project-taskmaster'+os.sep+'ceo-creds'+os.sep
# Set up the Tasks API
class setup_API():
    def __init__(self, changes, account):
        # set the scope
        if changes == True:
            self.SCOPES = 'https://www.googleapis.com/auth/tasks'
            print("Changes to tasks have been allowed: YES")
        else:
            self.SCOPES = 'https://www.googleapis.com/auth/tasks.readonly'
            print("Changes allowed: NO")
        # get credentials
        self.store = file.Storage(account+'credentials.json')
        self.creds = self.store.get()
        if not self.creds or self.creds.invalid:
            self.flow = client.flow_from_clientsecrets(account+'client_secret.json', SCOPES)
            self.creds = tools.run_flow(flow, store)
        setup_API.service = build('tasks', 'v1', http=self.creds.authorize(Http()))
# Call the Tasks API and print the current lists
class gather_Tasks():
    def __init__(self):
        # get all the lists
        gather_Tasks.tasklist = []
        maintasklist = setup_API.service.tasklists().list(maxResults=30).execute()
        mytasklists = maintasklist.get('items', [])
        if not mytasklists:
            print('No task lists found.')
        else:
            #print('Task lists:')
            for thislist in mytasklists:
                gather_Tasks.tasklist.append(thislist)
                print(thislist['title'])
                #print('{0} ({1})'.format("LIST: ", thislist['title']))
                # get all the tasks
                thesetasks = setup_API.service.tasks().list(tasklist=thislist['id']).execute()
                try:
                    for task in thesetasks['items']:
                        gather_Tasks.tasklist.append(task)
                        print("    "+task['title'])
                        #try: print("    Parent: "+task['parent'])
                        #except: print('    Parent: None')
                except: print("    [list is empty]") #pass #
# Write the ouput to file
class write_output():
    def __init__(self):
        # Create a Pandas dataframe from the data.
        df = pd.DataFrame(gather_Tasks.tasklist)
        # Write out the tasklist to a CSV file
        #DISABLED IN FAVOR OF EXCEL OUTPUT: pd.write_csv(cwd+os.sep+'data_output.csv')
        # Create a Pandas Excel writer using XlsxWriter as the engine.
        writer = pd.ExcelWriter(cwd+os.sep+'Taskmaster_tasklist_export.xlsx', engine='xlsxwriter')
        #writer = pd.ExcelWriter('pandas_simple.xlsx', engine='xlsxwriter')
        # Convert the dataframe to an XlsxWriter Excel object.
        df.to_excel(writer, sheet_name='Sheet1')
        # Close the Pandas Excel writer and output the Excel file.
        try:
            writer.save()
        except: print("Error saving file. Is it already open? Was the directory moved?")
# EXPERIMENTAL CLASSES - START ============================================
class waitify():
    """
    IF: task/project has "WAITING ON:" or "WO:" in the title
    THEN:
    1. add tag <wo>[condition]</wo>, where [condition] is everything after the ":" (and remove all spaces before the rest of the text)
    2. add condition to list "Waiting On" if not already present
    SO THAT: can make a viewing list for all items with <wo> tag, and can form a hard list and add the [condition]s (along with their parent task and list, in the form [task]([parent list]) )
    """
    def __init__(self):
        print("'waitify' class is under development.")
class projectify():
    """
    IF: task is top-level (parent = false)
    OR task is mid-level (parent = true, child = true)
    THEN: check if task title starts with "PROJECT: " (with or without the space afterward) and if not then add prefix "PROJECT: "
    OR: check if task title starts with "SUB-PROJECT: " (with or without the space afterward) and if not then add prefix "SUB-PROJECT: "
    SO THAT: (inherently, this forces tasks to EITHER be auto-upgraded to "project" status, "sub-project" status, or kept as an action)
    """
    def __init__(self):
        print("'projectify' class is under development.")
    def __init__(self):
        print("'waitify' class is under development.")
        for fullTask in gather_Tasks.tasklist:
            #print(fullTask)
            print(fullTask['title'])
            if fullTask['kind'] == 'tasks#task':
                #print("The item above is a task.")
                #if [task is NOT in 'Inbox' list, and IS something...]
                parentChain = self.get_parent_chain([])
                print("   PARENT CHAIN: ")
                if not parentChain:
                    print("   Task has no parents")
                else: print(parentChain)
                #childChain = self.get_child_chain()

            #     search in title for string "WAITING ON: " | "WO: "
            #    if match is found

            elif fullTask['kind'] == 'tasks#taskList':
                print("Above is a list.")
                # leave lists alone!
            else: print("I honestly don't know what the thing above me is...")
    def get_parent_chain(self, chain):
        #print("Testing: 'get_parent_chain'")
        self.chain = chain
        #print("initial chain: ")
        #print(chain)
        chain.append('a')
        chain.append('b')
        chain.append('c')
        return(chain)
    def get_child_chain(self):
        print("Test: 'get_child_chain'")
# EXPERIMENTAL CLASSES - END ==============================================
# Main program; only run if called locally
def main():
    # Set user options (DEBUGGING ONLY)
    allow_changes = False
    account_path = ceoCreds
    print("Account path: "+account_path)
    # Set up the Tasks API
    setup_API(allow_changes, account_path)
    # Call the Tasks API and print the current lists
    gather_Tasks()
    # Write the ouput to file
    write_output()
    # EXPERIMENTAL CLASSES!
    projectify()
    waitify()
    #  DEBUGGING ONLY
    #import sys
    #print(sys.getsizeof(df))
    #print(df)
# Execute main program
if __name__ == '__main__':
    main()
