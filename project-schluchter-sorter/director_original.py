#!/usr/bin/python3
import os, copy

#=======================================================================
# Determines how many levels to go down
max_level = 3
# Write to file instead of terminal?
write_file = True


def gather_dirs():
    # Get the current working directory
    cwd = os.getcwd()

    # Set the rest of the variables
    dir_list = []
    remove_dir_list = []
    valid_dirs = []
    sepflag = False
    intchar1 = False
    intchar2 = False
    intchar3 = False
    keepflag = True

    # Form a list of all directories within cwd
    for root, dirs, files in os.walk(cwd):
        level = root.replace(cwd, '').count(os.sep)
        if 0 < level <= max_level:
            dir_list.append(root.replace(cwd, ''))

    # For every dir, make a note of which do NOT fit the "/aaa." form
    for entry in dir_list:
        #print(entry)
        intchar1 = False
        intchar2 = False
        intchar3 = False
        tempchar = 'x'
        for char in entry:
            #print(char)
            #test = input()
            #if char == '.':
                #print("got a period here")
            if char == '.' and (intchar1 == True or intchar2 == True or intchar3 == True):
                #print("This directory is valid: "+str(entry))
                #print(entry)
                keepflag = True
                intchar1 = False
                intchar2 = False
                intchar3 = False
            if sepflag == True or intchar1 == True or intchar2 == True:
                try:
                    tempchar = int(char)
                except ValueError:
                    #print("Not an int after the OS seperator!")
                    #print("But there is a number after the slash in: ")
                    #print(entry)
                    keepflag = False
                    intchar1 = False
                    intchar2 = False
                    intchar3 = False
                    #pass

                if isinstance(tempchar, int):
                    tempchar = 'x'
                    #print("os.sep followed by >= 1 int")
                    if intchar2 == True:
                        intchar3 = True
                    if intchar1 == True:
                        intchar2 = True
                    else: intchar1 = True
                    #print(intchar1)

            sepflag = False
            if char == os.sep:
                #print("Seperated here")
                sepflag = True
                keepflag = False
        if keepflag == False:
            #print("Invalid dir. Remove dir: ")
            #print(entry)
            # Make a note of which dirs to remove later
            # MUST remove later: currently using the dir (causes ordering problem)
            remove_dir_list.append(entry)
            keepflag == True

    # Remove all of the invalid dirs
    for thisdir in remove_dir_list:
        try:
            dir_list.remove(thisdir)
        except:
            pass

    # Clean up entries: remove everything to the left of the deepest directory
    for entry in dir_list:
        trimmer = entry.rfind(os.sep)
        entry = entry[trimmer:]
        # Remove the os.sep operator at the start of each dir entry
        entry = entry[1:]
        valid_dirs.append(entry)
        #print(entry)

    # Print all dirs, print the invalid dirs, and print the valid dirs
    #print("===================================================")
    #print(*dir_list, sep='\n')
    #print("===================================================")
    #print(*remove_dir_list, sep='\n')
    #print("===================================================")
    #print(*valid_dirs, sep='\n')

    # sort lists if needed
    sorted_dirs = sorted(valid_dirs)
    #print(*sorted_dirs, sep='\n')
    #for entry in sorted_dirs:
    #    print(entry)

    # Print all dirs, after cleaning, to file:
    if write_file == True:
        fileout = open('directory.txt','w')
        for adir in sorted_dirs:
            fileout.write(adir+'\n')
        fileout.close()

    return sorted_dirs

def list2dirs(listin):
    # Sort inlist
    sortedlist = sorted(listin)
    # Initialize vars
    # Adjust this to change max number of dirs in L2 and L3
    max_dirs = 30

    # DEBUGGING
    inbox_name = ""

    # Apparently can't append to tuples, so making pre-sets through iterations
    L1list = []
    L2list = []
    L3list = []
    for x in range(max_dirs):
        L1list.append("")
    for x in range(max_dirs):
        L2list.append(copy.deepcopy(L1list))
    for x in range(max_dirs):
        L3list.append(copy.deepcopy(L2list))
    #print(L1list)
    #print(L2list)
    #print(L3list)

    for entry in sortedlist:
        #print("Entering sortedList method of director.py")
        #a = input()
        # Get L1, L2 and L3 values, using stratify method
        L1, L2, L3 = stratify(entry)
        #print(L1)
        #print(L2)
        #print(L3)
        #a = input()
        #L1, L2, L3 = 0
        if L1 > 0 and L2 > 0 and L3 > 0:
            # Insert entry into postion [L1][L2][L3] of L3list
            L3list[L1][L2][L3] = entry
            #print("condition 1")
        elif L1 > 0 and L2 > 0 and L3 == 0:
            # Insert entry into postion [L1][L2] of L2list
            #print("L2: "+str(L2))
            L2list[L1][L2] = entry
            #print("condition 2")
        elif L1 > 0 and L2 == 0 and L3 == 0:
            # Insert entry into postion [L1] of L1list
            #print("L1: "+str(L1))
            L1list[L1] = entry
            #print("condition 3")
        else:
            # Note: since find returns location, use -1 to note a nonexistence
            if entry.find("0.") != -1:
                inbox_name = entry
            else:
                print("Irregular directory name. Removing directory: ")
                print(entry)

    return L1list, L2list, L3list, inbox_name

def stratify(stringin):
    # Initialize vars
    L1 = 0
    L2 = 0
    L3 = 0
    Lsel = 1
    Lstr = ""

    for char in stringin:
        tempchar = "a"
        try:
            tempchar = int(char)
        except ValueError:
            pass
        if isinstance(tempchar, int):
            #print("int")
            if Lsel == 1 or Lsel == 2 or Lsel == 3:
                Lstr += char
            else:
                return L1, L2, L3
        else:
            #print("char")
            if char == '.':
                #print("(is period)")
                if Lsel == 1:
                    L1 = int(Lstr)
                    Lstr = ""
                    Lsel = Lsel + 1
                elif Lsel == 2:
                    L2 = int(Lstr)
                    Lstr = ""
                    Lsel = Lsel + 1
                elif Lsel == 3:
                    L3 = int(Lstr)
                    Lstr = ""
                    Lsel = Lsel + 1
                else:
                    return L1, L2, L3
            else:
                return L1, L2, L3

    return L1, L2, L3
