#!/usr/bin/python3
import os, copy
import tkinter
from tkinter import ttk
#import shutil # Will need this for moving files and folders

# Import modules (homemade libraries)
import director

# Upload and process directories into Sorter
#=======================================================================
# Print files as well?
print_files = False

# Get the current working dir
cwd = os.getcwd()

# Print sorted directories to terminal
dirslist = director.gather_dirs()
if print_files == True:
    for item in dirslist:
        print(item)

# get lists for all 3 directory levels
L1_list, L2_list, L3_list, inbox_name = director.list2dirs(dirslist)

# Global variables (State reason for each!)
#=======================================================================
# Stores last Major selection, required here, as selecting a Minor deselects all else
last_major_selection = ""
# Stores last Minor selection, required here, as selecting a Subject deselects all else
last_minor_selection = ""
# Stores last Subject selection, required here, as selecting a Minor deselects all else,
# and to maintain the list of alternatives static while selecting different options
last_subject_selection = ""


# Define methods used in app
#=======================================================================
def sort_inbox(*args):
    try:
        inbox_select = inbox_listbox.get(inbox_listbox.curselection()[0])
        sorting_listbox.insert(tkinter.END, inbox_select)
        inbox_listbox.delete(inbox_listbox.curselection()[0])
    except:
        #pass
        try:
            if inbox_listbox.get(0) != '':
                sorting_listbox.insert(tkinter.END, inbox_listbox.get(0))
                inbox_listbox.delete(0)
        except ValueError:
            pass

def unsort_inbox(*args):
    try:
        inbox_select = sorting_listbox.get(sorting_listbox.curselection()[0])
        inbox_listbox.insert(tkinter.END, inbox_select)
        sorting_listbox.delete(sorting_listbox.curselection()[0])
    except:
        #pass
        try:
            if sorting_listbox.get(0) != '':
                inbox_listbox.insert(tkinter.END, sorting_listbox.get(0))
                sorting_listbox.delete(0)
        except ValueError:
            pass

def confirm(*args):
    #cwd = os.getcwd()
    try:
        for item in sorting_listbox.get(0, tkinter.END):
            # Get path of file
            filepath = cwd+os.sep+inbox_name+os.sep+item
            #print(filepath)
            #destination = cwd+os.sep+item
            predest = cwd
            if last_major_selection != "":
                predest = predest+os.sep+last_major_selection
            if last_minor_selection != "" and last_minor_selection != "<none>":
                predest = predest+os.sep+last_minor_selection
            if last_subject_selection != "" and last_subject_selection != "<none>":
                predest = predest+os.sep+last_subject_selection
            destination = predest+os.sep+item
            #print(destination)
            os.rename(filepath, destination)
        # Clear out the box after sorting
        sorting_listbox.delete(0, tkinter.END)
    except:
        print("Problem in moving files!")
        print("destination: ",destination)

# Listbox selection methods
# Note: try/except is needed when clicking between listboxes w/out error
def CurSelectMajor(event):
    try:
        value=str(major_listbox.get(major_listbox.curselection()))
    except:
        #print("ERROR in CurSelectMajor")
        value = ""
        pass
    #value=str(major_listbox.get(major_listbox.curselection()))
    #print(value)
    if value != "":
        # Get the selection
        index = major_listbox.get(0, "end").index(value)
        # Clear out the downstream listbox
        minor_listbox.delete(0, tkinter.END)
        # Insert a <none> entry, followed by all other entries
        minor_listbox.insert(0, "<none>")
        for i in range(len(L2_list[index+1])):
            if L2_list[index+1][i] != "":
                minor_listbox.insert(i+1, L2_list[index+1][i])
        global last_major_selection
        last_major_selection = value
        #print("last_major_selection: ",last_major_selection)
        #print(L2_list[index+1])
        #print("len: ",len(L2_list[index+1]))

def CurSelectMinor(event):
    try:
        value1=str(minor_listbox.get(minor_listbox.curselection()))
        #print("in CurSelectMinor")
    except:
        #print("ERROR in CurSelectMinor")
        value1 = ""
        pass
    #print("value1: ",value1)
    #print("last_major_selection: ", last_major_selection)
    if value1 != "" and last_major_selection != "":
        # Get the selections from Minor and Major
        index1 = minor_listbox.get(0, "end").index(value1)
        index2 = major_listbox.get(0, "end").index(last_major_selection)
        # Clear out the downstream listbox
        subject_listbox.delete(0, tkinter.END)
        # Insert a <none> entry, followed by all other entries
        subject_listbox.insert(0, "<none>")
        for i in range(len(L3_list[index1][index2+1])):
            if L3_list[index1][index2+1][i] != "":
                subject_listbox.insert(i+1, L3_list[index1][index2+1][i])
        global last_minor_selection
        last_minor_selection = value1

def CurSelectSubject(event):
    try:
        value2=str(subject_listbox.get(subject_listbox.curselection()))
        #print(value2)
    except:
        value2 = ""
        pass
    if value2 != "" and last_minor_selection != "" and last_major_selection != "":
        global last_subject_selection
        last_subject_selection = value2

def CurSelectAlt(event):
    try:
        value=str(alternatives_listbox.get(alternatives_listbox.curselection()))
        print(value)
    except:
        pass


# Begin instance of tkinter GUI and configure main window
#=======================================================================
appwindow = tkinter.Tk()
appwindow.title("Schluchter's Sorter v0.7")
#appwindow.geometry("1200x600")
mainframe = ttk.Frame(appwindow, padding="3 3 12 12")
#mainframe = tkinter.Frame(appwindow, width=1200, height=600, bg="", colormap="new)
#width=768, height=576, bg="", colormap="new"
#, padding="3 3 12 12")
#mainframe.grid(column=0, row=0)
mainframe.grid(column=0, row=0, sticky=(tkinter.N, tkinter.W, tkinter.E, tkinter.S))
mainframe.columnconfigure(0, weight=1)
mainframe.rowconfigure(0, weight=1)

# tkinter variables
#=======================================================================
inbox_select = tkinter.StringVar()


# Cells
#=======================================================================
# listbox cells
major_listbox = tkinter.Listbox(mainframe, width=25, height=15)
major_listbox.grid(column=1, columnspan=3, row=2, rowspan=9)
# Insert entries
for x in range(1, len(L1_list)):
    if L1_list[x] != "":
        major_listbox.insert(x, (L1_list[x]))
major_listbox.bind('<<ListboxSelect>>',CurSelectMajor)

minor_listbox = tkinter.Listbox(mainframe, width=35, height=15)
minor_listbox.grid(column=4, columnspan=3, row=2, rowspan=9)
# Insert entries
# REMOVED: may override previous selections of other listboxes!
#minor_listbox.insert(0, "<none>")
#for x in range(1,21):
#    if L2_list[1][x] != "":
#        minor_listbox.insert(x, (L2_list[1][x]))
minor_listbox.bind('<<ListboxSelect>>',CurSelectMinor)

"""
# NOTE: Scrollbars requires the listboxes to be set within their own respective Frame :/
minor_scrollbar = tkinter.Scrollbar(minor_listbox)
minor_scrollbar.pack( side = tkinter.RIGHT, fill = tkinter.Y )
minor_listbox = tkinter.Listbox(mainframe, yscrollcommand = scrollbar.set)
minor_listbox.grid(column=3, columnspan=2, row=2, rowspan=9, sticky=(tkinter.N, tkinter.S, tkinter.W, tkinter.E))
for x in range(1,21):
    minor_listbox.insert(x, ("Entry "+str(x)))
mylist = tkinter.Listbox(root, yscrollcommand = scrollbar.set )
#mylist.pack( side = tkinter.LEFT, fill = tkinter.BOTH )
#minor_scrollbar.config( command = minor_listbox.yview )
"""

subject_listbox = tkinter.Listbox(mainframe, width=30, height=15)
subject_listbox.grid(column=7, columnspan=3, row=2, rowspan=9)
# Insert entries
# REMOVED: may override previous selections of other listboxes!
#subject_listbox.insert(0, "<none>")
#for x in range(1,21):
#    if L3_list[1][1][x] != "":
#        subject_listbox.insert(x, (L3_list[1][1][x]))
subject_listbox.bind('<<ListboxSelect>>',CurSelectSubject)

alternatives_listbox = tkinter.Listbox(mainframe)
alternatives_listbox.grid(column=10, columnspan=3, row=2, rowspan=20, sticky=(tkinter.N, tkinter.S, tkinter.W, tkinter.E))
# Insert entries
alternatives_listbox.insert(0, "<none>")
# THE FOLLOWING IS TEMPORARY!
for x in range(1,21):
    alternatives_listbox.insert(x, ("Entry "+str(x)))
alternatives_listbox.bind('<<ListboxSelect>>',CurSelectAlt)

inbox_listbox = tkinter.Listbox(mainframe)
inbox_listbox.grid(column=1, columnspan=5, row=13, rowspan=9, sticky=(tkinter.N, tkinter.S, tkinter.W, tkinter.E))
# Insert entries
# Note: this seems excessive, but also needed for sorting inbox before inputting
for files in os.walk(os.getcwd()+os.sep+inbox_name):
    presorted_inbox = []
    #print(files[2])
    # Note: the 2 refers to the 3rd thing in the os.walk tuple (the actual file names)
    for i in range(len(files[2])):
        presorted_inbox.insert(i, files[2][i])
    sorted_inbox = sorted(presorted_inbox)
    for i in range(len(sorted_inbox)):
        inbox_listbox.insert(i, sorted_inbox[i])

sorting_listbox = tkinter.Listbox(mainframe)
sorting_listbox.grid(column=6, columnspan=4, row=13, rowspan=9, sticky=(tkinter.N, tkinter.S, tkinter.W, tkinter.E))

# input cells
"""
#test_entry = ttk.Entry(mainframe, width=7, textvariable=vartest)
test_entry = ttk.Entry(mainframe, textvariable=vartest)
test_entry.grid(column=3, row=31, sticky=tkinter.W)

#feet_entry = ttk.Entry(mainframe, width=7, textvariable=feet)
sort_input = ttk.Entry(mainframe, textvariable=feet)
sort_input.grid(column=3, row=32, sticky=tkinter.W)
"""

# dynamic label cells
#meters_display = ttk.Label(mainframe, textvariable=meters).grid(column=3, row=33, sticky=tkinter.W)

# button cells
folder_major_button = ttk.Button(mainframe, text="Go to Folder", command=sort_inbox)
folder_major_button.grid(column=2, columnspan=2, row=1, rowspan=1)

folder_minor_button = ttk.Button(mainframe, text="Go to Folder", command=sort_inbox)
folder_minor_button.grid(column=5, columnspan=2, row=1, rowspan=1)

folder_subject_button = ttk.Button(mainframe, text="Go to Folder", command=sort_inbox)
folder_subject_button.grid(column=8, columnspan=2, row=1, rowspan=1)

sort_button = ttk.Button(mainframe, text="Sort", command=sort_inbox)
sort_button.grid(column=2, columnspan=2, row=11, rowspan=2)

unsort_button = ttk.Button(mainframe, text="Unsort", command=unsort_inbox)
unsort_button.grid(column=4, columnspan=2, row=11, rowspan=2)

confirm_button = ttk.Button(mainframe, text="Confirm", command=confirm)
confirm_button.grid(column=8, columnspan=2, row=11, rowspan=2)


# static label cells
major_label = ttk.Label(mainframe, text="Major")
major_label.grid(column=1, columnspan=1, row=1, sticky=(tkinter.S, tkinter.W))

minor_label = ttk.Label(mainframe, text="Minor")
minor_label.grid(column=4, columnspan=1, row=1, sticky=(tkinter.S, tkinter.W))

subject_label = ttk.Label(mainframe, text="Subject")
subject_label.grid(column=7, columnspan=1, row=1, sticky=(tkinter.S, tkinter.W))

alternatives_label = ttk.Label(mainframe, text="Alternatives")
alternatives_label.grid(column=10, columnspan=1, row=1, sticky=(tkinter.S, tkinter.W))

inbox_label = ttk.Label(mainframe, text="Inbox")
inbox_label.grid(column=1, row=11, rowspan=2, sticky=(tkinter.S, tkinter.W))

sorting_label = ttk.Label(mainframe, text="Sorting")
sorting_label.grid(column=6, row=11, rowspan=2, sticky=(tkinter.S, tkinter.W))

# THIS MUST COME AFTER ALL CELLS
# Set ability to use Return key to make selections in listboxes and buttons
inbox_listbox.bind('<Return>', sort_inbox)
sort_button.bind('<Return>', sort_inbox)
sorting_listbox.bind('<Return>', unsort_inbox)
unsort_button.bind('<Return>', unsort_inbox)

# Sets minimum padding between each cell
for child in mainframe.winfo_children(): child.grid_configure(padx=5, pady=5)

# Begin application
#=======================================================================
# This allows the user to start entry into the box at app launch
#feet_entry.focus()
appwindow.mainloop()
