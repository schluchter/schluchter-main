
"""
#=========================
Commands:
#=========================
SYSTEM:
    be quiet
    you may speak

MUSIC PLAYER:
    play <song|album|artist> [opt: by <artist>]
        NOTE: in case of conflict, announce:
        "There are _____ __'s by that name; playing the first."
    [ALT] play <first|second|etc.> <song|album|artist> [opt: by <artist>]
    pause music
    resume music
    set volume to ____ (0 to 100, as in percent)

TASKMASTER:
    take note (need something better for this? Stick to button command directly?)

"""
