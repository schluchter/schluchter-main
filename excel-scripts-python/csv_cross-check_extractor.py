import os, pandas, xlsxwriter, csv
import re

cwd = os.getcwd()
input_file_name = "input.csv"
input_file = cwd+os.sep+input_file_name

def getlist(filename, column, extend_flag):
    flag = extend_flag
    col = column
    thisList = []
    extendList = []
    with open(input_file, 'rt') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='|')
        if flag == True:
            for row in reader:
                #print(row[col])
                thisList.append(row[col])
                extendList.append(row[col+1])
        else:
            for row in reader:
                #print(row[col])
                thisList.append(row[col])
    #print(thisList)
    #x=input()
    if flag == True:
        thisList = [thisList, extendList]
    return thisList

def scrub_list(input_list, extend_flag):
    flag = extend_flag
    thisList = []
    if flag == True:
        for sublist in input_list:
            for item in sublist:
                #print(item)
                item = re.sub(r'[^\x00-\x7F]+',' ', item)
                item = item.strip(" ")
                thisList.append(item)
    else:
        for item in input_list:
            item = re.sub(r'[^\x00-\x7F]+',' ', item)
            item = item.strip(" ")
            thisList.append(item)
    return(thisList)

def main():
    list1 = getlist(input_file, 0, False)
    list1 = scrub_list(list1, False)
    list2 = getlist(input_file, 1, True)
    # only scrub the second list of it's not extended
    if not isinstance(list2[0], list):
        list2 = scrub_list(list2, True)
    # Build the output file
    outputDF = pandas.DataFrame(columns=['Acceptable','Extensions'])
    if isinstance(list2[0], list):
        for index, item in enumerate(list2[0]):
            if item in list1:
                #print("index: "+str(index))
                #print("list2[1] item: "+list2[1][index])
                outputDF = outputDF.append({'Acceptable':item,'Extensions':list2[1][index]}, ignore_index=True)
    else:
        for item in list2:
            if item in list1:
                outputDF = outputDF.append({'Acceptable':item}, ignore_index=True)
    # Save output files
    writer = pandas.ExcelWriter('output.xlsx')
    outputDF.to_excel(writer,'Sheet1')
    writer.save()

if __name__ == '__main__':
    main()
    print("\nThis script has completed.")
    x = input()
