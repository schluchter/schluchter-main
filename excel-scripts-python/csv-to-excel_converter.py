import os, pandas, xlsxwriter, csv
import re

cwd = os.getcwd()
#print("Enter input file name: ")
#input_name = input()


input_file = "task_export.csv"
#input_file = "empty.csv"


input_name = cwd+os.sep+input_file
#test_obj = pandas.read_csv(input_name)
#print(input_name)
data = pandas.read_csv(input_file, sep=';')

"""
    # Load the Pandas libraries with alias 'pd' 
    import pandas as pd 
    # Read data from file 'filename.csv' 
    # (in the same directory that your python process is based)
    # Control delimiters, rows, column names with read_csv (see later) 
    data = pd.read_csv("filename.csv") 
    # Preview the first 5 lines of the loaded data 
    data.head()
"""
#test_obj = pandas.read_csv("task_export.csv")

#outputDF = pandas.DataFrame(columns=['Acceptable','Extensions'])
outputDF = pandas.DataFrame(data)
writer = pandas.ExcelWriter('output.xlsx')
outputDF.to_excel(writer,'Sheet1')
writer.save()
