clear all
close all

addpath('C:\Users\aschluchter\Desktop');

workingDir = 'C:\Users\aschluchter\Desktop\testzone';
%mkdir(workingDir,'images')

ii = 1;

imageNames = dir(fullfile(workingDir,'imageSource','*.png'));
imageNames = {imageNames.name}';

for ii = (1:length(imageNames))
   img = imread(fullfile(workingDir,'imageSource',imageNames{ii}));
   filename = [sprintf('%03d',ii) '.jpg'];
   fullname = fullfile(workingDir,'images',filename);
   imwrite(img,fullname)    % Write out to a JPEG file (img1.jpg, img2.jpg, etc.)
   thisGrayMatrix = mat2gray(img);
   if ii == 1
       [x,y] = size(thisGrayMatrix);
       sumGrayMatrix = zeros(x,y);
       for jj = (1:x)
          for kk = (1:y)
              bigNum = cast(thisGrayMatrix(jj,kk),'double');
              sumGrayMatrix(jj,kk) = bigNum;
          end
       end
       jj = 1;
       kk = 1;
       %tempGrayMatrix = im2double(thisGrayMatrix);
       %sumGrayMatrix = sumGrayMatrix + tempGrayMatrix;
       %firstSumGrayMatrix = sumGrayMatrix;
   else
       %tempGrayMatrix = im2double(thisGrayMatrix);
       for jj = (1:x)
          for kk = (1:y)
              bigNum = cast(thisGrayMatrix(jj,kk),'double');
              sumGrayMatrix(jj,kk) = sumGrayMatrix(jj,kk) + bigNum;
              %arr(k) = arr(k) + abs(double(frm(k+i-1,j))-double(model(i,j)));
          end
       end
       jj = 1;
       kk = 1;
   end
   
   %ii = ii-(Y*X);
   % do I need this here? I think it's redundant... ii = ii+1;
end

sumImg = mat2gray(sumGrayMatrix);
fullname = fullfile(workingDir,'normalized_mask.jpg');
imwrite(sumImg,fullname)    % Write out to a JPEG file (img1.jpg, img2.jpg, etc.)

maxNum = max(max(sumGrayMatrix));
threshmin = 0.15*maxNum;
threshmax = maxNum - (0.05*maxNum);

%high thresholding
highThreshGrayMatrix = zeros(x,y);
for jj = (1:x)
    for kk = (1:y)
        if sumGrayMatrix(jj,kk) > threshmax
            highThreshGrayMatrix(jj,kk) = 255;
        end
    end
end
jj = 1;
kk = 1;

sumImg = mat2gray(highThreshGrayMatrix);
fullname = fullfile(workingDir,'high_mask.jpg');
imwrite(sumImg,fullname)    % Write out to a JPEG file (img1.jpg, img2.jpg, etc.)

%low thresholding
lowThreshGrayMatrix = zeros(x,y);
for jj = (1:x)
    for kk = (1:y)
        if sumGrayMatrix(jj,kk) < threshmin
            lowThreshGrayMatrix(jj,kk) = 255;
        end
    end
end
jj = 1;
kk = 1;

sumImg = mat2gray(lowThreshGrayMatrix);
fullname = fullfile(workingDir,'low_mask.jpg');
imwrite(sumImg,fullname)    % Write out to a JPEG file (img1.jpg, img2.jpg, etc.)

imageNames = dir(fullfile(workingDir,'images','*.jpg'));
imageNames = {imageNames.name}';

%outputVideo = VideoWriter(fullfile(workingDir,'video_out.avi'));
outputVideo = VideoWriter(fullfile('C:\Users\aschluchter\Desktop\video_out.avi'));

outputVideo.FrameRate = thisvideo.FrameRate;
open(outputVideo)

for ii = (1:length(imageNames))
   img = imread(fullfile(workingDir,'images',imageNames{ii}));
   writeVideo(outputVideo,img)
end

close(outputVideo)
