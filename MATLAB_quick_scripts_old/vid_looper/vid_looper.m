clear all
close all

addpath('C:\Users\aschl016\Desktop\');

X = 32; % number of frames
Y = 15; % number of loops to add

workingDir = tempname;
mkdir(workingDir)
mkdir(workingDir,'images')

% read the video and create the test signal with it
thisvideo = VideoReader('Long1.avi');

ii = 1;

while hasFrame(thisvideo)
   img = readFrame(thisvideo);
   filename = [sprintf('%03d',ii) '.jpg'];
       fullname = fullfile(workingDir,'images',filename);
       imwrite(img,fullname)    % Write out to a JPEG file (img1.jpg, img2.jpg, etc.)
   for jj = 1:Y
       ii = ii+X;
       filename = [sprintf('%03d',ii) '.jpg'];
       fullname = fullfile(workingDir,'images',filename);
       imwrite(img,fullname)    % Write out to a JPEG file (img1.jpg, img2.jpg, etc.)
       %{
       ii = ii+X;
       filename = [sprintf('%03d',ii) '.jpg'];
       fullname = fullfile(workingDir,'images',filename);
       imwrite(img,fullname)    % Write out to a JPEG file (img1.jpg, img2.jpg, etc.)
       ii = ii+X;
       filename = [sprintf('%03d',ii) '.jpg'];
       fullname = fullfile(workingDir,'images',filename);
       imwrite(img,fullname)    % Write out to a JPEG file (img1.jpg, img2.jpg, etc.)
       ii = ii-X-X-X;
       %}
       jj = jj +1;
   end    
   ii = ii-(Y*X);
   ii = ii+1;
end

imageNames = dir(fullfile(workingDir,'images','*.jpg'));
imageNames = {imageNames.name}';

%outputVideo = VideoWriter(fullfile(workingDir,'video_out.avi'));
outputVideo = VideoWriter(fullfile('C:\Users\aschl016\Desktop\video_out.avi'));

outputVideo.FrameRate = thisvideo.FrameRate;
open(outputVideo)

for ii = (1:length(imageNames))
   img = imread(fullfile(workingDir,'images',imageNames{ii}));
   writeVideo(outputVideo,img)
end

close(outputVideo)

%View the Final Video

%Construct a reader object.

%outAvi = VideoReader(fullfile(workingDir,'video_out.avi'));
outAvi = VideoReader(fullfile('C:\Users\aschl016\Desktop\video_out.avi'));

%Create a MATLAB movie struct from the video frames.

ii = 1;
while hasFrame(outAvi)
   mov(ii) = im2frame(readFrame(outAvi));
   ii = ii+1;
end