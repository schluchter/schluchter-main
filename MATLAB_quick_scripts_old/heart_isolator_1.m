clear all
close all

%set paths and make directories
addpath('C:\Users\aschluchter\Desktop\');
workingDir = 'C:\Users\aschluchter\Desktop';
mkdir(workingDir,'heartIsolator');
addpath('C:\Users\aschluchter\Desktop\heartIsolator');
workingDir = 'C:\Users\aschluchter\Desktop\heartIsolator';
mkdir(workingDir,'imagesInput');
mkdir(workingDir,'lowMaskImages');
mkdir(workingDir,'lowMaskImagesIsolated');


% Make a list of the file names in the input folder
imageNames = dir(fullfile(workingDir,'imagesInput','*.png'));
imageNames = {imageNames.name}';

% initialize counters
hh = 1;
ii = 1;
jj = 1;
kk = 1;

%for hh = (1:100)
%DEBUG VERSION:
for hh = (15:1:16)
for ii = (1:length(imageNames))

img = imread(fullfile(workingDir,'imagesInput',imageNames{ii}));
imgGray = rgb2gray(img);

 if ii == 1
       [y,x] = size(imgGray);
       %------------------------------------
        yCenter = round(y/2);
        xCenter = round(x/2);
        %------------------------------------
       sumGrayMatrix = zeros(y,x);
       for jj = (1:y)
          for kk = (1:x)
              bigNum = cast(imgGray(jj,kk),'double');
              sumGrayMatrix(jj,kk) = bigNum;
          end
       end
 else
       for jj = (1:y)
          for kk = (1:x)
              bigNum = cast(imgGray(jj,kk),'double');
              sumGrayMatrix(jj,kk) = sumGrayMatrix(jj,kk) + bigNum;
          end
       end

 end
 %---------------------------------------
sumGrayMatrix(yCenter,xCenter) = 255;
%---------------------------------------
end

% set the thresholds for high/low
maxNum = max(max(sumGrayMatrix));
threshmin = 0.01*hh*maxNum;

% build the low mask matrix
lowThreshGrayMatrix = zeros(y,x);
for jj = (1:y)
    for kk = (1:x)
        if sumGrayMatrix(jj,kk) < threshmin
            lowThreshGrayMatrix(jj,kk) = 255;
        end
    end
end

% convert the mask matrix to a grayscale image
sumImg = mat2gray(lowThreshGrayMatrix);

% save the mask image as a PNG in the output folder
filename = [sprintf('%03d',hh) '_low_mask.png'];
fullname = fullfile(workingDir,'lowMaskImages',filename);
imwrite(sumImg,fullname)    % Write out to a PNG file (img1.png, img2.png, etc.)

% isolate away the aorta/vena cava

% using only hh = 16 to save time
if hh == 16

%mark center
yCenter = round(y/2);
xCenter = round(x/2);
minDim = min(x,y);
maxRay = round(minDim/2)-2;
%debug only:
%sumGrayMatrix(yCenter,xCenter) = 255;
lastdx = 0;
lastdy = 0;

%for smoothing over spotty areas
thisSpottyrr = -1;
spottyFlag = 0;
%unSpottyCounter = 0;
lastUnspottyrr = [-1,-1];
newUnspottyrr = [-1,-1];
spottyDir = -1;
absDelMax = 7;
delrr = 0;





% statement to catch spotty regions and 'breaks' (put down below)
if (rr == 0) || (abs(delrr) > absDelMax)
    spottyFlag = 1;
    
    % mark direction instability went toward
    if sign(delrr) == 1 || sign(delrr) == 0
        spottyDir = 1;
        % SAVE LAST STABLE COORDINATE
    end
    
    if sign(delrr) == -1    % change to 'else' of if loop above once verified stable
        spottyDir = 0;
    end
end


% coming back from an excursion through un unstable region
% MAKE SURE TO PUT THIS SOMEWHERE WHERE RR IS NO LONGER INCREMENTING FOR THAT RAY!
if (rr > 0 &&) (abs(delrr) <= absDelMax) && (spottyFlag == 1)
    if sign(lastDelrr) == sign
        sadas
    end
    if asds
        
    end
end





%USING POLAR COORDINATES:
rotMax = 2*pi; % if using degrees
rotInc = pi/180;
rayVectors = zeros((round(rotMax/rotInc)),maxRay);
boundFunction = zeros(1,round(rotMax/rotInc));

%go to the right from center until a bound is hit and then go for at least another 1/4 of that distance
for rot = (0:rotInc:rotMax)
    skipFlag = 0;
    for rr = (1:maxRay)
        
        if skipFlag ==0;

            [dx,dy] = pol2cart(rot,rr);
            dy = round(dy);
            dx = round(dx);
            thisRay=1+round(rot/rotInc);
            if dx ~= lastdx || dy ~= lastdy
                rayVectors(thisRay,rr) = sumImg(yCenter-dy,xCenter+dx);
            else
                rayVectors(thisRay,rr) = rayVectors(thisRay,rr-1);
            end
            
            if rayVectors(thisRay,rr) == 1
                skipFlag = 1;
            end
            
            lastdx = dx;
            lastdy = dy;

        end
    end
end

for rot = (0:rotInc:rotMax)
    thisRay=1+round(rot/rotInc);
    for RR = (1:maxRay)
        if rayVectors(thisRay,RR) == 1
            boundFunction(1,thisRay) = RR;
        end
    end
end


end



% save the isolated mask image as a PNG in the output folder
filename = [sprintf('%03d',hh) '_low_mask_isolated.png'];
fullname = fullfile(workingDir,'lowMaskImagesIsolated',filename);
imwrite(sumImg,fullname)    % Write out to a PNG file (img1.png, img2.png, etc.)

end
%{
%VIDEO OUTPUT ONLY

% Set the output video location
outputVideo = VideoWriter(fullfile('C:\Users\aschluchter\Desktop\heartIsolator\video_out.avi'));
% Set framerate and open the output video
outputVideo.FrameRate = 16;
open(outputVideo)
%write the output video
for ii = (1:length(imageNames))
   img = imread(fullfile(workingDir,'imagesInput',imageNames{ii}));
   writeVideo(outputVideo,img)
end

close(outputVideo)
%}
