clear all
close all

% Set up the environment
workingDir = pwd
videoInput = 'test.mp4';
mkdir(workingDir,'imagesOutput')

% Grab some data
thisvideo = VideoReader(videoInput);
framerate = thisvideo.FrameRate
duration_in_seconds = thisvideo.Duration
num_frames = framerate*duration_in_seconds

% Parse through the frames of the video and save them
ii = 1;
while hasFrame(thisvideo)
   img = readFrame(thisvideo);      
    %save the current image as PNG
    filename = [sprintf('%03d',ii) '.png'];
    fullname = fullfile(workingDir,'imagesOutput',filename);
    imwrite(img,fullname)    % Write out to a PNG file (img1.png, img2.png, etc.)
   ii = ii+1;
end

