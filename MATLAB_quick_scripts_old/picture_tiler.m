% set image path
addpath('C:\Users\aschluchter\Desktop\')
addpath('C:\Users\aschluchter\Desktop\wall_project')
workingDir = 'C:\Users\aschluchter\Desktop\wall_project';
% output filename:
outfile = 'C:\Users\aschluchter\Desktop\wall_project\output5.jpg';
% load the image:
img = imread('space_fabric_5_quilting_cotton_44_in_cr.jpg');

% get the X and Y dimensions
[y_max, x_max, z_max] = size(img);

% make a blank image that's 5x by 9y
mult_img = zeros(y_max*9, x_max*5, z_max);

% for loops: Y iterations:
    % write pixels 1:max_X to same coordinates + (max_X times 0,1,2,3&4)
for i = 1:y_max
    for ii = 1:x_max
        for m = 0:4
            for iii = 1:z_max
                mult_img(i,ii+(x_max*m),iii)=img(i,ii,iii);
            end
        end
    end
end

% get new max dimensions
[y_max_big, x_max_big, z_max_big] = size(mult_img);

% for loops: new_max_X, or 5 time max_X, iterations:
% write pixels 1:max_Y to same coordinates + (max_Y times 1:8)

for j = 1:x_max_big
    for jj = 1:y_max
        %for m = 1:8
            for jjj = 1:z_max_big
                mult_img(jj+(y_max),j,jjj) = mult_img(jj,j,jjj);
                mult_img(jj+(y_max*2),j,jjj) = mult_img(jj,j,jjj);
                mult_img(jj+(y_max*3),j,jjj) = mult_img(jj,j,jjj);
                mult_img(jj+(y_max*4),j,jjj) = mult_img(jj,j,jjj);
                mult_img(jj+(y_max*5),j,jjj) = mult_img(jj,j,jjj);
                mult_img(jj+(y_max*6),j,jjj) = mult_img(jj,j,jjj);
                mult_img(jj+(y_max*7),j,jjj) = mult_img(jj,j,jjj);
                mult_img(jj+(y_max*8),j,jjj) = mult_img(jj,j,jjj);
            end
        %end
    end
end

%{
for ii = 1:y_max
    for m = 1:1
        mult_img(ii+(y_max*m),:,:)=mult_img(ii,:,:);           
    end
end
%}

% save the file
output = uint8(mult_img);
%output = mult_img;
imwrite(output, outfile);