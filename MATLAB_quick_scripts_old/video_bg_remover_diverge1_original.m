clear all
close all

addpath('C:\Users\aschluchter\Desktop');

%X = 32; % number of frames
%Y = 15; % number of loops to add

workingDir = tempname;
mkdir(workingDir)
mkdir(workingDir,'images')
mkdir(workingDir,'images_thresholded_high')
mkdir(workingDir,'images_thresholded_low')
mkdir(workingDir,'images_thresholded_high_and_low')
mkdir(workingDir,'images_frame_by_frame_plus1_derivatives')
mkdir(workingDir,'images_processed')

% read the video and create the test signal with it
thisvideo = VideoReader('Long1.avi');

ii = 1;

while hasFrame(thisvideo)
   img = readFrame(thisvideo);
   filename = [sprintf('%03d',ii) '.jpg'];
   fullname = fullfile(workingDir,'images',filename);
   imwrite(img,fullname)    % Write out to a JPEG file (img1.jpg, img2.jpg, etc.)
   thisGrayMatrix = rgb2gray(img);
   if ii == 1
       [x,y] = size(thisGrayMatrix);
       sumGrayMatrix = zeros(x,y);
       tempGrayMatrix = im2double(thisGrayMatrix);
       sumGrayMatrix = sumGrayMatrix + tempGrayMatrix;
       %firstSumGrayMatrix = sumGrayMatrix;
   else
       tempGrayMatrix = im2double(thisGrayMatrix);
       sumGrayMatrix = sumGrayMatrix + tempGrayMatrix;
   end
   
   %ii = ii-(Y*X);
   ii = ii+1;
end

sumImg = mat2gray(sumGrayMatrix);
fullname = fullfile(workingDir,'normalized_mask.jpg');
imwrite(sumImg,fullname)    % Write out to a JPEG file (img1.jpg, img2.jpg, etc.)

imageNames = dir(fullfile(workingDir,'images','*.jpg'));
imageNames = {imageNames.name}';

%outputVideo = VideoWriter(fullfile(workingDir,'video_out.avi'));
outputVideo = VideoWriter(fullfile('C:\Users\aschluchter\Desktop\video_out.avi'));

outputVideo.FrameRate = thisvideo.FrameRate;
open(outputVideo)

for ii = (1:length(imageNames))
   img = imread(fullfile(workingDir,'images',imageNames{ii}));
   writeVideo(outputVideo,img)
end

close(outputVideo)
