clear all
close all

addpath('C:\Users\aschluchter\Desktop\');

workingDir = 'C:\Users\aschluchter\Desktop\images2video';
mkdir(workingDir,'imagesInput');

%referenceVideo = VideoReader('Long1.avi');

% Make a video to output
imageNames = dir(fullfile(workingDir,'imagesInput','*.png'));
imageNames = {imageNames.name}';

%outputVideo = VideoWriter(fullfile(workingDir,'video_out.avi'));
outputVideo = VideoWriter(fullfile('C:\Users\aschluchter\Desktop\images2video\video_out.avi'));
%
%outputVideo.FrameRate = referenceVideo.FrameRate;
outputVideo.FrameRate = 16;
open(outputVideo)

for ii = (1:length(imageNames))
   img = imread(fullfile(workingDir,'imagesInput',imageNames{ii}));
   writeVideo(outputVideo,img)
end

close(outputVideo)