clear all
close all

%set paths and make directories
addpath('C:\Users\aschluchter\Desktop\');
workingDir = 'C:\Users\aschluchter\Desktop';
mkdir(workingDir,'highMaskMaker');
addpath('C:\Users\aschluchter\Desktop\highMaskMaker');
workingDir = 'C:\Users\aschluchter\Desktop\highMaskMaker';
mkdir(workingDir,'imagesInput');
mkdir(workingDir,'highMaskImages');

% Make a list of the file names in the input folder
imageNames = dir(fullfile(workingDir,'imagesInput','*.png'));
imageNames = {imageNames.name}';

% initialize counters
hh = 1;
ii = 1;
jj = 1;
kk = 1;

for hh = (1:100)
for ii = (1:length(imageNames))

img = imread(fullfile(workingDir,'imagesInput',imageNames{ii}));
imgGray = rgb2gray(img);

 if ii == 1
       [x,y] = size(imgGray);
       sumGrayMatrix = zeros(x,y);
       for jj = (1:x)
          for kk = (1:y)
              bigNum = cast(imgGray(jj,kk),'double');
              sumGrayMatrix(jj,kk) = bigNum;
          end
       end
 else
       for jj = (1:x)
          for kk = (1:y)
              bigNum = cast(imgGray(jj,kk),'double');
              sumGrayMatrix(jj,kk) = sumGrayMatrix(jj,kk) + bigNum;
          end
       end

 end


end

% set the thresholds for high/low
maxNum = max(max(sumGrayMatrix));
threshmax = (maxNum - (0.01*hh*maxNum));

% build the high mask matrix
lowThreshGrayMatrix = zeros(x,y);
for jj = (1:x)
    for kk = (1:y)
        if sumGrayMatrix(jj,kk) > threshmax
            lowThreshGrayMatrix(jj,kk) = 255;
        end
    end
end

% convert the mask matrix to a grayscale image
sumImg = mat2gray(lowThreshGrayMatrix);
% save the mask image as a PNG in the output folder
filename = [sprintf('%03d',hh) '_high_mask.png'];
fullname = fullfile(workingDir,'highMaskImages',filename);
imwrite(sumImg,fullname)    % Write out to a PNG file (img1.png, img2.png, etc.)

end
%{
%VIDEO OUTPUT ONLY

% Set the output video location
outputVideo = VideoWriter(fullfile('C:\Users\aschluchter\Desktop\highMaskMaker\video_out.avi'));
% Set framerate and open the output video
outputVideo.FrameRate = 16;
open(outputVideo)
%write the output video
for ii = (1:length(imageNames))
   img = imread(fullfile(workingDir,'imagesInput',imageNames{ii}));
   writeVideo(outputVideo,img)
end

close(outputVideo)
%}
