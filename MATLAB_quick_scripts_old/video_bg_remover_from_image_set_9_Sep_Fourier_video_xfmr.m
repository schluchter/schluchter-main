clear all
close all

addpath('C:\Users\aschluchter\Desktop\testzoneF');

%X = 32; % number of frames
%Y = 15; % number of loops to add

%workingDir = tempname;
workingDir = 'C:\Users\aschluchter\Desktop\testzoneF';
%mkdir(workingDir)
mkdir(workingDir,'images')
mkdir(workingDir,'diffImages')
mkdir(workingDir,'FFTImages')
%mkdir(workingDir,'images_thresholded_high')
%mkdir(workingDir,'images_thresholded_low')
%mkdir(workingDir,'images_thresholded_high_and_low')
%mkdir(workingDir,'images_frame_by_frame_plus1_derivatives')
%mkdir(workingDir,'images_processed')

% read the video and create the test signal with it
thisvideo = VideoReader('Long1.avi');

% Find number of frames in the video and get x,y
numFrames = 0;
while hasFrame(thisvideo)
    lastImg = readFrame(thisvideo);
    numFrames = numFrames + 1;
end
lastImgGray = rgb2gray(lastImg);
[x,y] = size(lastImgGray);

% prep multi_D arrays for difference frames and FFT xforming
imageBlock = zeros(x,y,numFrames);
FFTImageBlock = zeros(x,y,numFrames);

% Parse through the frames of the video
thisvideo = VideoReader('Long1.avi');
ii = 1;
while hasFrame(thisvideo)
   img = readFrame(thisvideo);
   thisImgGray = rgb2gray(img);
    %get inter-frame difference rate by pixel
        for jj = (1:x)
          for kk = (1:y)
              
              imageBlock(jj,kk,ii) = thisImgGray(jj,kk);
              
              %bigNum = cast(thisGrayMatrix(jj,kk),'double');
              %sumGrayMatrix(jj,kk) = bigNum;
          end
        end
    
    %save the difference image as PNG
    diffImg = mat2gray(imageBlock(:,:,ii));
    filename = [sprintf('%03d',ii) '.png'];
    fullname = fullfile(workingDir,'diffImages',filename);
    imwrite(diffImg,fullname);    % Write out to a PNG file (img1.png, img2.png, etc.)
              
    %save the current image as PNG
    filename = [sprintf('%03d',ii) '.png'];
    fullname = fullfile(workingDir,'images',filename);
    imwrite(img,fullname)    % Write out to a PNG file (img1.png, img2.png, etc.)
    thisGrayMatrix = rgb2gray(img);
    if ii == 1
       sumGrayMatrix = zeros(x,y);
       for jj = (1:x)
          for kk = (1:y)
              bigNum = cast(thisGrayMatrix(jj,kk),'double');
              sumGrayMatrix(jj,kk) = bigNum;
          end
       end
       jj = 1;
       kk = 1;
       %tempGrayMatrix = im2double(thisGrayMatrix);
       %sumGrayMatrix = sumGrayMatrix + tempGrayMatrix;
       %firstSumGrayMatrix = sumGrayMatrix;
   else
       %tempGrayMatrix = im2double(thisGrayMatrix);
       for jj = (1:x)
          for kk = (1:y)
              bigNum = cast(thisGrayMatrix(jj,kk),'double');
              sumGrayMatrix(jj,kk) = sumGrayMatrix(jj,kk) + bigNum;
              %arr(k) = arr(k) + abs(double(frm(k+i-1,j))-double(model(i,j)));
          end
       end
       jj = 1;
       kk = 1;
   end
   
   lastImg = img;
   lastImgGray = rgb2gray(lastImg);
   ii = ii+1;
end

%timeVector = zeros(numFrames, 1);
%FFTVector = zeros(numFrames, 1);
timeVector = (1:numFrames);
FFTVector = (1:numFrames);
realFFTVector = (1:numFrames);

% FFT that mofo
        for jj = (1:x)
          for kk = (1:y)
            timeVector(1,:) = imageBlock(100,100,:);
            FFTVector = fft(timeVector(1,:));
            realFFTVector =;
          end
        end

%save the FFT images as PNG
for ii = (1:numFrames)
FFTImg = img;
FFTImg = mat2gray(FFTImageBlock(:,:,ii));
filename = [sprintf('%03d',ii) '.png'];
fullname = fullfile(workingDir,'FFTImages',filename);
imwrite(FFTImg,fullname);    % Write out to a PNG file (img1.png, img2.png, etc.)
end

% make a normalized "average" mask
sumImg = mat2gray(sumGrayMatrix);
fullname = fullfile(workingDir,'normalized_mask.png');
imwrite(sumImg,fullname)    % Write out to a PNG file (img1.png, img2.png, etc.)

% set the thresholds for high/low
maxNum = max(max(sumGrayMatrix));
threshmin = 0.15*maxNum;
threshmax = maxNum - (0.05*maxNum);

%high thresholding
highThreshGrayMatrix = zeros(x,y);
for jj = (1:x)
    for kk = (1:y)
        if sumGrayMatrix(jj,kk) > threshmax
            highThreshGrayMatrix(jj,kk) = 255;
        end
    end
end
jj = 1;
kk = 1;

sumImg = mat2gray(highThreshGrayMatrix);
fullname = fullfile(workingDir,'high_mask.png');
imwrite(sumImg,fullname)    % Write out to a PNG file (img1.png, img2.png, etc.)

%low thresholding
lowThreshGrayMatrix = zeros(x,y);
for jj = (1:x)
    for kk = (1:y)
        if sumGrayMatrix(jj,kk) < threshmin
            lowThreshGrayMatrix(jj,kk) = 255;
        end
    end
end
jj = 1;
kk = 1;

sumImg = mat2gray(lowThreshGrayMatrix);
fullname = fullfile(workingDir,'low_mask.png');
imwrite(sumImg,fullname)    % Write out to a PNG file (img1.png, img2.png, etc.)



% Make a video to output
imageNames = dir(fullfile(workingDir,'images','*.png'));
imageNames = {imageNames.name}';

%outputVideo = VideoWriter(fullfile(workingDir,'video_out.avi'));
outputVideo = VideoWriter(fullfile('C:\Users\aschluchter\Desktop\video_out.avi'));

outputVideo.FrameRate = thisvideo.FrameRate;
open(outputVideo)

for ii = (1:length(imageNames))
   img = imread(fullfile(workingDir,'images',imageNames{ii}));
   writeVideo(outputVideo,img)
end

close(outputVideo)


% Make another video to output: the FFT
imageNames = dir(fullfile(workingDir,'FFTImages','*.png'));
imageNames = {imageNames.name}';

%outputVideo = VideoWriter(fullfile(workingDir,'video_out.avi'));
outputVideo = VideoWriter(fullfile('C:\Users\aschluchter\Desktop\FFTvideo_out.avi'));

outputVideo.FrameRate = thisvideo.FrameRate;
open(outputVideo)

for ii = (1:length(imageNames))
   img = imread(fullfile(workingDir,'FFTImages',imageNames{ii}));
   writeVideo(outputVideo,img)
end

close(outputVideo)

