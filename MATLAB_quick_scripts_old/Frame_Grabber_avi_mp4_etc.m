obj = VideoReader('test.mp4');
k = 1;
framerate = obj.FrameRate
dur = obj.Duration
num_frames = framerate*dur

for k = 1 : num_frames  %fill in the appropriate number
  this_frame = read(obj, k);
  thisfig = figure();
  thisax = axes('Parent', thisfig);
  image(this_frame, 'Parent', thisax);
  title(thisax, sprintf('Frame #%d', k));
end