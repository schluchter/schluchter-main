#! /usr/bin/env python
# (in general, use 'shebang' tag above for python 2 or 3 as needed)
# this code uses Python 3

"""
Dicom Time Frame Extracting Code
...
This code (modified by Hannah) must be placed in a folder containing folders of dicom files
Once run, in each folder, the number of time frames to be extracted is determined
by dividing all TFs by a factor (in code: eg, N = 3), and each of the middle TF 'group'
will be placed into a folder like the previous, and called "XXXX_extraction".
Note that the files are only moved, and not copied. This improves run time significantly.

"""

import os, sys, shutil

# dividing number
N = 3
# get the current working dir
mainDir = os.getcwd()

# flag for setting up folders initially
opening = 1

print("Extraction in progress...")


for root, dirs, files in os.walk(mainDir):
    print("root: ",root)
    print("dirs: ",dirs)
    print("files:" ,files)
    # For all subdirs in mainDir, make a copy called "<subdir_name>_extracted"
    for dir in dirs:
        full_list = []
        if not os.path.exists(mainDir+os.sep+dir+"_extraction"):
                os.mkdir(mainDir+os.sep+dir+"_extraction")
        dircont = os.listdir(dir)
        print dircont
        for name in dircont:
            #print("name: ",name)
            full_list.append(name)
        n = len(full_list)
        print("n: ",n)
        subset = n/N
        # naturally a float, change to int
        subset = int(subset)
        print("subset number: ",subset)
        sorted_list = sorted(full_list)
        # extract the middle third of the list
        extraction_list = sorted_list[subset:(2*subset)]
        print("extraction list: ",extraction_list)
        for eachfile in extraction_list:
            old_name = mainDir+os.sep+dir+os.sep+eachfile
            new_name = mainDir+os.sep+dir+"_extraction"+os.sep+eachfile
            shutil.copyfile(old_name, new_name)

print("Extraction complete.")
