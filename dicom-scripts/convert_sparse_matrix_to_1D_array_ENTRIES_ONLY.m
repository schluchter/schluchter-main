% Get all non-zero values from sparse matrix:
% For all values in A, append to new 1d array B

range = numel(A);
B = [];

% for all elements of A
for i = 1:range
    % if the entry is not 0
    if A(i) ~= 0
        % add the entry to B
        B = cat(1,B,A(i));
    end
end
% get rid of that whole "sparse" thing
B = full(B);