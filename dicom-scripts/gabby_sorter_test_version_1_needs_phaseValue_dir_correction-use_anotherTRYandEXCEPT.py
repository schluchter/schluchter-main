import dicom, os

# Function of this script: sort dicoms for OSIRIX and into ordered subfolders
# assumptions:
# all files end in ".dcm" or ".dicom"
# all files are in 1 folder (subfolder functionality is in currently in testing)
# all files (should) have cardiac phases in all files.
# NOTE: this is checked in the first step of the program


# To use: place this file into the folder of dicom files to be sorted and run it


#--------------------------------------------------------------------
# Example useage of commands:

# EXAMPLE READ
#ds = dicom.read_file("IM-0001-0001-0001.dcm")
#print(ds)

#PY3 VERSION COMMANDS
#print(list(ds.keys())[0])      # key of "first" element
#print(list(ds.values())[0])    # value of "first" element
#print(list(ds.items())[0])     # (key, value) tuple of "first" element

#BOTH OF THESE COMMANDS PRINT THE SAME THING!
#print(ds.PatientsName)
#print(ds[0x10,0x10].value)

# Example command to change field entries
#ds.PatientID = "8675309"

#--------------------------------------------------------------------


# get the current working dir
mainDir = os.getcwd()

# establish flag to denote errors and skips
skip_flag = 0

# Make empty list "phases_list"
phases_list = []


# Verify all files are valid ---------------------------------------------------            


print("Checking for valid headers in all files.")
print("Approximate wait time: No more than roughly 10 seconds per 3000 files...")


# walk all the subdirs
for root, dirs, files in os.walk(mainDir):
    for name in files:
        #print(name)
        if name.endswith((".dicom", ".dcm")):
            dcmFile = root + os.sep + name

            # Read file
            #print("step 1")
            # ERROR HANDLER
            try:
                ds = dicom.read_file(dcmFile)
            except:
                print("Error log: Invalid or corrupted file: ")
                print(dcmFile)
                print("Skipping file. Still processing...")
                skip_flag = 1
                #break
            
            # Read Acquisition Date
            #print("step _")
            # ERROR HANDLER
            try:
                phaseValue = ds.NominalPercentageofCardiacPhase
            except:
                print("Error log: Could not find Nominal Percentage of Cardiac Phase field in: ")
                print(dcmFile)
                print("Skipping file. Still processing...")
                skip_flag = 1
                #break

            # To phases_list, copy over the cardiac phase
            phaseDir = "Cardiac_phase_%"+str(phaseValue)
            phases_list.append(phaseDir)
            #print(phases_list)
            #print("STEP_3")
            #input()

# Remove all non-unique entries in list
phases_list = list(set(phases_list))
#print(phases_list)
#print("STEP_3.5")
#print()


#print("step ?")
# Write the file
if skip_flag == 1:
    print()
    print("There appear to be dicom files without proper cardiac phase information in this folder.")
    print("If you are sure you want to continue, press Enter. I HIGHLY recommend you do not, and instead close the window or end the program, and contact Andrew Schluchter, but feel free to continue if you're certain. I ain't your boss...")
    input()
    skip_flag = 0
else:
    print()
    print("All files verified valid. Sorting now.")
    print("Approximate wait time: No more than roughly 80 seconds per 3000 files...")


# Modify and sort files ---------------------------------------------------            


# make each cardiac phase into a folder
for p in phases_list:
    try:
        os.mkdir(p)
    except:
        print()
#print("STEP_4")
#print()
#input()
    

# walk all the subdirs
for root, dirs, files in os.walk(mainDir):
    for name in files:
        #print(name)
        if name.endswith((".dicom", ".dcm")):
            dcmFile = root + os.sep + name

            # Read file
            #print("step 1")
            # ERROR HANDLER
            try:
                ds = dicom.read_file(dcmFile)
            except:
                print("Error log: Invalid or corrupted file: ")
                print(dcmFile)
                print("Skipping file. Still processing...")
                skip_flag = 1
                #break
            
            # Read Nominal Percentage of Cardiac Phase
            #print("step _")
            # ERROR HANDLER
            try:
                phaseValue = ds.NominalPercentageofCardiacPhase
            except:
                print("Error log: Could not find Nominal Percentage of Cardiac Phase field in: ")
                print(dcmFile)
                print("Skipping file. Still processing...")
                skip_flag = 1
                #break
            
            # Set Series Instance UID field
            #print("step _")
            # ERROR HANDLER
            try:
                ds.SeriesInstanceUID = str(phaseValue)
            except:
                print("Error log: Could not find Series UID field in: ")
                print(dcmFile)
                print("Skipping file. Still processing...")
                skip_flag = 1
                #break

            # Set Series Description field
            #print("step _")
            # ERROR HANDLER
            try:
                ds.SeriesDescription = "Cardiac Phase %: "+str(phaseValue)
            except:
                print("Error log: Could not find Series Description field in: ")
                print(dcmFile)
                print("Skipping file. Still processing...")
                skip_flag = 1
                #break

            #print("step ?")
            # Write the file
            if skip_flag == 0:
                #print(root)
                ds.save_as(dcmFile)
                #print(dcmFile)
                newName = os.path.join(root, "Cardiac_phase_%"+str(phaseValue),name)
                #print(newName)
                os.rename(dcmFile, newName)
                #input()

print()
print("Done. Close window, or press ENTER to exit (I know that sounds weird, but trust me on this).")
input()
