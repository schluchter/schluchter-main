import dicom, os, pandas, xlsxwriter
# I may not need these in the final version:
import glob, os.path, copy

# OPERATOR ACTION: place this script in the folder in question, and run script

# Global variables:
mainDir = os.getcwd()
# Options:
minDatasetName = '' # initialization value; may be changed later

def main():
    #test_data = [['model0','studyDesc0','studyID0'],['model1','studyDesc1','studyID1'],['model0','studyDesc0','studyID0']]
    #testFrame = pandas.DataFrame(test_data,columns=['Model','StudyDesc','StudyID'])
    #print("generating testFrameOutput: ")
    #print(getMostCommon(testFrame))
    dir_list = []
    dir_list_pre = next(os.walk('.'))[1]
    #print(dir_list)
    print("Enter the minimum dataset to pull from, then press Enter.")
    print("To pull from all datasets, just press Enter:")
    minDatasetName = input()
    # for all entries in dir_list, remove entries 'less than' minDatasetName
    for entry in dir_list_pre:
        if entry >= minDatasetName:
            dir_list.append(entry)
    print("Extracting data now...")
    #print("Printing dir_list_pre: ")
    #print(dir_list_pre)
    #print("Printing dir_list: ")
    #print(dir_list)
    finalDataframe = pullData(dir_list)
    #print("Final dataframe: ")
    #print(finalDataframe)
    # Create and export the data to excel file
    writer = pandas.ExcelWriter('output.xlsx')
    finalDataframe.to_excel(writer,'Sheet1')
    writer.save()
def getManufacturersModelName(diData, diFile):
    # Read field
    #print("step 1")
    # ERROR HANDLER
    try:
        field = diData.ManufacturersModelName
        #print("Manufacturer: "+field)
        output_field = str(field)
    except:
        #print("Error log: Could not find manufacturer field in: ")
        #print(diFile)
        #print("Skipping file. Still processing...")
        skip_flag = 1
        #break
        output_field = "error"
    return(output_field)
def getStudyDescription(diData, diFile):
    # Read field
    #print("step 2")
    # ERROR HANDLER
    try:
        field = diData.StudyDescription
        #print("Study Description: "+field)
        output_field = str(field)
    except:
        #print("Error log: Could not find Study Description field in: ")
        #print(diFile)
        #print("Skipping file. Still processing...")
        skip_flag = 1
        #break
        output_field = "error"
    return(output_field)
def getStudyID(diData, diFile):
    # Read field
    #print("step 3")
    # ERROR HANDLER
    try:
        field = diData.StudyID
        #print("Study ID: "+field)
        output_field = str(field)
    except:
        #print("Error log: Could not find Study ID field in: ")
        #print(diFile)
        #print("Skipping file. Still processing...")
        skip_flag = 1
        #break
        output_field = "error"
    return(output_field)
def getMostCommon(self, fullName):
    # input datframe, output is a small dataframe that is used to UPDATE the larger dataframe (OUTSIDE of this function!)
    # thus, input is large single-patient dataframe, output is tiny single-patient dataframe
    modeDataFrame = self.mode()
    name = fullName.replace(mainDir+os.sep,'')
    #print("mode dataframe:")
    #print(modeDataFrame)
    #input()
    model_Com = modeDataFrame.iloc[0][0]
    studyDesc_Com = modeDataFrame.iloc[0][1]
    studyID_Com = modeDataFrame.iloc[0][2]
    out_data = [[model_Com],[studyDesc_Com],[studyID_Com]]
    outputFrame = pandas.DataFrame(out_data,columns=[name])
    #outputFrame = pandas.DataFrame(out_data,columns=[name], index=[0,1,2])
    #print("output frame:")
    #print(outputFrame)
    return(outputFrame)
def pullData(dir_list):
    # Determine minimum dataset to pull from
    mainDataFrame = pandas.DataFrame()
    # walk all the subdirs
    mainList = dir_list
    #print(mainList)
    for dir_list_entry in mainList:
        fullPath = mainDir+os.sep+dir_list_entry
        #print(fullPath)
        subDataFrame = pandas.DataFrame(columns=['StudyID','Model','StudyDesc'])
        for root, dirs, files in os.walk(fullPath):
            for name in files:
                # IMPORTANT: the double parantheses are needed here when searching for multiple sub-strings
                if name.endswith((".dicom", ".dcm")):
                    dcmFile = root + os.sep + name
                    #print(dcmFile)
                    # ERROR HANDLER
                    try:
                        ds = dicom.read_file(dcmFile)
                        #print("Name: ")
                        #print(name)
                        #print("root: ")
                        #print(root)
                        #print("Getting Dataset name...")
                        #print(getDatasetName(root))
                        #datasetName = getDatasetName(root)
                        studyID = getStudyID(ds, dcmFile)
                        modelName = getManufacturersModelName(ds, dcmFile)
                        studyDesc = getStudyDescription(ds, dcmFile)
                        #print(modelName)
                        #print(studyDesc)
                        #print(studyID)
                        subDataFrame = subDataFrame.append({'StudyID':studyID,'Model': modelName,'StudyDesc':studyDesc}, ignore_index=True)
                        #print("subDataFrame:")
                        #print(subDataFrame)
                        # NOTE: this cluster being within the same TRY means any error with the file,
                        # even just a missed field, then the data will not being taken from the file.
                    except:
                        print("Error log: "+dcmFile+" is not a valid dicom file.")
                        #break
        #mainDataFrame.append() with getMostCommon
        #print("subDataFrame right before sending to mode method:")
        #print(subDataFrame)
        if not subDataFrame.empty:
            mode_df = getMostCommon(subDataFrame, fullPath)
            #print("mode_df:")
            #print(mode_df)
            mainDataFrame = pandas.concat([mainDataFrame, mode_df], axis=1)
            #print('mainDataFrame after combining with mode_df:')
            #print(mainDataFrame)
            #input()
        else:
            print("")
            #print("Empty dataset; skipping...")
        print("Patient extracted or skipped: "+dir_list_entry)

    # DEBUGGING RETURN VALUES
    #test_data = [['model0','studyDesc0','studyID0'],['model1','studyDesc1','studyID1'],['model2','studyDesc2','studyID2']]
    #outputDataframe = pandas.DataFrame(test_data,columns=['Model','StudyDesc','StudyID'])
    #return(outputDataframe)
    return(mainDataFrame)

if __name__ == '__main__':
    main()
