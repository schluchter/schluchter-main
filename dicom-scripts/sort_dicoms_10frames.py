# This script is designed to do the following:
# 1. Gather all dicom files in every sub-directory of the chosen directory
# 2. Count the number of dicoms [num_dicoms] and divide into groups of 20 or 21, depending on which [num_frames] gives %==0
# 3. Divide num_dicoms/num_frames to get [dicoms_per_frame]
# 4. Create parallel folder called "sorted" and in the new folder, create folders "01" through [num_frames] NOTE: use precision of 2 for naming folders
# 5. For i = 1:num_frames, 
#		for ii = 1:dicoms_per_frame
#			move [dicom file][ii] into folder[i]

import glob, os, os.path, copy


# DEBUG: enter number of folders to divide files into:
#num_frames = 21
# DEBUG: enter dicom files per time frame:
#dicoms_per_frame = 206


mainDir = os.getcwd()
#print(mainDir)

subDirs = os.listdir(os.getcwd())
subDirs.remove('sort_dicoms_10frames.py')
#print(subDirs)
num_dirs = len(subDirs)


allFilesListPath = []
allFilesListFile = []

for i in range(0, num_dirs):
	curDir = mainDir+'\\'+subDirs[i]
	#print(curDir)
	# GET ALL THE FILES, PUT INTO 1 LIST
	files_in_curDir = os.listdir(curDir)
	num_dicoms_in_curDir = len(os.listdir(curDir))
	#print(num_dicoms_in_curDir)
	for ii in range(0, num_dicoms_in_curDir):
		allFilesListPath.append(curDir+'\\'+files_in_curDir[ii])
		allFilesListFile.append(files_in_curDir[ii])
#print(allFilesListFile[299])
#print(allFilesListPath[299])

num_dicoms = len(allFilesListFile)

# MANUAL SET OF FRAMES
num_frames = 10

dicoms_per_frame = num_dicoms/num_frames
dicoms_per_frame = int(dicoms_per_frame)

os.mkdir('sorted')
for i in range(1, num_frames+1):
	stringi = str(i)
	if i<10:
		os.mkdir('sorted\\'+'0'+stringi)
	else:
		os.mkdir('sorted\\'+stringi)


frame_count = 0

for i in range(1, num_frames+1):
	for ii in range(0, dicoms_per_frame):
		stringi = str(i)
		if i<10:
			#os.mkdir('sorted\\'+'0'+stringi)
			# move allFilesListPath+filename[frame_count+ii] to folder '0'+stringi
			newPath = mainDir+'\\'+'sorted'+'\\'+'0'+stringi+'\\'+allFilesListFile[ii+frame_count]
			oldPath = allFilesListPath[ii+frame_count]
			print(newPath)
			print(oldPath)
			os.rename(oldPath, newPath)
		else:
			#os.mkdir('sorted\\'+stringi)
			# move files to folder stringi
			newPath = mainDir+'\\'+'sorted'+'\\'+stringi+'\\'+allFilesListFile[ii+frame_count]
			oldPath = allFilesListPath[ii+frame_count]
			os.rename(oldPath, newPath)
	frame_count = frame_count + dicoms_per_frame

for i in range(0, num_dirs):
	os.rmdir(subDirs[i])
