import os, dicom, pandas, numpy
import seaborn, matplotlib
# NOTES: WILL NEED TO BRING "dicom" LIB FOLDER WHEN SCRIPT IS RUN

print("Processing....")

# get the current working dir
cwd = os.getcwd()
df = pandas.DataFrame()

# Open every dicom file
for root, dirs, files in os.walk(cwd):
    for name in files:
        if name.endswith((".dicom", ".dcm")):
            dcmFile = root + os.sep + name
            # Get dicom header data and convert to dataframe
            ds = dicom.read_file(dcmFile)
            attributes = ds.dir()
            data = []
            for entry in attributes:
                try:
                    # Have to import the data explicity as strings, otherwise all hell breaks loose
                    data.append(str(ds.data_element(entry).value))
                except:
                    #print("d'oh!")
                    data.append(numpy.nan)
            table = [attributes,data]
            new_df = pandas.DataFrame(table)
            # Set the column identifiers to the data in the first "row"
            new_df.columns = new_df.iloc[0]
            # Delete the empty row
            new_df = new_df.drop([0])
            # Make the filepath the first columns
            relativePath = dcmFile.replace(cwd,'')
            new_df['filepath'] = relativePath
            df = df.append(new_df, ignore_index=True, verify_integrity=False)

# Move the 'filepath' column to the front
# get the list of columns
cols = df.columns.tolist()
# reorder the list of columns
cols.insert(0, cols.pop(cols.index('filepath')))
# reorder the dataframe to match
df = df.reindex(columns= cols)

# Make a copy of the dataframe that's purely boolean
# copy the dataframe
df_copy = df.copy()
# replace with all boolean values

for i in range(df.shape[0]):
    for col in cols:
        if pandas.isnull(df[col].iloc[i]):
            ##df_copy[col].iloc[i] = "NULL ENTRY"
            df_copy[col].iloc[i] = 0
            #df_copy.loc[i, col] = 0
        else:
            df_copy[col].iloc[i] = 1
"""
# To convert to purely boolean fields; has trouble with mixed entries and 0's
for entry in cols:
#    if (df[entry] == None):
#        df_copy[entry] = False
    df_copy[entry] = df[entry].astype('bool')
"""

# Save dataframe as xlsx, csv
writer = pandas.ExcelWriter(cwd+os.sep+'output_file_original.xlsx', engine='xlsxwriter')
df.to_excel(writer, sheet_name='Sheet1')
try:
    writer.save()
except: print("Error saving file. Is it already open? Was the directory moved?")
writer.close()

# Aaaand save the boolean dataframe as xlsx, csv
writer = pandas.ExcelWriter(cwd+os.sep+'output_file_boolean.xlsx', engine='xlsxwriter')
df_copy.to_excel(writer, sheet_name='Sheet1')
try:
    writer.save()
except: print("Error saving file. Is it already open? Was the directory moved?")
writer.close()

"""
# Seems like I need a better way to visualize huge data :/
seaborn.set()
#f, ax = matplotlib.pyplot.subplots(figsize=(9, 6))
matplotlib.pyplot.subplots(figsize=(25,15))
seaborn.heatmap(df_copy, annot=False, fmt="d", linewidths=.5, cbar=False)#, square=True)
matplotlib.pyplot.show()
"""

# Finish it all
print("Done. Press ENTER to exit.")
input()
