import os, glob, os.path, copy
# OPERATOR ACTION: place this script in the folder in question, and run script

# Get a list called "files_list" of all files
# get the current working dir
mainDir = os.getcwd()
# get the subdirectories and files
files_list = os.listdir()
#print(subdirs)

print("STEP_1")

# remove all .py, .DS_Store, and .stl file(s) from files_list
dirList = [ f for f in os.listdir(".") if f.endswith(".py") or f.endswith(".stl") or f.endswith(".DS_Store")]
for f in dirList:
    files_list.remove(f)
#print(subdirs)

print("STEP_2")

# Make empty list "frames_list"
frames_list = []
# To frames_list, copy over first 7 characters from files_list
#print(files_list)
for f in files_list:
    f = f[:7]
    frames_list.append(f)
#print(frames_list)

print("STEP_3")

# Remove all non-unique entries in list
frames_list = list(set(frames_list))
#print(frames_list)

# Remove all but last 2 characters in each entry of frames_list and
# make each into a folder (should leave 01, 02, ... 20, etc. as folders)
for f in frames_list:
    f = f[5:]
    os.mkdir(f)

print("STEP_4")

# For each entry (now unique!) in frames_list, rename all entries that begin
# with that entry from <filename> to <entry>+'\\'+<filename>
for frame in frames_list:
    for f in files_list:
        if f.startswith(frame):
            #print(os.getcwd())
            os.rename(f, frame[5:] + '\\' + f)
