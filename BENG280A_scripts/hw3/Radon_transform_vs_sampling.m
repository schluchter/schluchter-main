% "Radon_transform_vs_sampling" demo uses the intrinsic MATLAB radon and 
% iradon to demonstrate sinograms, and the effect of undersampling...
%
% WARNING: For matlab matrix I(i,j), i=row number (top to bottom), 
%                                    j=column number (left to right)
%
% (This is not a "natural" spatial coordinate system, like I(x,y))
%
% Elliot McVeigh, BENG280A, 2018
%
clear all;
close all;

%choose image to use from 3 options (set the option you want to 1):

% small squares
small_squares=0;
% Shepp-Logan phantom
shepplogan=1;
% human coronary CTA
coronary_CTA=0;


% make a blank 256x256 image array
I=zeros(256,256);

if small_squares==1
%
%  make 3x3 squares, separated by 1 pixel at image center
%
I(127:129,127:129)=1.0;
I(131:133,127:129)=1.0;

%  make 3x3 squares, separated by 1 pixel, on the image diagonal
%
I(63:65,63:65)=1.0;
I(66:68,66:68)=1.0;

% and, place two 3x3 squares at an offset position x_0, y_0
%
x_0=28;
y_0=128;
I(x_0-1:x_0+1,y_0-1:y_0+1)=1.0;

x_0=32;
y_0=128;
I(x_0-1:x_0+1,y_0-1:y_0+1)=1.0;
end  %small_squares=1

%
% alternative image is the shepp-logan phantom

if shepplogan==1
I=phantom(256);
end

% alternative image is a single slice from coronary CTA
%
if coronary_CTA==1
%Iread=imread('/Users/emcveigh/Documents/BENG280A/2017/example_coronary_patient_000.tif');
Iread=imread('C:\Users\schluchter\Desktop\software and programming\schluchter-repo.git\BENG280A_scripts\hw3\example_coronary_patient_000.tif');
I=double(Iread);
I=(I-30720);
Imax=max(max(I));
I=I/Imax;
end

figure('Name','original image','Position',[1 420 400 400]); imagesc(I); title('original image'); axis('square'); colormap('gray');

% ADJUSTABLE PROJECTION SAMPLING PARAMETERS - the set of projections used to produce the image
%
first_projection_angle=0;
last_projection_angle=180;
%delta_theta=16.0;
delta_theta=1.0;

figure('Name','parameters of run','Position',[1201 404 240 401]);
text(0.1,0.9,sprintf('delta theta = %0.2f',delta_theta),'Fontsize', 12)
text(0.1,0.8,sprintf('angles %0.2f through %0.2f',first_projection_angle,last_projection_angle),'Fontsize', 12)

% set the number of theta views
theta=first_projection_angle:delta_theta:last_projection_angle;

% compute the Radon transform of the image at the angles specified
rad_I=radon(I,theta);
figure('Name','g(l,theta)','Position',[401 420 400 400]); imagesc(rad_I); title('projections g(l,theta) - Radon Transform'); axis('square'); xlabel('projection angle theta'); ylabel('linear displacement - l');
[N_l,N_theta]=size(rad_I);

%
% compute the image that would be reconstructed from the specified views
inv_rad_I=iradon(rad_I,theta);
figure('Name','noise free iradon{g(l,theta)}','Position',[801 420 400 400]); imagesc(inv_rad_I); title('Inverse Radon of noise free Projections'); axis('square'); colormap('gray')


% Artifacts from missing samples
%
if 1==1
% compute the image that would result if there are some missing contiguous
% data segments... or try other data reduction techniques...
%
art_rad_I=rad_I;
art_rad_I(:,64:128)=0.0;
figure('Name','g(l,theta) - missing projections','Position',[401 20 400 400]); imagesc(art_rad_I); title('projections g(l,theta) - Radon Transform'); axis('square'); xlabel('projection angle theta'); ylabel('linear displacement - l');
inv_rad_I=iradon(art_rad_I,theta);
figure('Name','iradon{g(l,theta) - missing projections}','Position',[801 20 400 400]); imagesc(inv_rad_I); title('Inverse Radon, with missing projections'); axis('square'); colormap('gray')
%
end
%%