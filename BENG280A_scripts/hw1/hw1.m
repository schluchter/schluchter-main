% Andrew Schluchter
% BENG HW #1

% 1. make the mat
res = 128;
hw1Mat = zeros(res);

% 2. build a cross
bigBoundLow = floor(res/3);
bigBoundHigh = 2*bigBoundLow;
littleBoundLow = floor(bigBoundLow/3);
littleBoundHigh = 2*littleBoundLow;
for i = bigBoundLow:bigBoundHigh
    for ii = bigBoundLow:bigBoundHigh
        if ((bigBoundLow+littleBoundLow)<= i && i<=(bigBoundLow+littleBoundHigh)) || ((bigBoundLow+littleBoundLow)<=ii && ii<=(bigBoundLow+littleBoundHigh))
            hw1Mat(i,ii) = 1;
        end
    end
end

% 3. view as image, mod the colorbar/colormap
%imagesc(hw1Mat);
%colorbar('Ticks',[0,1],'TickLabels',{'Background','Object'});
%colormap(parula(2));

% 4. Build 11x11 Gaussian kernel, for center x0, y0, and width parameter w
%test_kernel = fspecial('gaussian', [11 11], 4);
[x,y] = meshgrid(-5:1:5, -5:1:5);
x0 = 0;
y0 = 0;
w = 1;
% w not yet defined; will do so @ time of convolving
fg = exp(-(((x-x0).^2)/w)+(((y-y0).^2)/w));

% 5. give it some parameters and convolve it
% (go through different w settings and plot them)
subplot(2,2,1);
w = .1;
fg = exp(-(((x-x0).^2)/w)+(((y-y0).^2)/w));
convhw1Mat = conv2(hw1Mat,fg);
imagesc(convhw1Mat);
title('w = 0.1')

subplot(2,2,2);
w = 1;
fg = exp(-(((x-x0).^2)/w)+(((y-y0).^2)/w));
convhw1Mat = conv2(hw1Mat,fg);
imagesc(convhw1Mat);
title('w = 1')

subplot(2,2,3);
w = 10;
fg = exp(-(((x-x0).^2)/w)+(((y-y0).^2)/w));
convhw1Mat = conv2(hw1Mat,fg);
imagesc(convhw1Mat);
title('w = 10')

subplot(2,2,4);
w = 100;
fg = exp(-(((x-x0).^2)/w)+(((y-y0).^2)/w));
convhw1Mat = conv2(hw1Mat,fg);
imagesc(convhw1Mat);
title('w = 100')

% 6. See printout(s)