% PART 2:
span = -10:.5:10;
[x,y] = meshgrid(span,span);
kx = .02;ky = .02;
g = exp(j*2*pi*(kx*x + ky*y));
figure(2);clf;
quiver(x,y,real(g),imag(g));
grid;


% PART 1:
%j = sqrt(-1);
%x = -10:.1:10;
%kx = .1;
%g = exp(j*kx*2*pi*x);
%figure(1);clf;
%quiver3(x,zeros(size(x)),zeros(size(x)),zeros(size(x)),real(g),imag(g));