% Andrew Schluchter
% HW 2

load BE280Ahw1im.mat;
%whos
%imagesc(Mimage);
Mf = fft2(Mimage);
%plot(Mf)
Mf = fftshift(Mf);
plot(Mf);

imagesc(abs(Mf));
b = min(min(abs(Mf)));
a = max(max(abs(Mf)));
format long g
a/b;

cmin = 200;
cmax = 1e6;
imagesc(abs(Mf),[cmin, cmax])

subplot(2,2,1)
imagesc(real(Mf))
subplot(2,2,2)
imagesc(imag(Mf))
subplot(2,2,3)
imagesc(angle(Mf))

close all
span = 128 + (-20:20);
mesh(abs(Mf(span,span)));

subplot(2,2,1)
imagesc(abs(Mf(span,span)));

res_span = 129 + (-16:16);
Mf2 = zeros(256,256);
Mf2(:,res_span) = Mf(:,res_span);
Mf2 = fftshift(Mf2);
M_resx = ifft2(Mf2);
subplot(2,2,2)
imagesc(abs(M_resx));

res_span = 129 + (-16:16);
Mf2 = zeros(256,256);
Mf2(res_span,:) = Mf(res_span,:);
Mf2 = fftshift(Mf2);
M_resy = ifft2(Mf2);
subplot(2,2,3)
imagesc(abs(M_resy));

res_span = 129 + (-16:16);
Mf2 = zeros(256,256);
Mf2(res_span,res_span) = Mf(res_span,res_span);
Mf2 = fftshift(Mf2);
M_resxy = ifft2(Mf2);
subplot(2,2,4)
imagesc(abs(M_resxy));

close all
subplot(2,3,1)
ky = 129; kx = 129;
Mfzero = Mf;
Mfzero(ky,kx) = 0;
iMFzero = ifft2(fftshift(Mfzero));
imagesc(iMFzero);

subplot(2,3,2)
kx = 1:256; ky = 129 + (-16:16);
Mfzero = Mf;
Mfzero(ky,kx) = 0;
iMFzero = ifft2(fftshift(Mfzero));
imagesc(iMFzero);

subplot(2,3,3)
kx = 129 + (-16:16); ky = 1:256;
Mfzero = Mf;
Mfzero(ky,kx) = 0;
iMFzero = ifft2(fftshift(Mfzero));
imagesc(iMFzero);

subplot(2,3,4)
kx = 129 + (-16:16); ky = 129 + (-16:16);
Mfzero = Mf;
Mfzero(ky,kx) = 0;
iMFzero = ifft2(fftshift(Mfzero));
imagesc(iMFzero);

subplot(2,3,5)
kx = 1:2:256; ky = 1:256;
Mfzero = Mf;
Mfzero(ky,kx) = 0;
iMFzero = ifft2(fftshift(Mfzero));
imagesc(iMFzero);

close all
Mfspike = Mf;
spike = 100e6;
%Mfspike(ky,kx) = Mf(ky,kx) + spike;
%iMFspike = ifft2(fftshift(Mfspike));

ky = 129; kx = 129;
Mfspike(ky,kx) = Mf(ky,kx) + spike;
iMFspike = ifft2(fftshift(Mfspike));
subplot(2,2,1)
imagesc(Mfspike);
%imagesc(abs(Mfspike));

kx = 161; ky = 129;
Mfspike(ky,kx) = Mf(ky,kx) + spike;
iMFspike = ifft2(fftshift(Mfspike));
subplot(2,2,2)
%imagesc(Mfspike);
imagesc(abs(Mfspike));

kx = 161; ky = 161;
Mfspike(ky,kx) = Mf(ky,kx) + spike;
iMFspike = ifft2(fftshift(Mfspike));
subplot(2,2,3)
%imagesc(Mfspike);
imagesc(abs(Mfspike));