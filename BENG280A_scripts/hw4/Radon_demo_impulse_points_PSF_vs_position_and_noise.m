% Reson_demo_impulse_points_PSF_position_and_noise.m
% Radon demo uses the intrinsic MATLAB radon and iradon to demonstrate
% sinograms, SNR, artifacts, position dependent PSF, etc.
%
% Elliot McVeigh, BENG280A, 2018
%
clear all;
close all;
% make an image with two simple 3x3 squares, separated by 1 pixel 
%
I=zeros(256,256);
%
% place the impulse signals at an offset position x_0, y_0
%
x_0=128;
y_0=128;
I(x_0,y_0)=1.0;

x_0=64;
y_0=64;
I(x_0,y_0)=1.0;

x_0=32;
y_0=32;
I(x_0,y_0)=1.0;

x_0=32;
y_0=128;
I(x_0,y_0)=1.0;

x_0=64;
y_0=128;
I(x_0,y_0)=1.0;

x_0=8;
y_0=128;
I(x_0,y_0)=1.0;

figure('Name','original image'); imagesc(I); title('original image'); axis('square'); colormap('gray');

% ADJUSTABLE PARAMETERS - the set of projections used to produce the images

first_projection_angle=0;
last_projection_angle=180;
delta_theta=1.0;

%change the level of the noise on raw data...
%
data_peak_to_noise_ratio=1.0;

% change text to reflect parameters of the particular run
figure('Name','parameters of run','Position',[1201 404 240 401]);
text(0.1,0.9,'delta theta = 1.0','Fontsize', 14)
text(0.1,0.7,'angles 0 through 180','Fontsize', 14)
text(0.1,0.5,'data SNR = 1.0','Fontsize', 14)
text(0.1,0.3,'recon kernel = hamming','Fontsize', 14)


% set the number of theta views
theta=first_projection_angle:delta_theta:last_projection_angle;
%
rad_I=radon(I,theta);
%figure('Name','g(l,theta)'); imagesc(rad_I); title('projections g(l,theta) - Radon Transform'); axis('square'); xlabel('projection angle theta'); ylabel('linear displacement - l');
[N_l,N_theta]=size(rad_I);
%
inv_rad_I=iradon(rad_I,theta,'Ram-Lak');

% ALTERED CODE % ==========================================================
%inv_rad_I=iradon(rad_I,theta,'Ram-Lak');
% /ALTERED CODE % =========================================================

figure('Name','noise free iradon{g(l,theta)}','Position',[1 420 400 400]); imagesc(inv_rad_I); title('Inverse Radon of noise free Projections'); axis('square'); colormap('gray')

% ADDED CODE % ============================================================
halfMax = 0.5*max(size(inv_rad_I));
inv_rad_I_PSF=inv_rad_I(halfMax,:);
figure('Name','PSF of noise free iradon{g(l,theta)}','Position',[1 100 400 400]); plot(inv_rad_I_PSF); title('PSF of Noise-Free iRadon'); axis('square'); colormap('gray')
inv_rad_I_PSF=inv_rad_I(halfMax,(halfMax-5):(halfMax+5));
figure('Name','Zoomed - PSF of noise free iradon{g(l,theta)}','Position',[410 100 400 400]); plot(inv_rad_I_PSF); title('Zoomed - PSF of Noise-Free iRadon'); axis('square'); colormap('gray')
% /ADDED CODE % ===========================================================

%
% add noise to the "raw data"
%
data_peak=max(max(rad_I));
raw_noise=rand(N_l,N_theta);
%figure('Name','raw noise'); imagesc(raw_noise); title('Raw Noise to add to projections'); axis('square');
%
scaled_noise=raw_noise*(data_peak/data_peak_to_noise_ratio); 
nse_rad_I=rad_I+scaled_noise;
%
% try smoothing the raw data to increase the SNR
%
smoothing=0
if smoothing
h=ones(1,2)/2;
filt_nse_rad_I=conv2(nse_rad_I,h,'same');
% note: the 'same' parameter cuts off extra data post convolution
nse_rad_I=filt_nse_rad_I;
%figure('Name','g(l,theta) * filter'); imagesc(filt_nse_rad_I); title('3x3 Filtered Projections'); axis('square');
end
    
figure('Name','g(l,theta) + scaled_noise','Position',[801 420 400 400]); 
imagesc(nse_rad_I); title('Noisy Projections'); axis('square');
%
inv_nse_rad_I=iradon(nse_rad_I,theta,'hamming');
%
%
figure('Name','iradon{ g(l,theta) + scaled_noise}','Position',[401 420 400 400]); 
imagesc(inv_nse_rad_I); title('Inverse Radon of Noisy Projections'); axis('square'); colormap(gray);


%%