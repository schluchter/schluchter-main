% Andrew Schluchter
% test platform only

%pi = 3.14159;
%syms theta

%f = rectangularPulse((x.^2)/3)
%f = rectangularPulse((x)/3)
%f = rectangularPulse(x)
%f = heaviside(x*2+1)-heaviside(x*2-1)
%f = dirac(x);

%f = (heaviside(x*2+1)-heaviside(x*2-1))*(heaviside(x*2+1)-heaviside(x*2-1))
%fplot(f)

%{
r = cos(3*theta-(pi/6));
for theta = 0:.1:(2*pi)
    polarplot(theta,r)
end
%} 

factor = 3*sqrt(5)/2;
%theta = 0:(pi/36):(2*pi);
%polarplot(theta,factor*(cos(3*theta+(pi/6)))+(6-factor))
%test = factor*(cos(3*(pi/2)+(pi/6)))+(6-factor)
theta = (-pi/6):(pi/36):(pi/2);
polarplot(theta,(cos(3*theta+(pi/2)))+(sin(3*theta+(pi/2)))+(10))