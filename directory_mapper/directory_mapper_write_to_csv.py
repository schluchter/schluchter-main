import os, folderstats, graphviz, pydot
import networkx as nx
import matplotlib.pyplot as plt
from networkx.drawing.nx_pydot import graphviz_layout
import pandas as pd


# Write the main path
cwd = os.getcwd()
#myPath = r'C:\Users\schluchter\Desktop\mesh_file_batch_processing'
#myPath = r'C:\Users\schluchter\Desktop\CViL'
#myPath = r'G:\My Drive\Reference'
#myPath = r'Z:\projects\Andrew\3D_modelling'
print("Print path to map: ")
myPath=str(input())

# Get maximum depth to map
#max_depth = 7
print("Print max depth (an int): ")
max_depth=int(input())

'''
# Note: things seem to work without this, so we'll see I guess :|
print("Windows or UNIX/Linux/Mac? w/u?")
myOS = input()
if myOS=='w':
    OSchar='\\'
if myOS=='u':
    OSchar='/'
'''
OSchar='/'

# Get folder stats into a dataframe
print("Gathering data...")
#df = folderstats.folderstats(myPath, hash_name='md5', ignore_hidden=True)
start_depth = len(myPath.split('\\'))
i=1
id_list=list()
data=list()
name_list=list()
depth_list=list()
for stuff in os.walk(myPath, followlinks=False):
    # Note that 'stuff' is a list of 3 things: path, a list of subdirs, and a list of files
    id_list.append(i)
    data.append(stuff)
    # Split the paths apart to get the depth
    name = stuff[0].split('\\')
    depth_list.append(len(name)-start_depth)
    # Get the last entry in that broken string to get the local name
    name_list.append(name[-1:][0])
    i=i+1
    #print(stuff)

print("Configuring data...")
df = pd.DataFrame(data, columns=['path', 'subdirs', 'files'])
df['id'] = id_list
df['name'] = name_list
df['depth'] = depth_list

dirs=list(map(list, zip(*data)))[0]
subdirs=list(map(list, zip(*data)))[1]
listLength=len(dirs)
#print(listLength)


parents = [0] * listLength

for i in range(listLength):
    #print("Entry i: "+str(i))
    try:
        subdirs[i][0]
        for sd in subdirs[i]:
            fullPath=str(dirs[i])+os.sep+str(sd)
            #print("Child node path: ")
            #print(fullPath)
            #print("Child index/node: ")
            childIndex = dirs.index(fullPath)
            #print(childIndex)
            #print("Updating parents:")
            parents[childIndex]=i+1
            #print(parents[childIndex])
        #print('\n')
    except:
        pass
        #print("no subdirs found")

df['parent'] = parents


# Reorganize the df:
df = df[['name', 'id', 'parent', 'depth', 'path', 'subdirs', 'files']]

# Write the data to .csv
df.to_csv(cwd+os.sep+'directory_output.csv')


# ========================================================================

# TEMPORARY !!!!

# ========================================================================
print("temporarily stopped after writing .csv")
temporary_var=int(input())
# ========================================================================

# TEMPORARY !!!!

# ========================================================================








print("Sorting and writing to .gv output...")

# Sort the index
#df_sorted = df.sort_values(by='id')
# Drop the non-dirs
#df_sorted_dirs = df[df.folder]
# Drop all files and sort:
#df = df.sort_values(by='id')
#df = df[df.folder]

# Open the file and write the header
#print(df_sorted)
file1 = open("directory_map.gv","w+")
file1.write("digraph G_component_0 {"+'\n')
# Write the first line and declare root
file1.write("   graph [ranksep=3, root=1];"+'\n')
#file1.write("Nodes: ")
#file1.write('\n')

# Set up the coloring by level of depth
#colors = ['red','orange','yellow','green','blue','purple']
node_colors = ['red','orange','yellow','greenyellow','aquamarine','lightskyblue','thistle']
edge_colors = ['darkred','darkorange','gold','olivedrab','turquoise','skyblue','mediumorchid']

# Write all the nodes
for index, row in df.iterrows():
    if (row['depth']<max_depth):
        try:
            file1.write('   '+str(row['id'])+' [label="'+str(row['name'])+'", style=filled, color='+node_colors[row['depth']]+'];')
            #file1.write('   '+str(row['id'])+' [label="'+str(row['name'])+'", style=filled, color='+"'"+colors[row['depth']]+"'"+'];')
        except: file1.write('   '+str(row['id'])+' [label="'+str(row['name'])+'", style=filled, color=lightgray];')
        file1.write('\n')

# Write all the edges
for index, row in df.iterrows():
    if (row['parent']>0) and (row['depth']<max_depth):
        try:
            file1.write('   '+str(row['parent'])+' -> '+str(row['id'])+' [label=" ", color='+edge_colors[row['depth']-1]+', arrowhead=normal];')
            file1.write('\n')
        except:
            file1.write('   '+str(row['parent'])+' -> '+str(row['id'])+' [label=" ", color=darkgray, arrowhead=normal];')

file1.write('}')
file1.close()
