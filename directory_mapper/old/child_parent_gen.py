import os, folderstats, pydot
import networkx as nx
import matplotlib.pyplot as plt
from networkx.drawing.nx_pydot import graphviz_layout
import pandas as pd

# Write the main path
cwd = os.getcwd()
#myPath = r'C:\Users\schluchter\Desktop\mesh_file_batch_processing'
myPath = r'C:\Users\schluchter\Desktop\CViL'
#myPath = r'G:\My Drive\Reference'
#myPath = r'Z:\projects\Andrew\3D_modelling'
#myPath = r'/home/zero/Desktop/sync_ceo/Reference'
max_depth = 10


data = list()
for root in os.walk(myPath):
    data.append(root)
    #print(root)

dirs=list(map(list, zip(*data)))[0]
subdirs=list(map(list, zip(*data)))[1]
listLength=len(dirs)
#print(listLength)

df = pd.DataFrame(data, columns=['dirs', 'subdirs', 'files'])
parents = [0] * listLength

for i in range(listLength):
    print("Entry i: "+str(i))
    try:
        subdirs[i][0]
        for sd in subdirs[i]:
            fullPath=str(dirs[i])+os.sep+str(sd)
            print("Child node path: ")
            print(fullPath)
            print("Child index/node: ")
            childIndex = dirs.index(fullPath)
            print(childIndex)
            print("Updating parents:")
            parents[childIndex]=i
            print(parents[childIndex])
        print('\n')
    except:
        #pass
        print("no subdirs found")

print("parents: ")
print(parents)
print("length of parents: ")
print(len(parents))

df['parent'] = parents
df.to_csv(cwd+os.sep+'fast_testing_output.csv')
