import os, folderstats, graphviz, pydot
import networkx as nx
import matplotlib.pyplot as plt
from networkx.drawing.nx_pydot import graphviz_layout

# Note that "folderstats" has been imported via pip
# Note that "graphviz" has ALSO been installed via: brew install graphviz

# Write the main path
cwd = os.getcwd()
#myPath = r'C:\Users\schluchter\Desktop\mesh_file_batch_processing'
myPath = r'C:\Users\schluchter\Desktop\CViL'
#myPath = r'G:\My Drive\Reference'

# Get folder stats into a dataframe
df = folderstats.folderstats(myPath, hash_name='md5', ignore_hidden=True)
#x = os.stat(r'C:\Users\schluchter\Desktop\CViL')

# Sort the index
df_sorted = df.sort_values(by='id')
df.to_csv(cwd+os.sep+'output.csv')

G = nx.Graph()
for i, row in df_sorted.iterrows():
    if row.parent:
        G.add_edge(row.id, row.parent)




file1 = open("test_output.txt","a")
file1.write("Nodes: ")
file1.write('\n\n')
for i in G.nodes():
    #print(i)
    file1.write(str(i))
    file1.write('\n')
file1.write("Edges: ")
file1.write('\n\n')
for i in G.edges():
    #print(i)
    file1.write(str(i))
    file1.write('\n')
file1.close()





# Print some additional information
print(nx.info(G))

'''
# Visualize that shiz: what's the most common filetype?
with plt.style.context('ggplot'):
    df['extension'].value_counts().plot(
        kind='bar', color='C1', title='Extension Distribution by Count');
'''


'''
# What about if they're grouped by size?
with plt.style.context('ggplot'):
    # Group by extension and sum all sizes for each extension 
    extension_sizes = df.groupby('extension')['size'].sum()
    # Sort elements by size
    extension_sizes = extension_sizes.sort_values(ascending=False)
    
    extension_sizes.plot(
        kind='bar', color='C1', title='Extension Distribution by Size')
'''


'''
# Make a tree map of it:
import squarify

# Group by extension and sum all sizes for each extension
extension_sizes = df.groupby('extension')['size'].sum()
# Sort elements by size
extension_sizes = extension_sizes.sort_values(ascending=False)

#squarify.plot(sizes=extension_counts.values, label=extension_counts.index.values)
squarify.plot(sizes=extension_sizes.values, label=extension_sizes.index.values)
plt.title('Extension Treemap by Size')
plt.axis('off');
'''


'''
# Now let's visualize the largest folders
with plt.style.context('ggplot'):
    # Filter the data set to only folders
    df_folders = df[df['folder']]
    # Set the name to be the index (so we can use it as a label later)
    df_folders.set_index('name', inplace=True)
    # Sort the folders by size
    df_folders = df_folders.sort_values(by='size', ascending=False)
    
    # Show the size of the largest 50 folders as a bar plot
    #df_sizes['size'][:50].plot(kind='bar', color='C0', title='Folder Sizes');
    df_folders['size'][:50].plot(kind='bar', color='C0', title='Folder Sizes');
'''


'''
# Create hierarchical map
pos_dot = graphviz_layout(G, prog='dot')
#pos_dot = graphviz_layout(G, prog='dot.exe')

fig = plt.figure(figsize=(16, 8))
nodes = nx.draw_networkx_nodes(G, pos_dot, node_size=2, node_color='C0')
edges = nx.draw_networkx_edges(G, pos_dot, edge_color='C0', width=0.5)
plt.axis('off');
'''


# Create radial map
pos_twopi = graphviz_layout(G, prog='twopi', root=1)
fig = plt.figure(figsize=(14, 14))
nx.draw_networkx_nodes(G, pos_twopi, node_size=2, node_color='C0')
nx.draw_networkx_nodes(G, pos_twopi, nodelist=[1], node_color='red', node_size=100, alpha=0.8)
nx.draw_networkx_edges(G, pos_twopi, edge_color='C0', width=0.5)

plt.axis('off')
plt.axis('equal');

# Not showing a plot? Try this:
import pylab
pylab.show()

# Can also travel to Desktop in CMD, then enter the following:
# folderstats CViL -c md5 -i -o stats.csv
