% TVL1denoise demo
%
% Manolis Lourakis 2016

%im=imread('tavr30_TF1_slice130.png');
%im = rgb2gray(im);
im=imread('impulse noise.png');
im = rgb2gray(im);
imshow(im);

outim=TVL1denoise(im, 1.0, 1000);
figure; imshow(outim, []);