% OUTLINE:
% Pull in original file
% Set up folder system to name incoming files sequentially
% Do the TVR thang
% Within the regularization loop, output the file to images
% Use the images to build a video

% NOTE: the TV regularization section has been transferred from external
% code; for more info, see:
%   A. Mordvintsev: ROF and TV-L1 denoising with Primal-Dual algorithm,
%   http://znah.net/rof-and-tv-l1-denoising-with-primal-dual-algorithm.html
% and
%   Chambolle et al. An introduction to Total Variation for Image Analysis, 2009. <hal-00437581>
%   https://hal.archives-ouvertes.fr/hal-00437581/document

% Start fresh
clear all;
close all;

% Provide the input file name here:
inputFile = 'impulse noise.png';
% Provide the framerate of the output video here (in fps):
framerate = 10;

% set up folder to collect the image set
workingDir = pwd;
mkdir(workingDir,'images_for_video')

% pull in original file
im=imread(inputFile);
% ensure that it's grayscale [may need to omit/alter for certain images]
im = rgb2gray(im);
% show the starting image [optional; currently deactivated]
%figure; imshow(im);

xx = 0;
% =============================
% optional: include 10 frames of the original image to better see progess
for xx = 1:10
% output the image file
filename = [sprintf('%03d',xx) '.jpg'];
fullname = fullfile(workingDir,'images_for_video',filename);
% make a copy of the current image to be outputted
iterIm = im;
imwrite(iterIm,fullname);  % Write out to a JPEG file (img1.jpg, img2.jpg, etc.)
end
xx = 10;
% =============================



% =================================================================
% BEGIN TV REGULARIZATION PROCEDURE
% =================================================================

% Set algorithm parameters
% regularization parameter
lambda = 1;
% number of iterations
niter = 100;
L2=8.0;
tau=0.02;
sigma=1.0/(L2*tau);
theta=1.0;
lt=lambda*tau;

% Establish image parameters
[height, width]=size(im); 
unew=zeros(height, width);
p=zeros(height, width, 2);
d=zeros(height, width);
ux=zeros(height, width);
uy=zeros(height, width);

mx=max(im(:));
if(mx>1.0)
nim=double(im)/double(mx); % normalize
else
nim=double(im); % leave intact
end
u=nim;

%[p(:, :, 1), p(:, :, 2)]=imgradientxy(u, 'IntermediateDifference');
  p(:, :, 1)=u(:, [2:width, width]) - u;
  p(:, :, 2)=u([2:height, height], :) - u;
  
for k=1:niter
% projection
% compute gradient in ux, uy
%[ux, uy]=imgradientxy(u, 'IntermediateDifference');
ux=u(:, [2:width, width]) - u;
uy=u([2:height, height], :) - u;
p=p + sigma*cat(3, ux, uy);
% project
normep=max(1, sqrt(p(:, :, 1).^2 + p(:, :, 2).^2)); 
p(:, :, 1)=p(:, :, 1)./normep;
p(:, :, 2)=p(:, :, 2)./normep;

% shrinkage
% compute divergence in div
div=[p([1:height-1], :, 2); zeros(1, width)] - [zeros(1, width); p([1:height-1], :, 2)];
div=[p(:, [1:width-1], 1)  zeros(height, 1)] - [zeros(height, 1)  p(:, [1:width-1], 1)] + div;

% TV-L1 model
v=u + tau*div;
unew=(v-lt).*(v-nim>lt) + (v+lt).*(v-nim<-lt) + nim.*(abs(v-nim)<=lt);

%if(v-nim>lt); unew=v-lt; elseif(v-nim<-lt) unew=v+lt; else unew=nim; end

% extragradient step
u=unew + theta*(unew-u);

% output the image file
filename = [sprintf('%03d',k+xx) '.jpg'];
fullname = fullfile(workingDir,'images_for_video',filename);
% make a copy of the current image to be outputted
iterIm = u;
imwrite(iterIm,fullname);  % Write out to a JPEG file (img1.jpg, img2.jpg, etc.)

end
% =================================================================
% END TV REGULARIZATION PROCEDURE
% =================================================================

% show the final image [optional; currently deactivated]
%figure; imshow(u, []);

% Get all the images you just made
imageNames = dir(fullfile(workingDir,'images_for_video','*.jpg'));
imageNames = {imageNames.name}';

% Make an empty video file
outputVideo = VideoWriter(fullfile(workingDir,'output_video.avi'));
outputVideo.FrameRate = framerate;
open(outputVideo);

% Build the video
for ii = (1:length(imageNames))
   img = imread(fullfile(workingDir,'images_for_video',imageNames{ii}));
   writeVideo(outputVideo,img);
end

% Close it out
close(outputVideo);



