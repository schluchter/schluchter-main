%{
% EXAMPLE:
y = [52.7 57.5 45.9 44.5 53.0 57.0 45.9 44.0]';
g1 = [1 2 1 2 1 2 1 2]; 
g2 = {'hi';'hi';'lo';'lo';'hi';'hi';'lo';'lo'}; 
g3 = {'may';'may';'may';'may';'june';'june';'june';'june'};

p = anovan(y,{g1,g2,g3})
%}
clear all;
close all;

% import data
dataIn = csvread('C:\Users\schluchter\Desktop\software and programming\schluchter-repo.git\MATLAB_scripts\3wayANOVA\columnated ICCs.csv', 1);

% convert data
measurement = dataIn(:,1);
patient = dataIn(:,2);
frame = dataIn(:,3);
observer = dataIn(:,4);
trial = dataIn(:,5);

% Calculate and output to terminal
%p = anovan(measurement,{patient,frame,observer,trial})
p = anovan(measurement,{observer,trial})
%p = anovan(measurement,{patient,frame})