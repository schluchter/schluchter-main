%{
%EXAMPLE
y = [52.7 57.5 45.9 44.5 53.0 57.0 45.9 44.0]';
g1 = [1 2 1 2 1 2 1 2]; 
g2 = {'hi';'hi';'lo';'lo';'hi';'hi';'lo';'lo'}; 
g3 = {'may';'may';'may';'may';'june';'june';'june';'june'};
p = anovan(y,{g1,g2,g3})
%}
clear all;
close all;

% import data
%dataIn = csvread('C:\Users\schluchter\Desktop\software and programming\schluchter-repo.git\MATLAB_scripts\3wayANOVA\columnated ICCs.csv', 1);
dataIn = csvread('C:\Users\schluchter\Desktop\software and programming\schluchter-repo.git\MATLAB_scripts\3wayANOVA\ICCs_for_python.csv', 1);

% convert data
o1t1 = dataIn(:,1);
o1t2 = dataIn(:,2);
o1t3 = dataIn(:,3);
o2t1 = dataIn(:,4);
o2t2 = dataIn(:,5);
o2t3 = dataIn(:,6);
o3t1 = dataIn(:,7);
o3t2 = dataIn(:,8);
o3t3 = dataIn(:,9);

%measurement = dataIn(:,1);
%patient = dataIn(:,2);
%frame = dataIn(:,3);
%observer = dataIn(:,4);
%trial = dataIn(:,5);

% Calculate and output to terminal
p = anovan(o1t1,{o1t2,o1t3,o2t1})
%p = anovan(measurement,{patient,frame,observer,trial})