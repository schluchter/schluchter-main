%STLin ='MESH_FILE_FULL_PATH.stl'
STLin ='Z:\projects\Andrew\LAA_Research\Watchman_Best_Fit_Planner_subproject\skeletonization_files\test_skel_files\';
STLin1 = strcat(STLin,'LAA_test_mesh.stl');
STLin2 = strcat(STLin,'LAA_test_mesh_v1_half_skel.stl');
STLin3 = strcat(STLin,'LAA_test_mesh_half_skel_combined.stl');

% Get the max x, y, and z dimensions of the STLin1
[coordVERTICES] = READ_stl(STLin1);
maxX = max(max(coordVERTICES(:,1,:)));
minX = min(min(coordVERTICES(:,1,:)));
maxY = max(max(coordVERTICES(:,2,:)));
minY = min(min(coordVERTICES(:,2,:)));
maxZ = max(max(coordVERTICES(:,3,:)));
minZ = min(min(coordVERTICES(:,3,:)));
% Correct ranges for stl to volume conversion
preRangeX = (maxX - minX);
laa_rangeX = ceil(preRangeX)*2;
preRangeY = (maxY - minY);
laa_rangeY = ceil(preRangeY)*2;
preRangeZ = (maxZ - minZ);
laa_rangeZ = ceil(preRangeZ)*2;

% Get the max x, y, and z dimensions of the STLin2
[coordVERTICES] = READ_stl(STLin2);
maxX = max(max(coordVERTICES(:,1,:)));
minX = min(min(coordVERTICES(:,1,:)));
maxY = max(max(coordVERTICES(:,2,:)));
minY = min(min(coordVERTICES(:,2,:)));
maxZ = max(max(coordVERTICES(:,3,:)));
minZ = min(min(coordVERTICES(:,3,:)));
% Correct ranges for stl to volume conversion
preRangeX = (maxX - minX);
hs_rangeX = ceil(preRangeX)*2;
preRangeY = (maxY - minY);
hs_rangeY = ceil(preRangeY)*2;
preRangeZ = (maxZ - minZ);
hs_rangeZ = ceil(preRangeZ)*2;

% Get the max x, y, and z dimensions of the STLin3
[coordVERTICES] = READ_stl(STLin3);
maxX = max(max(coordVERTICES(:,1,:)));
minX = min(min(coordVERTICES(:,1,:)));
maxY = max(max(coordVERTICES(:,2,:)));
minY = min(min(coordVERTICES(:,2,:)));
maxZ = max(max(coordVERTICES(:,3,:)));
minZ = min(min(coordVERTICES(:,3,:)));
% Correct ranges for stl to volume conversion
preRangeX = (maxX - minX);
max_rangeX = ceil(preRangeX)*2;
preRangeY = (maxY - minY);
max_rangeY = ceil(preRangeY)*2;
preRangeZ = (maxZ - minZ);
max_rangeZ = ceil(preRangeZ)*2;

% Voxelize them
[LAAinput,gridCOx,gridCOy,gridCOz] = VOXELISE(laa_rangeX,laa_rangeY,laa_rangeZ,STLin1);
[half_skel_LAA,gridCOx,gridCOy,gridCOz] = VOXELISE(hs_rangeX,hs_rangeY,hs_rangeZ,STLin2);
%[maxLAA,gridCOx,gridCOy,gridCOz] = VOXELISE(max_rangeX,max_rangeY,max_rangeZ,STLin3);

% Orient LAA volume into proper sized 3D matrix
% Make empty 3D matrix of max size, with padding
LAA = zeros(max_rangeX+2,max_rangeY+2,max_rangeZ+2);
% Translate in new matrix, and
% Pad the skeleton voxel volume by 2 and translate each dimension by 1
for iterX = 1:laa_rangeX
    for iterY = 1:laa_rangeY
        for iterZ = 1:laa_rangeZ
            LAA((iterX+1)+.5*(max_rangeX-laa_rangeX),(iterY+1)+.5*(max_rangeY-laa_rangeY),(iterZ+1)+.5*(max_rangeZ-laa_rangeZ)) = LAAinput(iterX,iterY,iterZ);
        end
    end
end

% Skeletonize the half_skel volume
skeleton = Skeleton3D(half_skel_LAA);

% Remove all branches by transforming to node network with conditions, and
% then transforming back
[A,node,link] = Skel2Graph3D(skeleton, 10);
w=size(skeleton,1);
l=size(skeleton,2);
h=size(skeleton,3);
skeleton = Graph2Skel3D(node,link,w,l,h);

% Orient skeleton volume into proper sized 3D matrix
% Make empty 3D matrix of max size, with padding
skel = zeros(max_rangeX+2,max_rangeY+2,max_rangeZ+2);
% Translate in new matrix, and
% Pad the skeleton voxel volume by 2 and translate each dimension by 1
for iterX = 1:hs_rangeX
    for iterY = 1:hs_rangeY
        for iterZ = 1:hs_rangeZ
            skel((iterX+1)+.5*(max_rangeX-hs_rangeX),(iterY+1)+.5*(max_rangeY-hs_rangeY),(iterZ+1)+.5*(max_rangeZ-hs_rangeZ)) = skeleton(iterX,iterY,iterZ);
        end
    end
end

%volumeViewer(skel);

%{
% Save the STL's
LAAOutPath ='Z:\projects\Andrew\LAA_Research\Watchman_Best_Fit_Planner_subproject\skeletonization_files\test_skel_files\LAA_test_mesh_output.stl';
skelOutPath ='Z:\projects\Andrew\LAA_Research\Watchman_Best_Fit_Planner_subproject\skeletonization_files\test_skel_files\LAA_test_mesh_skeleton_output.stl';
LAAOut = isosurface(logical(LAA));
skelOut = isosurface(logical(skel));
stlwrite(LAAOutPath,LAAOut);
stlwrite(skelOutPath,skelOut);
%}

% Highlight the skeleton voxels
for iterX = 1:max_rangeX
    for iterY = 1:max_rangeY
        for iterZ = 1:max_rangeZ
            if  skel(iterX,iterY,iterZ) == 1%skel(iterX,iterY,iterZ) == 1 && LAA(iterX,iterY,iterZ) == 1
                LAA(iterX,iterY,iterZ) = 2;
            end
        end
    end
end

LAA_with_skel = LAA;
%save('LAA_with_skel');

% Load the volume into the app
volumeViewer(LAA_with_skel);
