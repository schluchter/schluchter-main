clear all;
close all;

% load example binary skeleton image
load skel

% INPUT GOES HERE
%{
STLin ='Z:\projects\Andrew\LAA_Research\Watchman_Best_Fit_Planner\skeletonization_files\';
STLin= strcat(STLin,'Anon_264803_TF08_edited_for_skeleton_pulled_extra_long_and_pointed_tunnels_removed.stl')
[coordVERTICES] = READ_stl(STLin);
maxX = max(max(coordVERTICES(:,1,:)));
minX = min(min(coordVERTICES(:,1,:)));
maxY = max(max(coordVERTICES(:,2,:)));
minY = min(min(coordVERTICES(:,2,:)));
maxZ = max(max(coordVERTICES(:,3,:)));
minZ = min(min(coordVERTICES(:,3,:)));
preRangeX = (maxX - minX);
rangeX = ceil(preRangeX)*2;
preRangeY = (maxY - minY);
rangeY = ceil(preRangeY)*2;
preRangeZ = (maxZ - minZ);
rangeZ = ceil(preRangeZ)*2;
[gridOUTPUT,gridCOx,gridCOy,gridCOz] = VOXELISE(rangeX,rangeY,rangeZ,STLin);
skel = Skeleton3D(gridOUTPUT);
%}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%skelOutPath ='Z:\projects\Andrew\LAA_Research\Watchman_Best_Fit_Planner\skeletonization_files\test_skeleton_output.stl';
%skelOut = isosurface(skel);
%stlwrite(skelOutPath,skelOut);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

w = size(skel,1);
l = size(skel,2);
h = size(skel,3);

% initial step: condense, convert to voxels and back, detect cells
[~,node,link] = Skel2Graph3D(skel,0);

% total length of network
wl = sum(cellfun('length',{node.links}));

skel2 = Graph2Skel3D(node,link,w,l,h);
[~,node2,link2] = Skel2Graph3D(skel2,0);

% calculate new total length of network
wl_new = sum(cellfun('length',{node2.links}));

% iterate the same steps until network length changed by less than 0.5%
while(wl_new~=wl)

    wl = wl_new;   
    
     skel2 = Graph2Skel3D(node2,link2,w,l,h);
     [A2,node2,link2] = Skel2Graph3D(skel2,0);

     wl_new = sum(cellfun('length',{node2.links}));

end;

%{
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
skelOutPath ='Z:\projects\Andrew\LAA_Research\Watchman_Best_Fit_Planner\skeletonization_files\test_skeleton_output.stl';
skelOut = isosurface(skel2);
stlwrite(skelOutPath,skelOut);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%}

% display result
figure();
hold on;
for i=1:length(node2)
    x1 = node2(i).comx;
    y1 = node2(i).comy;
    z1 = node2(i).comz;
    
    if(node2(i).ep==1)
        ncol = 'c';
    else
        ncol = 'y';
    end;
    
    for j=1:length(node2(i).links)    % draw all connections of each node
        if(node2(link2(node2(i).links(j)).n2).ep==1)
            col='k'; % branches are blue
        else
            col='k'; % links are red
        end;
        if(node2(link2(i).n1).ep==1)
            col='k';
        end;

        
        % draw edges as lines using voxel positions
        for k=1:length(link2(node2(i).links(j)).point)-1            
            [x3,y3,z3]=ind2sub([w,l,h],link2(node2(i).links(j)).point(k));
            [x2,y2,z2]=ind2sub([w,l,h],link2(node2(i).links(j)).point(k+1));
            line([y3 y2],[x3 x2],[z3 z2],'Color',col,'LineWidth',2);
        end;
    end;
    
    % draw all nodes as yellow circles
    plot3(y1,x1,z1,'o','Markersize',9,...
        'MarkerFaceColor',ncol,...
        'Color','k');
end;
axis image;axis off;
set(gcf,'Color','white');
drawnow;
view(-17,46);

