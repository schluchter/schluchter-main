% Script to feed in a (monochrome) dicom file and output a quantification of noise
% unique functions used: nqKern.m

addpath('noise_quantifier'); % location of functions and images

% test nqKern script to ensure in proper path
[ave,stdev] = nqKern(values)




% 1. Take in image and convert to matrix


% 2. Build kernel engine(s)
% Kernel function: 
% a. obtain local gradient (4-neighbor? 8-neighbor?)
% b. For kernel sizes larger than a single pixel, average out the pixel
%   values to form the X-neighbor shape as needed
% c. 


% 3. Run kernel over image matrix. Output values per pixel to another matrix
% (normalize, convert to image and output/save), and add the values to a sum
% variable. The variable =? the "sum noise level" of the image.