% function to take voxelized object in raw (sparse) matrix and fill in
% tunnels: in xy, yz, and zx planes

%function outMat = simpleFill(inMat)

% DEBUG:
% Load in the STL and convert to voxelized logical matrix
%------------------------------------------------------------------------

%STLin ='MESH_FILE_FULL_PATH.stl'
STLin ='Z:\projects\Andrew\LAA_Research\Watchman_Best_Fit_Planner_subproject\skeletonization_files\test_skel_files\';
STLin1 = strcat(STLin,'toroid_tester_mesh.stl');

% Get the max x, y, and z dimensions of the STLin1
[coordVERTICES] = READ_stl(STLin1);
laa_maxX = ceil(max(max(coordVERTICES(:,1,:))));
laa_minX = ceil(min(min(coordVERTICES(:,1,:))));
laa_maxY = ceil(max(max(coordVERTICES(:,2,:))));
laa_minY = ceil(min(min(coordVERTICES(:,2,:))));
laa_maxZ = ceil(max(max(coordVERTICES(:,3,:))));
laa_minZ = ceil(min(min(coordVERTICES(:,3,:))));

% Correct ranges for stl to volume conversion
preRangeX = (laa_maxX - laa_minX);
laa_rangeX = ceil(preRangeX);
preRangeY = (laa_maxY - laa_minY);
laa_rangeY = ceil(preRangeY);
preRangeZ = (laa_maxZ - laa_minZ);
laa_rangeZ = ceil(preRangeZ);

% Voxelize it
[LAAinput,gridCOx,gridCOy,gridCOz] = VOXELISE(laa_rangeX,laa_rangeY,laa_rangeZ,STLin1);
inMat = LAAinput;


% Prepare the function
%-------------------------------------------------------------------------

% make sure the loaded voxel matrix is logical (modify later if needed)
inMat = logical(inMat);

% get max dimensions of x, y, z
[maxX, maxY, maxZ] = size(inMat);

% make an empty 3d matrix box
LAA = zeros(maxX,maxY,maxZ);

% make a flag for knowing when crossing a 1d tunnel
tunnelFlag = false;
tunnelNum = 0;

% Fill in the tunnels
%-------------------------------------------------------------------------

% fill in xy planes

%-------------------------------------------------------------------------
%-------------------------------------------------------------------------
% CURRENT WORK IN PROGRESS ZONE

for iii = 1:maxZ
    for ii = 1:maxY
        for i = 1:maxX
            if inMat(i,ii,iii) == 1 && tunnelFlag == true
                % fill in the tunnel since flag was raised
                LAA(i-tunnelNum:i,ii,iii) = 1;
            elseif inMat(i,ii,iii) == 1
                LAA(i,ii,iii) = 1;
            else
                tunnelFlag = true;
                tunnelNum = tunnelNum + 1;   
            end
        end
        tunnelFlag = false;
        tunnelNum = 0;
    end
    tunnelFlag = false;
    tunnelNum = 0;
end
tunnelFlag = false;
tunnelNum = 0;
%-------------------------------------------------------------------------
%-------------------------------------------------------------------------

%WORK IN PROGRESS
% fill in yz planes
for i = 1:maxX
    for iii = 1:maxZ
        for ii = 1:maxY
            % do the thing
        end
    end
end

%WORK IN PROGRESS
% fill in zx planes
for ii = 1:maxY
    for i = 1:maxX
        for iii = 1:maxZ
            % do the thing
        end
    end
end


% Make another voxelized matrix, but bounded on eash side by 1 for proper
% STL output
%-------------------------------------------------------------------------

% make an empty 3d matrix box
meshOutLAA = zeros(maxX+2,maxY+2,maxZ+2);

% buffer the voxel matrix by 1 on each side before saving it as STL
for iii = 1:maxZ
    for ii = 1:maxY
        for i = 1:maxX
            if LAA(i,ii,iii) == 1
                meshOutLAA(i+1,ii+1,iii+1) = 1;
            end
        end
    end
end
%make it logical
meshOutLAA = logical(meshOutLAA);
% save the STL's
LAAOutPath ='Z:\projects\Andrew\LAA_Research\Watchman_Best_Fit_Planner_subproject\skeletonization_files\test_skel_files\toroid_tester_mesh_filled.stl';
LAAOut = isosurface(meshOutLAA);
stlwrite(LAAOutPath,LAAOut);

% end the function
%---------------------------------------------------------------------

%changing name to simplify testing and debugging
outMat = LAA;
    
%end