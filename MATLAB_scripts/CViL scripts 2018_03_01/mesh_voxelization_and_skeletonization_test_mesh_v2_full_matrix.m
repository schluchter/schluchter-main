%STLin ='MESH_FILE_FULL_PATH.stl'
STLin ='Z:\projects\Andrew\LAA_Research\Watchman_Best_Fit_Planner_subproject\skeletonization_files\test_skel_files\';
STLin1 = strcat(STLin,'LAA_test_mesh.stl');
STLin2 = strcat(STLin,'LAA_test_mesh_v1_half_skel.stl');
STLin3 = strcat(STLin,'LAA_test_mesh_half_skel_combined.stl');

% Get the max x, y, and z dimensions of the STLin1
[coordVERTICES] = READ_stl(STLin1);
laa_maxX = ceil(max(max(coordVERTICES(:,1,:))));
laa_minX = ceil(min(min(coordVERTICES(:,1,:))));
laa_maxY = ceil(max(max(coordVERTICES(:,2,:))));
laa_minY = ceil(min(min(coordVERTICES(:,2,:))));
laa_maxZ = ceil(max(max(coordVERTICES(:,3,:))));
laa_minZ = ceil(min(min(coordVERTICES(:,3,:))));
% Correct ranges for stl to volume conversion
preRangeX = (laa_maxX - laa_minX);
laa_rangeX = ceil(preRangeX);
preRangeY = (laa_maxY - laa_minY);
laa_rangeY = ceil(preRangeY);
preRangeZ = (laa_maxZ - laa_minZ);
laa_rangeZ = ceil(preRangeZ);

% Get the max x, y, and z dimensions of the STLin2
[coordVERTICES] = READ_stl(STLin2);
hs_maxX = ceil(max(max(coordVERTICES(:,1,:))));
hs_minX = ceil(min(min(coordVERTICES(:,1,:))));
hs_maxY = ceil(max(max(coordVERTICES(:,2,:))));
hs_minY = ceil(min(min(coordVERTICES(:,2,:))));
hs_maxZ = ceil(max(max(coordVERTICES(:,3,:))));
hs_minZ = ceil(min(min(coordVERTICES(:,3,:))));
% Correct ranges for stl to volume conversion
preRangeX = (hs_maxX - hs_minX);
hs_rangeX = ceil(preRangeX);
preRangeY = (hs_maxY - hs_minY);
hs_rangeY = ceil(preRangeY);
preRangeZ = (hs_maxZ - hs_minZ);
hs_rangeZ = ceil(preRangeZ);

% Get the max x, y, and z dimensions of the STLin3
[coordVERTICES] = READ_stl(STLin3);
combo_maxX = ceil(max(max(coordVERTICES(:,1,:))));
combo_minX = ceil(min(min(coordVERTICES(:,1,:))));
combo_maxY = ceil(max(max(coordVERTICES(:,2,:))));
combo_minY = ceil(min(min(coordVERTICES(:,2,:))));
combo_maxZ = ceil(max(max(coordVERTICES(:,3,:))));
combo_minZ = ceil(min(min(coordVERTICES(:,3,:))));
% Correct ranges for stl to volume conversion
preRangeX = (combo_maxX - combo_minX);
combo_rangeX = ceil(preRangeX);
preRangeY = (combo_maxY - combo_minY);
combo_rangeY = ceil(preRangeY);
preRangeZ = (combo_maxZ - combo_minZ);
combo_rangeZ = ceil(preRangeZ);

% Voxelize them
[LAAinput,gridCOx,gridCOy,gridCOz] = VOXELISE(laa_rangeX,laa_rangeY,laa_rangeZ,STLin1);
[half_skel_LAA,gridCOx,gridCOy,gridCOz] = VOXELISE(hs_rangeX,hs_rangeY,hs_rangeZ,STLin2);
%[maxLAA,gridCOx,gridCOy,gridCOz] = VOXELISE(max_rangeX,max_rangeY,max_rangeZ,STLin3);



%%%
%%%

% function to fill in tunnels to LAAinput goes here

%%%
%%%




% Orient LAA volume into proper sized 3D matrix
% Make empty 3D matrix of max size, with padding
LAA = zeros(combo_rangeX+2,combo_rangeY+2,combo_rangeZ+2);
% Translate in new matrix, and
% Pad the skeleton voxel volume by 2 and translate each dimension by 1
for iterX = 1:laa_rangeX
    for iterY = 1:laa_rangeY
        for iterZ = 1:laa_rangeZ
            LAA((iterX+1)+(laa_minX-combo_minX),(iterY+1)+(laa_minY-combo_minY),(iterZ+1)+(laa_minZ-combo_minZ)) = LAAinput(iterX,iterY,iterZ);
        end
    end
end

% Skeletonize the half_skel volume
skeleton = Skeleton3D(half_skel_LAA);

% Remove all branches by transforming to node network with conditions, and
% then transforming back
[A,node,link] = Skel2Graph3D(skeleton, 10);
w=size(skeleton,1);
l=size(skeleton,2);
h=size(skeleton,3);
skeleton = Graph2Skel3D(node,link,w,l,h);

% Orient skeleton volume into proper sized 3D matrix
% Make empty 3D matrix of max size, with padding
skel = zeros(combo_rangeX+2,combo_rangeY+2,combo_rangeZ+2);
% Translate in new matrix, and
% Pad the skeleton voxel volume by 2 and translate each dimension by 1
for iterX = 1:hs_rangeX
    for iterY = 1:hs_rangeY
        for iterZ = 1:hs_rangeZ
            skel((iterX+1)+(hs_minX-combo_minX),(iterY+1)+(hs_minY-combo_minY),(iterZ+1)+(hs_minZ-combo_minZ)) = skeleton(iterX,iterY,iterZ);
        end
    end
end

%volumeViewer(skel);


% Save the STL's
LAAOutPath ='Z:\projects\Andrew\LAA_Research\Watchman_Best_Fit_Planner_subproject\skeletonization_files\test_skel_files\LAA_test_mesh_output.stl';
skelOutPath ='Z:\projects\Andrew\LAA_Research\Watchman_Best_Fit_Planner_subproject\skeletonization_files\test_skel_files\LAA_test_mesh_skeleton_output.stl';
LAAOut = isosurface(logical(LAA));
skelOut = isosurface(logical(skel));
stlwrite(LAAOutPath,LAAOut);
stlwrite(skelOutPath,skelOut);


% Highlight the skeleton voxels
for iterX = 1:combo_rangeX
    for iterY = 1:combo_rangeY
        for iterZ = 1:combo_rangeZ
            if  skel(iterX,iterY,iterZ) == 1%skel(iterX,iterY,iterZ) == 1 && LAA(iterX,iterY,iterZ) == 1
                LAA(iterX,iterY,iterZ) = 2;
            end
        end
    end
end

LAA_with_skel = uint8(LAA);
save('LAA_with_skel');

% Load the volume into the app
%volumeViewer(LAA_with_skel);