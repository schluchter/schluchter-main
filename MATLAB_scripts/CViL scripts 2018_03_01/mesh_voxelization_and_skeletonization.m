%STLin ='MESH_FILE_FULL_PATH.stl'
STLin ='Z:\projects\Andrew\LAA_Research\Watchman_Best_Fit_Planner_subproject\skeletonization_files\';
%STLin= strcat(STLin,'Anon_264803_TF08_edited_for_skeleton_orifice_pulled_and_pointed_tunnels_removed.stl')
STLin = strcat(STLin,'watchman_fit_test_mesh_coned.stl');

% Get the max x, y, and z dimensions of the watchman file
[coordVERTICES] = READ_stl(STLin);
maxX = max(max(coordVERTICES(:,1,:)));
minX = min(min(coordVERTICES(:,1,:)));
maxY = max(max(coordVERTICES(:,2,:)));
minY = min(min(coordVERTICES(:,2,:)));
maxZ = max(max(coordVERTICES(:,3,:)));
minZ = min(min(coordVERTICES(:,3,:)));

preRangeX = (maxX - minX);
rangeX = ceil(preRangeX)*2;
preRangeY = (maxY - minY);
rangeY = ceil(preRangeY)*2;
preRangeZ = (maxZ - minZ);
rangeZ = ceil(preRangeZ)*2;

%doubling to increase resolution
%{
preRangeX = 2*(maxX - minX);
rangeX = ceil(preRangeX)*2;
preRangeY = 2*(maxY - minY);
rangeY = ceil(preRangeY)*2;
preRangeZ = 2*(maxZ - minZ);
rangeZ = ceil(preRangeZ)*2;
%}

% Voxelize it
%[gridOUTPUT,gridCOx,gridCOy,gridCOz] = VOXELISE(rangeX,rangeY,rangeZ,STLin);
[gridOUTPUT,gridCOx,gridCOy,gridCOz] = VOXELISE(rangeX+1,rangeY+1,rangeZ+1,STLin);

% pad the voxel volume by 2 and translate each dimension by 1
LAA = zeros(rangeX+2,rangeY+2,rangeZ+2);
for iterX = 1:rangeX
    for iterY = 1:rangeY
        for iterZ = 1:rangeZ
            %try
            LAA(iterX+1,iterY+1,iterZ+1) = gridOUTPUT(iterX,iterY,iterZ);
            %catch
            %    disp('caught');
            %end
        end
    end
end

% Normalize the ranges to the new volume
rangeX = rangeX+2;
rangeY = rangeY+2;
rangeZ = rangeZ+2;


% skeletonize that mofo
%LAA = gridOUTPUT;
skel = Skeleton3D(LAA);
skeleton_test = Skeleton3D(LAA);
save('skeleton_test');

%skel = Skeleton3D(gridOUTPUT);
%save('skel')
%testSkelGraph = Skel2Graph3D(skel, 100)
%[A,node,link] = Skel2Graph3D(skel, 100);
[A,node,link] = Skel2Graph3D(skel, 1);
%[A,node,link] = Skel2Graph3D(skel, 1);

%{
for x = 1:69
    fn_structdisp(node(x))
end

for y = 1:33
    fn_structdisp(link(y))
end
%}


w = size(skel,1);
l = size(skel,2);
h = size(skel,3);
%skel = logical(skel);
skel = Graph2Skel3D(node,link,w,l,h);
save('skel')


% DEBUG:
% convert back and output them: compare to original inputs and/or eachother
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
LAAOutPath ='Z:\projects\Andrew\LAA_Research\Watchman_Best_Fit_Planner_subproject\skeletonization_files\watchman_fit_test_mesh_coned_output.stl';
skelOutPath ='Z:\projects\Andrew\LAA_Research\Watchman_Best_Fit_Planner_subproject\skeletonization_files\watchman_fit_test_mesh_coned_thresh_1_skeleton_output.stl';
LAAOut = isosurface(logical(LAA));
skelOut = isosurface(logical(skel));
%STLout ='Z:\projects\Andrew\LAA_Research\Watchman_Best_Fit_Planner\watchman_files\test_output.stl';
stlwrite(LAAOutPath,LAAOut);
stlwrite(skelOutPath,skelOut);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Save the Watchman labelled volumes to file
% NOTE: you actually have to say the NAME of the variable
%save(preFileName, 'LAA');

% highlight the skeleton voxels
for iterX = 1:rangeX
    for iterY = 1:rangeY
        for iterZ = 1:rangeZ
            if  skel(iterX,iterY,iterZ) == 1 && LAA(iterX,iterY,iterZ) == 1
                LAA(iterX,iterY,iterZ) = 2;
            end
        end
    end
end

% Load the volume into the app
%volumeViewer(LAA);