% pull a 20x20 pixel chunk out of the listed files

addpath('noise_quantifier'); % location of functions and images

% all images, convert immediately to matrix to allow concatenation
image1 = rgb2gray(imread('lenna_base.png'));
image2 = rgb2gray(imread('Lenna_noise_value_10.png'));
image3 = rgb2gray(imread('Lenna_noise_value_50.png'));
image4 = rgb2gray(imread('Lenna_noise_value_99.png'));
image5 = rgb2gray(imread('Lenna_noise_value_200.png'));

% Must use cat(3... <variables>); doing it c++ style concatenates them
% along a pre-existing dimension, making 1 long image
images = cat(3, image1, image2, image3, image4, image5);

% build the output array
%THIS PART IS TEMPORARY: will have to change if adding more or less images
outImages = cat(3, zeros(20), zeros(20), zeros(20), zeros(20), zeros(20));
%for num = 1:min(size(images))
%    outImages(:,:,num) = zeros(20);
%end

% set counter: extract once for every image
counterM = 1;

% for every image
for curIm = 1:min(size(images))
    % convert to grayscale proper, then get the max and min values for later
    % for whatever reason, mat2gray needs max/min to be doubles
    
    %iMat = rgb2gray(image1);
    iMat = images(:,:,curIm);
    amin = double(min(min(iMat)));
    amax = double(max(max(iMat)));

    output = zeros(20);
    for counterY = 1:20
        for counterX = 1:20
            %
            output(counterX, counterY) = iMat(counterX, counterY);
        end
    end

    % convert and write to output, being sure to normalize to the original max
    % and min values of the image
    imageOut = mat2gray(output, [amin amax]);
    outImages(:,:,counterM) = imageOut;
    counterM = counterM +1;
end

% THIS PART IS ALSO TEMPORARY: CHANGE IF CHANGING INPUT IMAGES
imwrite(outImages(:,:,1), 'noise_quantifier/output_base.png');
imwrite(outImages(:,:,2), 'noise_quantifier/output_20.png');
imwrite(outImages(:,:,3), 'noise_quantifier/output_50.png');
imwrite(outImages(:,:,4), 'noise_quantifier/output_99.png');
imwrite(outImages(:,:,5), 'noise_quantifier/output_200.png');

figure
subplot(3,2,1)
imshow(outImages(:,:,1));
subplot(3,2,2)
imshow(outImages(:,:,2));
subplot(3,2,3)
imshow(outImages(:,:,3));
subplot(3,2,4)
imshow(outImages(:,:,4));
subplot(3,2,5)
imshow(outImages(:,:,5));
