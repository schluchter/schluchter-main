%STLin ='MESH_FILE_FULL_PATH.stl'
w21 ='Z:\projects\Andrew\LAA_Research\Watchman_Best_Fit_Planner_subproject\watchman_files\21mm_processed_Watchman_mesh_aligned_centered.stl';
w24 ='Z:\projects\Andrew\LAA_Research\Watchman_Best_Fit_Planner_subproject\watchman_files\24mm_processed_Watchman_mesh_aligned_centered.stl';
w27 ='Z:\projects\Andrew\LAA_Research\Watchman_Best_Fit_Planner_subproject\watchman_files\27mm_processed_Watchman_mesh_aligned_centered.stl';
w30 ='Z:\projects\Andrew\LAA_Research\Watchman_Best_Fit_Planner_subproject\watchman_files\30mm_processed_Watchman_mesh_aligned_centered.stl';
w33 ='Z:\projects\Andrew\LAA_Research\Watchman_Best_Fit_Planner_subproject\watchman_files\33mm_processed_Watchman_mesh_aligned_centered.stl';
%STLin ='Z:\projects\Andrew\LAA_Research\Watchman_Best_Fit_Planner\watchman_files\input.stl';
watchmanList = [w21;w24;w27;w30;w33];
listSize = size(watchmanList(:,1));
listLen = listSize(1,1);

% for every watchman mesh file:
for iterW = 1:listLen
    
    STLin = watchmanList(iterW,:);
    % Get the max x, y, and z dimensions of the watchman file
    [coordVERTICES] = READ_stl(STLin);
    maxX = ceil(max(max(coordVERTICES(:,1,:))));
    minX = ceil(min(min(coordVERTICES(:,1,:))));
    maxY = ceil(max(max(coordVERTICES(:,2,:))));
    minY = ceil(min(min(coordVERTICES(:,2,:))));
    maxZ = ceil(max(max(coordVERTICES(:,3,:))));
    minZ = ceil(min(min(coordVERTICES(:,3,:))));
    preRangeX = (maxX - minX);
    rangeX = ceil(preRangeX);
    preRangeY = (maxY - minY);
    rangeY = ceil(preRangeY);
    preRangeZ = (maxZ - minZ);
    rangeZ = ceil(preRangeZ);

    % Voxelize it
    %[gridOUTPUT,gridCOx,gridCOy,gridCOz] = VOXELISE(100,100,100,STLin);
    [gridOUTPUT,gridCOx,gridCOy,gridCOz] = VOXELISE(rangeX,rangeY,rangeZ,STLin);
    %[gridOUTPUT,gridCOx,gridCOy,gridCOz] = VOXELISE(rangeX+1,rangeY+1,rangeZ+1,STLin);

    % convert from logical to simple numeric to allow marking of front and back
    preVol = uint8(gridOUTPUT);

    % pad the voxel volume by 2 and translate each dimension by 1
    wm_vol = zeros(rangeX+2,rangeY+2,rangeZ+2);
    for iterX = 1:rangeX
        for iterY = 1:rangeY
            for iterZ = 1:rangeZ
                %try
                wm_vol(iterX+1,iterY+1,iterZ+1) = preVol(iterX,iterY,iterZ);
                %catch
                %    disp('caught');
                %end
            end
        end
    end

    % Normalize the ranges to the new volume
    rangeX = rangeX+2;
    rangeY = rangeY+2;
    rangeZ = rangeZ+2;

    % DEBUG:
    % convert it back and output it: compare to original input
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    test_output = isosurface(logical(wm_vol));
    wSize = num2str(iterW);
    % yes, I actually do need the following in multiple pieces, so I can
    % call them separately later on. I know what I'm doing >:P
    preFileName = strcat('w',wSize);
    fileName = strcat(preFileName,'.stl');
    STLout = fullfile('Z:\projects\Andrew\LAA_Research\Watchman_Best_Fit_Planner_subproject\watchman_files\',fileName);
    %STLout ='Z:\projects\Andrew\LAA_Research\Watchman_Best_Fit_Planner\watchman_files\test_output.stl';
    stlwrite(STLout,test_output);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % iterate through the different voxels and double the intensity of the 
    % "back" center voxel, and triple the intensity of the "front" center voxel
    for iterX = 1:rangeX
        for iterY = 1:rangeY
            for iterZ = 1:rangeZ
                if  iterX < (0.3*rangeX) && wm_vol(iterX,iterY,iterZ) == 1
                    wm_vol(iterX,iterY,iterZ) = 2;
                end
                if iterY == ceil(rangeY/2)
                    if iterZ == ceil(rangeZ/2)
                        if iterX == 2
                            wm_vol(iterX,iterY,iterZ) = 4;
                        elseif iterW == 5 && iterX == rangeX-2
                                wm_vol(iterX,iterY,iterZ) = 3;
                        elseif iterW == 4 && iterX == rangeX-2
                                wm_vol(iterX,iterY,iterZ) = 3;
                        elseif iterW ~= 4&& iterW ~= 5 && iterX == rangeX-1
                                wm_vol(iterX,iterY,iterZ) = 3;
                        end
                    end
                end
            end
        end
    end
    % Save the Watchman labelled volumes to file
    % NOTE: you actually have to say the NAME of the MATLAB variable
    save(preFileName, 'wm_vol');
    
    % Load the volume into the app
    %volumeViewer(wm_vol);
end

%volumeViewer(wm_vol);