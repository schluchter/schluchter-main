% Clear figures, etc.
clear all; close all; clc;

% Add full repo path
% NOTE: genpath is to make it recursive
%addpath(genpath('C:\Users\schluchter\Desktop\software and programming\ucsd_cvi_repo\squeez\tools\cpd2'))
addpath(genpath('C:\Users\woodford\ucsd_cvi_repo'));

% Add all object input paths and names (in order of vertex count)
atrial_kick ='C:\Users\woodford\Desktop\4 phase LV meshes\atrial_kick_smooth.obj';
systole1 ='C:\Users\woodford\Desktop\4 phase LV meshes\systole1_smooth.obj';
diastasis ='C:\Users\woodford\Desktop\4 phase LV meshes\diastasis_smooth.obj';
systole2 ='C:\Users\woodford\Desktop\4 phase LV meshes\systole2_smooth.obj';

% Add all object output paths and names (in order of vertex count)
% NOT NEEDED: atrial_assimilated_kick_path ='C:\Users\woodford\Desktop\4 phase LV meshes\atrial_kick_smooth.stl';
%systole1_assimilated ='C:\Users\woodford\Desktop\4 phase LV meshes\systole1_smooth_assimilated.obj';
%diastasis_assimilated ='C:\Users\woodford\Desktop\4 phase LV meshes\diastasis_smooth_assimilated.obj';
%systole2_assimilated ='C:\Users\woodford\Desktop\4 phase LV meshes\systole2_smooth_assimilated.obj';
systole1_assimilated ='systole1_smooth_assimilated.obj';
diastasis_assimilated ='diastasis_smooth_assimilated.obj';
systole2_assimilated ='systole2_smooth_assimilated.obj';

%{
% Paths to fixed and moving examples.
f_fn = 'C:\Users\woodford\ucsd_cvi_repo\tools\wobj\examples-dv\suzanne-fixed.obj';
m_fn = 'C:\Users\woodford\ucsd_cvi_repo\tools\wobj\examples-dv\suzanne-moving.obj';
o_fn = 'registered.obj';
%}

% Define the CPD options.
opt.method='nonrigid_lowrank'; % Use the lowrank matrix approximation.
opt.numeig=30;                 % leave only 30 larges (out of 8171) eigenvectors/values to approximate G
opt.eigfgt=1;                  % use FGT to find the largest eigenvectore/values
opt.beta=1;                    % the width of Gaussian kernel (smoothness)
opt.lambda=10;                  % regularization weight
opt.viz=1;                     % show every iteration
opt.outliers=0.1;              % use 0.7 noise weight
% May wish to change value below
opt.fgt=2;                     % use FGT to compute matrix-vector products (2 means to switch to truncated version at the end, see cpd_register)
% May wish to change value below
opt.normalize=1;               % normalize to unit variance and zero mean before registering (default)
opt.corresp=0;                 % compute correspondence vector at the end of registration (not being estimated by default)
opt.max_it=100;                % max number of iterations
opt.tol=1e-3;                  % tolerance


% For each mesh (not mesh0): make new mesh "meshN" mapped from mesh0
%cpd_register_obj(f_fn, m_fn, o_fn, opt)
% Map atrial_kick vertices onto systole1
cpd_register_obj(atrial_kick, systole1, systole1_assimilated, opt);
% Map atrial_kick vertices onto diastasis
% Map atrial_kick vertices onto systole2