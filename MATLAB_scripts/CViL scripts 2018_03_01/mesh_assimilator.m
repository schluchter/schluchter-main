% Full repo path
%addpath('C:\Users\schluchter\Desktop\software and programming\ucsd_cvi_repo\')
% cpd2 path, genpath is to make it recursive
%addpath(genpath('C:\Users\schluchter\Desktop\software and programming\ucsd_cvi_repo\squeez\tools\cpd2'))
addpath(genpath('C:\Users\woodford\ucsd_cvi_repo'))


% bring in all meshes
%STLin ='MESH_FILE_FULL_PATH.stl'
w21 ='C:\Users\schluchter\Desktop\00.stl';
w24 ='C:\Users\schluchter\Desktop\00.stl';
w27 ='C:\Users\schluchter\Desktop\00.stl';
w30 ='C:\Users\schluchter\Desktop\00.stl';
w33 ='C:\Users\schluchter\Desktop\00.stl';
%STLin ='Z:\projects\Andrew\LAA_Research\Watchman_Best_Fit_Planner\watchman_files\input.stl';
watchmanList = [w21;w24;w27;w30;w33];
listSize = size(watchmanList(:,1));
listLen = listSize(1,1);

% for every watchman mesh file:
for iterW = 1:listLen
    
    STLin = watchmanList(iterW,:);
    % Get the max x, y, and z dimensions of the watchman file
    [coordVERTICES] = READ_stl(STLin);
    maxX = ceil(max(max(coordVERTICES(:,1,:))));
    minX = ceil(min(min(coordVERTICES(:,1,:))));
    maxY = ceil(max(max(coordVERTICES(:,2,:))));
    minY = ceil(min(min(coordVERTICES(:,2,:))));
    maxZ = ceil(max(max(coordVERTICES(:,3,:))));
    minZ = ceil(min(min(coordVERTICES(:,3,:))));
    preRangeX = (maxX - minX);
    rangeX = ceil(preRangeX);
    preRangeY = (maxY - minY);
    rangeY = ceil(preRangeY);
    preRangeZ = (maxZ - minZ);
    rangeZ = ceil(preRangeZ);

    % Voxelize it
    %[gridOUTPUT,gridCOx,gridCOy,gridCOz] = VOXELISE(100,100,100,STLin);
    [gridOUTPUT,gridCOx,gridCOy,gridCOz] = VOXELISE(rangeX,rangeY,rangeZ,STLin);
    %[gridOUTPUT,gridCOx,gridCOy,gridCOz] = VOXELISE(rangeX+1,rangeY+1,rangeZ+1,STLin);

    % convert from logical to simple numeric to allow marking of front and back
    preVol = uint8(gridOUTPUT);

    % pad the voxel volume by 2 and translate each dimension by 1
    wm_vol = zeros(rangeX+2,rangeY+2,rangeZ+2);
    for iterX = 1:rangeX
        for iterY = 1:rangeY
            for iterZ = 1:rangeZ
                %try
                wm_vol(iterX+1,iterY+1,iterZ+1) = preVol(iterX,iterY,iterZ);
                %catch
                %    disp('caught');
                %end
            end
        end
    end

    % Normalize the ranges to the new volume
    rangeX = rangeX+2;
    rangeY = rangeY+2;
    rangeZ = rangeZ+2;

    % DEBUG:
    % convert it back and output it: compare to original input
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    test_output = isosurface(logical(wm_vol));
    wSize = num2str(iterW);



% count vertices of all meshes
% find mesh with highest count - "mesh0"

% for each mesh (not mesh0): make new mesh "meshN" mapped from mesh0
cpd_register
% pad the voxel volume by 2 and translate each dimension by 1
%    wm_vol = zeros(rangeX+2,rangeY+2,rangeZ+2);
% NOTE: you actually have to say the NAME of the MATLAB variable below
%save(preFileName, 'wm_vol');
%test_output = isosurface(logical(wm_vol));


% output all meshes (mesh0 not needed :P)
systole1_assimilated_path ='C:\Users\woodford\Desktop\4 phase LV meshes\diastasis_assimilated.stl';
stlwrite(systole1_assimilated_path,systole1_assimilated);
diastasis_assimilated_path ='C:\Users\woodford\Desktop\4 phase LV meshes\diastasis_assimilated.stl';
stlwrite(diastasis_assimilated_path,diastasis_assimilated);
systole2_assimilated_path ='C:\Users\woodford\Desktop\4 phase LV meshes\diastasis_assimilated.stl';
stlwrite(systole2_assimilated_path,systole2_assimilated);