load('skeleton_test');

%testSkelGraph = Skel2Graph3D(skel, 100)
[A,node,link] = Skel2Graph3D(skeleton_test, 1);

%network = struct(linkNum,0,nodeA,0,nodeB,0,length,0,terminal,0)
link_network = struct('linkNum',{},'nodeA',{},'nodeB',{},'length',{},'terminal',{});

structLen = length(link);
%disp(structLen)

%[dump ALen] = size(A);
%disp(ALen)

lengthMat = full(A);

for counter = 1:structLen
    % add fields to node_network
    nodeA = link(counter).n1;
    nodeB = link(counter).n2;
    %disp("A entry: ")
    %disp(A(nodeA,nodeB))
    link_network(counter) = struct('linkNum',counter,'nodeA',nodeA,'nodeB',nodeB,'length',lengthMat(nodeA,nodeB),'terminal',node(counter).ep);
end

% Making a new "version" of the 'node' struct: including node # this time
node_network = node;
% add nodeNum field to node_network
[node_network(:).nodeNum] = deal(0);
for counter = 1:length(node)
    node_network(counter).nodeNum = counter;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DEBUG ONLY: display full node_network structure
%for x = 1:structLen
%    fn_structdisp(node_network(x))
%end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Prune network to remove terminal nodes
% create a new struct for the non-terminal nodes
%disp(length(node_network))
newNodeNetwork = struct();
[newNodeNetwork(:).idx] = deal(0);
[newNodeNetwork(:).links] = deal(0);
[newNodeNetwork(:).conn] = deal(0);
[newNodeNetwork(:).comx] = deal(0);
[newNodeNetwork(:).comy] = deal(0);
[newNodeNetwork(:).comz] = deal(0);
[newNodeNetwork(:).ep] = deal(0);
%{
% THIS VERSION FOR ADDING nodeNum: NOT USED FOR USE IN ACTUAL FUNCTION!
[newNodeNetwork(:).nodeNum] = deal(0);
%disp(newNodeNetwork)
% remove the terminal nodes
for i = 1:length(node_network)
    %remove entire node entry if a node is terminal
    if node_network(i).ep == 0
        newNodeNetwork(i) = struct('idx',node_network(i).idx,'links',node_network(i).links,'conn',node_network(i).conn,'comx',node_network(i).comx,'comy',node_network(i).comy,'comz',node_network(i).comz,'ep',node_network(i).ep,'nodeNum',node_network(i).nodeNum);
        %disp(node_network(i))
        %disp(newNodeNetwork(i))
    end
end
%}
% USE THIS VERSION FOR ACTUAL FUNCTIONS
%disp(newNodeNetwork)
% remove the terminal nodes
for i = 1:length(node)
    %remove entire node entry if a node is terminal
    if node(i).ep == 0
        newNodeNetwork(i) = struct('idx',node(i).idx,'links',node(i).links,'conn',node(i).conn,'comx',node(i).comx,'comy',node(i).comy,'comz',node(i).comz,'ep',node(i).ep);
    end
end
% now do the same for the "link" struct:
% create a new struct for the non-terminal links
newLinkNetwork = struct();
[newLinkNetwork(:).n1] = deal(0);
[newLinkNetwork(:).n2] = deal(0);
[newLinkNetwork(:).point] = deal(0);
%disp(length(newNodeNetwork))
% copy over all non-terminal links to newLinkNetwork
nodeRange = length(newNodeNetwork);
ii = 1; % for use in loop below
for i = 1:length(link)    
    % remove entire link entry if either n1 or n2 is > nodeRange
    if link(i).n1 <= nodeRange && link(i).n2 <= nodeRange
        newLinkNetwork(ii) = struct('n1',link(i).n1,'n2',link(i).n2,'point',link(i).point);
        % using ii, above, to properly index the newLinkNetwork fields
        ii = ii + 1;
    end
end
% now clean up all references to terminal nodes and links left in newNodeNetwork:
%for all newNodeNetwork(i):
    %for all entries in newNodeNetwork(i).links:
        %if entry > nodeRange:
            %delete that entry
    %for all entries in newNodeNetwork(i).conn:
        %if entry > nodeRange:
            %delete that entry


%{
% put it all together and into a skeleton
w = size(skeleton_test,1);
l = size(skeleton_test,2);
h = size(skeleton_test,3);
%skel_level2 = Graph2Skel3D(newNodeNetwork,link,w,l,h);
skel_level2 = Graph2Skel3D(newNodeNetwork,newLinkNetwork,w,l,h);
%volumeViewer(test_this)
[AL2,nodeL2,linkL2] = Skel2Graph3D(skel_level2, 1);
%}

%{
% DEBUG:
% convert back and output: compare to original input
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
skelOutPath ='Z:\projects\Andrew\LAA_Research\Watchman_Best_Fit_Planner\skeletonization_files\LAA_skeleton_output_level2.stl';
skelOut = isosurface(logical(skel_level2));
disp("stuff")
stlwrite(skelOutPath,skelOut);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%}


% NEXT STEPS

% re-analyze network to get new set of terminal nodes
%


% REPEAT until only 2 terminal nodes remain
% Prune X number of nodes as "padding" (make 'X' a variable for the user!)
% Start "growing" the network back out from the ends to find longest path:
    % Get the 2 terminal nodes; save them and the immediately connected 
    % nodes "interior" to the backbone. For each of the 2:
    % LOOP:
    % List nodes connected to the 2 terminal nodes (that aren't interior)
    % If a branch occurs, convert the scalar value of length to an array
    % List the associated nodes in an array similarly
    % Once a terminal node is reached in any branch, 'close off' that
    % branch to further "growing"
    % Make an array of all possible lengths from each of the original 2
    % terminal nodes
    % Search for the longest length sum, and return the corresponding nodes
    % in that path
    % Add those nodes to the original network to form the full, "true"
    % centerline path

% use "ind2sub" function to convert indices to x,y,z coordinates
% convert the node network back into a skeleton
% new skeleton: map "into" LAA volume, and convert it to STL