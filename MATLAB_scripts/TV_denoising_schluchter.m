
% set paths

addpath('C:\Users\schluchter\Desktop\')
addpath('C:\Users\schluchter\Desktop\software and programming\schluchter-repo.git\MATLAB_testing_grounds')
workingDir = 'C:\Users\schluchter\Desktop\software and programming\schluchter-repo.git\MATLAB_testing_grounds';

% output filename:
outfile = 'C:\Users\schluchter\Desktop\MATLAB_testing\output.jpg';

% load the image:
img = imread('captured_coronary_xray.bmp');
