clear all;
close all;

% Get paths and load data
badNiiPath = 'C:\Users\schluchter\Desktop\software and programming\schluchter-repo.git\MATLAB_scripts\nii scripts\bad.nii.gz';
goodNiiPath = 'C:\Users\schluchter\Desktop\software and programming\schluchter-repo.git\MATLAB_scripts\nii scripts\good.nii.gz';
dataOutPath = 'C:\Users\schluchter\Desktop\software and programming\schluchter-repo.git\MATLAB_scripts\nii scripts\bad to good_output.nii.gz';
badNii = load_nii(badNiiPath);
goodNii = load_nii(goodNiiPath);
%view_nii(dataIn)

% Make backup!
badNiibackup = badNii;

affine = [goodNii.hdr.hist.srow_x; goodNii.hdr.hist.srow_y; goodNii.hdr.hist.srow_z]


% Transfer over the header data
%badNii.hdr.dime.datatype = goodNii.hdr.dime.datatype;
%badNii.hdr.dime.bitpix = goodNii.hdr.dime.bitpix;
%badNii.hdr.hist.quatern_c = goodNii.hdr.hist.quatern_c;
%badNii.hdr.hist.quatern_d = goodNii.hdr.hist.quatern_d;
%badNii.hdr.hist.qoffset_y = goodNii.hdr.hist.qoffset_y;


%badNii.hdr.hist.srow_x = goodNii.hdr.hist.srow_x;
%badNii.hdr.hist.srow_x(1) = 0.5;
%badNii.hdr.hist.srow_y = goodNii.hdr.hist.srow_y;
%badNii.hdr.hist.srow_y(1) = 0
%badNii.hdr.hist.srow_z = goodNii.hdr.hist.srow_z;

% ATTEMPTING TO TRANSLATE
%badNii.original.hdr.hist.sform_code = 1;  % = goodNii.original.hdr.hist.sform_code
%badNii.hdr.hist.srow_x(4) = badNii.hdr.hist.srow_x(4) + 300;
%badNii.hdr.hist.srow_y(4) = badNii.hdr.hist.srow_y(4) + 300;
%badNii.hdr.hist.srow_z(4) = badNii.hdr.hist.srow_z(4) + 300;
%badNii.original.hdr.hist.srow_x(4) = goodNii.original.hdr.hist.srow_x(4) + 300;
%badNii.original.hdr.hist.srow_y(4) = goodNii.original.hdr.hist.srow_y(4) + 300;
%badNii.original.hdr.hist.srow_z(4) = goodNii.original.hdr.hist.srow_z(4) + 300;
affine = [badNii.hdr.hist.srow_x; badNii.hdr.hist.srow_y; badNii.hdr.hist.srow_z]

%badNii.hdr.hist.originator = goodNii.hdr.hist.originator;
%badNii.hdr.hist.flip_orient = goodNii.hdr.hist.flip_orient;

% And the same for the section called 'original'
%{
badNii.original.hdr.dime.datatype = goodNii.original.hdr.dime.datatype;
badNii.original.hdr.dime.bitpix = goodNii.original.hdr.dime.bitpix;
badNii.original.hdr.dime.pixdim = goodNii.original.hdr.dime.pixdim;
badNii.original.hdr.hist.qform_code = goodNii.original.hdr.hist.qform_code;
badNii.original.hdr.hist.sform_code = goodNii.original.hdr.hist.sform_code;
badNii.original.hdr.hist.quatern_c = goodNii.original.hdr.hist.quatern_c;
badNii.original.hdr.hist.quatern_d = goodNii.original.hdr.hist.quatern_d;
badNii.original.hdr.hist.qoffset_y = goodNii.original.hdr.hist.qoffset_y;
badNii.original.hdr.hist.srow_x = goodNii.original.hdr.hist.srow_x;
badNii.original.hdr.hist.srow_y = goodNii.original.hdr.hist.srow_y;
badNii.original.hdr.hist.srow_z = goodNii.original.hdr.hist.srow_z;
badNii.original.hdr.hist.originator = goodNii.original.hdr.hist.originator;
%}

% Moving the LAA
%{
shifter = 30;
badNii.hdr.hist.originator(1) = badNii.original.hdr.hist.originator(1) + shifter;
badNii.original.hdr.hist.originator(1) = badNii.original.hdr.hist.originator(1) + shifter;
%}

% NOTHING :[
%{
badNii.hdr.hist.qoffset_x = badNii.hdr.hist.qoffset_x + shifter;
badNii.original.hdr.hist.qoffset_x = badNii.original.hdr.hist.qoffset_x + shifter;
badNii.hdr.hist.qoffset_y = badNii.hdr.hist.qoffset_y + shifter;
badNii.original.hdr.hist.qoffset_y = badNii.original.hdr.hist.qoffset_y + shifter;
badNii.hdr.hist.qoffset_z = badNii.hdr.hist.qoffset_z + shifter;
badNii.original.hdr.hist.qoffset_z = badNii.original.hdr.hist.qoffset_z + shifter;
%}

% Save it:
save_nii(badNii, dataOutPath); 