clear all;
close all;

% Get paths and load data
dataInPath = 'C:\Users\schluchter\Desktop\software and programming\schluchter-repo.git\MATLAB_scripts\nii scripts\test.nii.gz';
dataOutPath = 'C:\Users\schluchter\Desktop\software and programming\schluchter-repo.git\MATLAB_scripts\nii scripts\test_output.nii.gz';
dataIn = load_nii(dataInPath);
%view_nii(dataIn)

% Set rotation angle (theta must be in radians!)
theta = pi;

% Transformation matrix:
Tz = [cos(theta) -sin(theta) 0; sin(theta) cos(theta) 0; 0 0 1];

% Take in current affine matrix
old_origin =            dataIn.hdr.hist.originator(1:3);
%new_origin =            newDataIn.hdr.hist.originator(1:3);





% THIS SECTION IS FOR ROTATION OF DATA DIRECTLY
%{ 
% Get old coordinates/matrix from the file
rl = load_untouch_nii(dataInPath); 
old_xyz = [rl.hdr.hist.srow_x(1:3);rl.hdr.hist.srow_y(1:3);rl.hdr.hist.srow_z(1:3)];

% Apply transformation
new_xyz = Tz * old_xyz; 
rl.hdr.hist.srow_x(1:3) = new_xyz(1,:);
rl.hdr.hist.srow_y(1:3) = new_xyz(2,:);
rl.hdr.hist.srow_z(1:3) = new_xyz(3,:); 

% Save the changes
save_untouch_nii(rl, dataInPath); 

% Reslice it (this saves it automatically)
reslice_nii(dataInPath, dataOutPath); 
%}

% WIP: THIS SECTION IS FOR PADDING
%{
% Load the full nii
newDataIn = load_nii(dataOutPath);
% Apply padding
%pad_nii
old_origin =            dataIn.hdr.hist.originator(1:3);
new_origin =            newDataIn.hdr.hist.originator(1:3);
origin_diffs =          new_origin - old_origin;
opt.pad_from_L =        origin_diffs(1)
%opt.pad_from_P =        origin_diffs(2)
opt.pad_from_I =        origin_diffs(3)
padded =                pad_nii(newDataIn, opt);
%}

% Reslice it (this saves it automatically)
%reslice_nii(dataInPath, dataOutPath); 

% And last of all, read it back
%rotatedData = load_nii(dataOutPath); 
%view_nii(rotatedData); 