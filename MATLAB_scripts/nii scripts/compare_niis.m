clear all;
close all;

% Get paths and load data
dataInPath1 = 'C:\Users\schluchter\Desktop\software and programming\schluchter-repo.git\MATLAB_scripts\nii scripts\good.nii.gz';
dataInPath2 = 'C:\Users\schluchter\Desktop\software and programming\schluchter-repo.git\MATLAB_scripts\nii scripts\bad.nii.gz';
goodNii = load_nii(dataInPath1);
badNii = load_nii(dataInPath2);
%view_nii(dataIn)
