% set image path
addpath('C:\Users\schluchter\Desktop\')
addpath('C:\Users\schluchter\Desktop\MATLAB_testing')
workingDir = 'C:\Users\schluchter\Desktop\MATLAB_testing';
% output filename:
outfile = 'C:\Users\schluchter\Desktop\MATLAB_testing\output.jpg';
% load the image:
img = imread('captured_coronary_xray.bmp');

% get the X and Y dimensions
[y_max, x_max, z_max] = size(img);

% make a blank image that's 5x by 9y
mult_img = zeros(y_max*9, x_max*5, z_max);

% get new max dimensions
[y_max_big, x_max_big, z_max_big] = size(mult_img);



%%%%%%%%%%%% SECTION IN PROGRESS %%%%%%%%%%%%

my_image = img(:,:,:);
% loop over all rows and columns to change pixel values
for ii=1:size(my_image,1)
    for jj=1:size(my_image,2)
        % get pixel value
        pixel=my_image(ii,jj);
          % check pixel value and assign new value
          if pixel<100
              new_pixel=0;
          elseif pixel>200
              new_pixel=255;
          else
              new_pixel = pixel;
          end
          % save new pixel value in thresholded image
          testimg(ii,jj,:)=new_pixel;
      end
  end
% Removes impossible values
%testimg(testimg<0) = 0;
imshow(testimg);

%%%%%%%%%%%% END - SECTION IN PROGRESS %%%%%%%%%%%%



% Duplicate original image into separate columns, then rows
for i = 1:y_max
    for ii = 1:x_max
        for n = 0:4
            for m = 0:8
                for iii = 1:z_max
                    mult_img(i+(y_max*m),ii+(x_max*n),iii)=img(i,ii,iii);
                end
            end
        end
    end
end
%{
% duplicate rows procedurally
for ii = 1:y_max
    for m = 1:8
        mult_img(ii+(y_max*m),:,:)=mult_img(ii,:,:);           
    end
end
%}
%{
% duplicate rows sequentially
for j = 1:x_max_big
    for jj = 1:y_max
        %for m = 1:8
            for jjj = 1:z_max_big
                mult_img(jj+(y_max),j,jjj) = mult_img(jj,j,jjj);
                mult_img(jj+(y_max*2),j,jjj) = mult_img(jj,j,jjj);
                mult_img(jj+(y_max*3),j,jjj) = mult_img(jj,j,jjj);
                mult_img(jj+(y_max*4),j,jjj) = mult_img(jj,j,jjj);
                mult_img(jj+(y_max*5),j,jjj) = mult_img(jj,j,jjj);
                mult_img(jj+(y_max*6),j,jjj) = mult_img(jj,j,jjj);
                mult_img(jj+(y_max*7),j,jjj) = mult_img(jj,j,jjj);
                mult_img(jj+(y_max*8),j,jjj) = mult_img(jj,j,jjj);
            end
        %end
    end
end
%}

% save the file
output = uint8(mult_img);
%output = mult_img;
imwrite(output, outfile);