% set paths

% === NOTE: I moved this whole folder recently; fix paths before using! ===
addpath('C:\Users\schluchter\Desktop\')
addpath('C:\Users\schluchter\Desktop\MATLAB_testing')
workingDir = 'C:\Users\schluchter\Desktop\MATLAB_testing';
% output filename:
outfile = 'C:\Users\schluchter\Desktop\output.jpg';
% load the image:
img = imread('captured_coronary_xray.bmp');
% =========================================================================


% Number of columns
N = 5;
% Number of rows
M = 5;

% TESTING ONLY
%N = 1; M = 2;

% Tile it (within which, is binning it)
out_image = bin_and_tile(img, N, M);

% Removes impossible (negative) values
%testimg(testimg<0) = 0;

% save the file
output = uint8(out_image);
imwrite(output, outfile);

% ============================= FUNCTIONS =============================

% Binning function (output is either 0 or 255)
% loop over all rows and columns to change pixel values
function outImg = binit2(inImg, minCn, maxCn)
    for ii=1:size(inImg,1)
        for jj=1:size(inImg,2)
            % get pixel value
            pixel=inImg(ii,jj);
            % check pixel value and assign new value
            if pixel<minCn
              new_pixel=0;
            elseif pixel>maxCn
              new_pixel=0;
            else
              new_pixel = 255;
            end
            % save new pixel value in thresholded image
            outImg(ii,jj,:)=new_pixel;
          end
    end
end

% Binning function
% loop over all rows and columns to change pixel values
function outImg = binit(inImg, minCn, maxCn)
    for ii=1:size(inImg,1)
        for jj=1:size(inImg,2)
            % get pixel value
            pixel=inImg(ii,jj);
              % check pixel value and assign new value
              if pixel<minCn
                  new_pixel=0;
              elseif pixel>maxCn
                  new_pixel=255;
              else
                  new_pixel = pixel;
              end
              % save new pixel value in thresholded image
              outImg(ii,jj,:)=new_pixel;
          end
    end
end


% Duplicate original image into separate columns, then rows
function bigImg = bin_and_tile(startImg, N, M)
    % get the X and Y dimensions
    [y_max, x_max, z_max] = size(startImg);
    % make a blank image that's 5x by 9y
    bigImg = zeros(y_max*M, x_max*N, z_max);
    % derive the increments of binning, and the levels to bin at (0-255)
    numBins = M*N;
    incVal = 255/numBins;
    inc = 1;
    % iterate that shizz
    for m = 1:M
        for n = 1:N
            minCn = (inc-1)*incVal;
            maxCn = inc*incVal;
            if inc == M*N
                maxCn = maxCn + 1;
            end
            smallImg = uint8(binit2(startImg, minCn, maxCn)); % note: loses the z values of original image (changing a line below)
            inc = inc + 1;
            %smallImg = startImg; % doesn't lose original image format
            for i = 1:y_max
                for ii = 1:x_max
                    %for iii = 
                    bigImg(i+(y_max*(m-1)),ii+(x_max*(n-1)),:)=smallImg(i,ii);
                end
            end
            %disp(['m = ',m,'n = ',n])
            disp('m = ,n = ')
            disp(m)
            disp(n)
        end
    end
end