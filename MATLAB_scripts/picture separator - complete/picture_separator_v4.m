% set image path
addpath('C:\Users\schluchter\Desktop\')
addpath('C:\Users\schluchter\Desktop\MATLAB_testing')
workingDir = 'C:\Users\schluchter\Desktop\MATLAB_testing';
% output filename:
outfile = 'C:\Users\schluchter\Desktop\MATLAB_testing\output.jpg';
% load the image:
img = imread('captured_coronary_xray.bmp');


% Number of columns
N = 5;
% Number of rows
M = 9;

% TESTING ONLY
N = 1; M = 2;


% TESTING ONLY
% Tile it (within which, is binning it)
out_image = bin_and_tile(img, N, M);

% TESTING ONLY
%bin it:
%out_image = binit(img);


% Removes impossible (negative) values
%testimg(testimg<0) = 0;

% save the file
output = uint8(out_image);
%output = rgb2gray(uint8(out_image));
imwrite(output, outfile);
%disp(output(1,1))

% ============================= FUNCTIONS =============================

% Binning function
% loop over all rows and columns to change pixel values
function outImg = binit(inImg)
    for ii=1:size(inImg,1)
        for jj=1:size(inImg,2)
            % get pixel value
            pixel=inImg(ii,jj);
              % check pixel value and assign new value
              if pixel<100
                  new_pixel=0;
              elseif pixel>200
                  new_pixel=255;
              else
                  new_pixel = pixel;
              end
              % save new pixel value in thresholded image
              outImg(ii,jj,:)=new_pixel;
          end
    end
end


% Duplicate original image into separate columns, then rows
function bigImg = bin_and_tile(startImg, N, M)
    % get the X and Y dimensions
    [y_max, x_max, z_max] = size(startImg);
    % make a blank image that's 5x by 9y
    bigImg = zeros(y_max*M, x_max*N, z_max);
    % derive the increments of binning, and the levels to bin at (0-255)
    numBins = M*N;
    
    
    % iterate that shizz
    for m = 1:M
        for n = 1:N
            smallImg = uint8(binit(startImg)); % note: loses the z values of original image (changing a line below)
            %smallImg = startImg; % doesn't lose original image format
            for i = 1:y_max
                for ii = 1:x_max
                    bigImg(i+(y_max*(m-1)),ii+(x_max*(n-1)))=smallImg(i,ii);
                    %{
                    % ONLY USE THIS LINE IF NOT LOSING THE Z VALUES OF IMG
                    for iii = 1:z_max
                        bigImg(i+(y_max*(m-1)),ii+(x_max*(n-1)),iii)=smallImg(i,ii,iii);
                    end
                    %}
                end
            end
            %disp(['m = ',m,'n = ',n])
            disp('m = ,n = ')
            disp(m)
            disp(n)
        end
    end
end


%{
% Duplicate original image into separate columns, then rows
function bigImg = bin_and_tile(startImg, N, M)
    % get the X and Y dimensions
    [y_max, x_max, z_max] = size(startImg);
    % make a blank image that's 5x by 9y
    bigImg = zeros(y_max*M, x_max*N, z_max);

    smallImg = startImg;

    % iterate that shizz
    for i = 1:y_max
        for ii = 1:x_max
            for n = 1:N
                for m = 1:M
                    for iii = 1:z_max
                        bigImg(i+(y_max*(m-1)),ii+(x_max*(n-1)),iii)=smallImg(i,ii,iii);
                    end
                end
            end
        end
    end
end
%}

% ======================== HOW TO USE FUNCTIONS ========================
%{
function [avg, med] = mystats(x)
n = length(x);
avg = mymean(x,n);
med = mymedian(x,n);
end

function a = mymean(v,n)
% MYMEAN Example of a local function.

a = sum(v)/n;
end
%}