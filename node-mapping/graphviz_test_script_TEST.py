# Create graph object
from graphviz import Digraph
# default format: .pdf
#dot = Digraph(comment='The Round Table')
dot = Digraph(comment='The Round Table', format='svg')


#dot  #doctest: +ELLIPSIS

# Add nodes and edges
dot.node('A', 'String 1')
dot.node('B', 'String 2')
dot.node('L', 'String 3')

dot.node('AA', 'Test PJ', shape='box')
dot.node('BA', 'Task 1')
dot.node('CA', 'Task 2')
dot.node('DA', 'Task 3')


dot.edge('A', 'B')
dot.edge('A', 'L')
dot.edge('B', 'L', constraint='false') # constraint puts this node on same vertical level as last node

dot.edge('AA', 'BA')
dot.edge('BA', 'CA')
dot.edge('BA', 'DA')
dot.edge('CA', 'DA')


# Echo the generated source code
print(dot.source)  # doctest: +NORMALIZE_WHITESPACE

# Render and view the result as a .gv and .pdf
dot.render('test-output/round-table.gv', view=True)  # doctest: +SKIP
#'test-output/round-table.gv.pdf'
