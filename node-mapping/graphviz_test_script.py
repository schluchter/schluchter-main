# Create graph object
from graphviz import Digraph
dot = Digraph(comment='The Round Table')
#dot  #doctest: +ELLIPSIS

# Add nodes and edges
dot.node('A', 'King Arthur')
dot.node('B', 'Sir Bedevere the Wise')
dot.node('L', 'Sir Lancelot the Brave')

dot.edges(['AB', 'AL'])
dot.edge('B', 'L', constraint='false')

# Echo the generated source code
print(dot.source)  # doctest: +NORMALIZE_WHITESPACE

# Render and view the result as a .pdf
dot.render('test-output/round-table.gv', view=True)  # doctest: +SKIP
'test-output/round-table.gv.pdf'
