import os, pandas, xlsxwriter, csv, svgwrite
#import re

cwd = os.getcwd()
input_file = "task_export.csv"
input_name = cwd+os.sep+input_file

data = pandas.read_csv(input_file, sep='`')
df = pandas.DataFrame(data)

# Drop unneeded columns
df.drop(columns=['Local Id', 'Google Id', 'Notes', 'Due Date','Compl. Date', 'Last Modified'], inplace=True)

# Make the node map
#dwg = svgwrite.Drawing('test.svg', profile='full') # 'full' means slower, but more features
dwg = svgwrite.Drawing('test.svg', profile='tiny')


dwg.add(dwg.text('Test', insert=(0, 12), fill='red'))
dwg.add(dwg.rect((10, 10), (300, 200), stroke=svgwrite.rgb(10, 10, 16, '%'), fill='red'))
'''
dwg.add(dwg.line((0, 0), (10, 0), stroke=svgwrite.rgb(10, 10, 16, '%')))
dwg.add(dwg.text('Test', insert=(0, 0.2), fill='red'))
'''


dwg.save()


print(df)
writer = pandas.ExcelWriter('output.xlsx')
df.to_excel(writer,'Sheet1')
writer.save()
