import os

# RENAME THE STL FILES
# get the current working dir
mainDir = os.getcwd()

for aDir in os.scandir(mainDir):
    # NOTE: "not entry.name.startswith('.')" is for UNIX-based systems (linux only?)
    # that also stops .DS_store files! Ha HA!
    if not aDir.name.startswith('.') and aDir.name != "new_directory" and aDir.is_dir():
        #print(aDir.name)

        for subdir in os.scandir(aDir):
            if subdir.is_dir():
                #print(subdir.name)

                for entry in os.scandir(subdir):
                    if not entry.name.startswith('.') and entry.name.endswith('.stl'):
                        print(entry.name)
                        # RENAME the STL to 4th&5th chars of 'subdir' (.stl)
                        fullPathOld = subdir.path + os.sep + entry.name
                        print(fullPathOld)
                        newName = subdir.name[3:5] + '.stl'
                        print(newName)
                        fullPathNew = subdir.path + os.sep + newName
                        os.rename(fullPathOld, fullPathNew)


# RELOCATE THE STL FILES
for root, dirs, files in os.walk(mainDir):
    for name in files:
        #print(name)
        if name.endswith((".stl", ".obj")):
            new_name = mainDir+os.sep+name
            #print(name)
            os.rename(root + os.sep + name, new_name)


# DELETE THE REMAINING FOLDERS
for aDir in os.scandir(mainDir):
    if aDir.is_dir():
        #print(aDir.name)
        os.remove(aDir)
