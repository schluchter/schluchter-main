#!/usr/bin/python3

import os

cwd = os.getcwd()

folder_paths = []
folder_names = []
#files = []

# FULL FILE SYSTEM TREE
#for entry in os.scandir('/'):
for entry in os.scandir(cwd):
    if entry.is_dir():
        folder_paths.append(entry.path)
        folder_names.append(entry.name)
#    elif entry.is_file():
#        files.append(entry.path)

if not os.path.exists(cwd+os.sep+"directories"):
    os.makedirs(cwd+os.sep+"directories")
for folder in folder_names:
    os.makedirs(cwd+os.sep+"directories"+os.sep+folder)

# DEBUGGING:
"""
print('Names:')
# Print in one big chunk
#print(folder_names)
# Print sequentially, seperated by carriage return
print(*folder_names, sep='\n')
print("")

print('Paths:')
# Print in one big chunk
#print(folder_paths)
# Print sequentially, seperated by carriage return
print(*folder_paths, sep='\n')

test = input()
"""
