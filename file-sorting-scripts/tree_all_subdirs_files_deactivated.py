import os

#PARAMETERS
#====================================
# Print files as well?
print_files = False
# Determines how many levels to go down
max_level = 2
# Write to file instead of terminal?
write_file = True

def list_files(startpath):
    if  write_file == True:
        fileout = open('directory.txt','w')
        for root, dirs, files in os.walk(startpath):
            level = root.replace(startpath, '').count(os.sep)
            indent = ' ' * 4 * (level)
            if level <= max_level:
                fileout.write(indent+os.path.basename(root)+'\n')
                subindent = ' ' * 4 * (level + 1)
                # Uncomment below to include files in the tree:
                if print_files:
                    for f in files:
                        fileout.write(subindent+f+'\n')
        fileout.close()
    else:
        for root, dirs, files in os.walk(startpath):
            level = root.replace(startpath, '').count(os.sep)
            indent = ' ' * 4 * (level)
            if level <= max_level:
                print(indent+os.path.basename(root)+'\n')
                subindent = ' ' * 4 * (level + 1)
                # Uncomment below to include files in the tree:
                if print_files:
                    for f in files:
                        print(subindent+f+'\n')
