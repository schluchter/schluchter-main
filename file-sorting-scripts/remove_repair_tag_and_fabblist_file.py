import os

# get the current working dir
mainDir = os.getcwd()

for root, dirs, files in os.walk(mainDir):
    for name in files:
        #print(name)
        if name.find(" (repaired)"):
            fullPathOld = root + os.sep + name
            newName = name.replace(' (repaired)','')
            #newName = name - " (repaired)" Sorry, not in python :P
            fullPathNew = root + os.sep + newName
            os.rename(fullPathOld, fullPathNew)
        if name.endswith(".fabblist"):
        # DEBUG: only use this next line AFTER script is verified working!
        #if name.endswith((".fabblist",".py")):
            # build the full name
            fullPath = root + os.sep + name
            # delete name
            os.remove(fullPath)
