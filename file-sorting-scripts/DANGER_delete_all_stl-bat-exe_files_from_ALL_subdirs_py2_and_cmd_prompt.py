import os

print("Note: this script cannot be run directly from the terminal: must be from within IDE.")
print("Are you sure you want to run this script? Enter \"yes\" to run:")
#safety = input()
safety = raw_input()

if safety == "yes":
    # get the current working dir
    mainDir = os.getcwd()

    for root, dirs, files in os.walk(mainDir):
        for name in files:
            #print(name)
            if name.endswith((".stl", ".exe", ".bat")):
                # build the full name
                fullPath = root + os.sep + name
                # delete name
                os.remove(fullPath)
