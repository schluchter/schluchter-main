#!/usr/bin/python3

import os

cwd = os.getcwd()

folder_paths = []
folder_names = []
#files = []

# FULL FILE SYSTEM TREE
#for entry in os.scandir('/'):
for entry in os.scandir(cwd):
    if entry.is_dir():
        folder_paths.append(entry.path)
        folder_names.append(entry.name)
#    elif entry.is_file():
#        files.append(entry.path)
folder_names_sorted = sorted(folder_names)

fileout = open('directories.txt','w')
#fileout.write(indent+os.path.basename(root)+'\n')
for f in folder_names_sorted:
    fileout.write(f+'\n')
fileout.write("\n"+"--------END--------"+"\n")
