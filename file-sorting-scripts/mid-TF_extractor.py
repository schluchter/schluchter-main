# use shebang tag for python 2 or 3 as needed
import os

# dividing number
N = 3
# get the current working dir
mainDir = os.getcwd()

# flag for setting up folders initially
opening = 1

print("Extraction in progress...")

for root, dirs, files in os.walk(mainDir):
    #print("root: ",root)
    #print("dirs: ",dirs)
    # For all subdirs in mainDir, make a copy called "<subdir_name>_extracted"
    if opening == 1:
        dirs_list = dirs
        for folder in dirs:
            # make new folder in mainDir called "<folder_name>_extraction"
            if not os.path.exists(mainDir+os.sep+folder+"_extraction"):
                os.mkdir(mainDir+os.sep+folder+"_extraction")
            #print("folder: ")
            #print(folder)
            opening = 0

for folder in dirs_list:
    full_list = []
    for name in files:
        #print("name: ",name)
        full_list.append(name)
    n = len(full_list)
    #print("n: ",n)
    subset = n/N
    # naturally a float, change to int
    subset = int(subset)
    #print("subset number: ",subset)
    sorted_list = sorted(full_list)
    # extract the middle third of the list
    extraction_list = full_list[subset:(2*subset)]
    #print("extraction list: ",extraction_list)

    for eachfile in extraction_list:
        old_name = mainDir+os.sep+folder+os.sep+eachfile
        new_name = mainDir+os.sep+folder+"_extraction"+os.sep+eachfile
        #print("old_name: ",old_name)
        #print("new_name: ",new_name)
        os.rename(old_name, new_name)

print("Extraction complete.")
