import dicom, os

# get current working directory of script
mainDir = os.getcwd()
# get a list of all directories
dirs = [d for d in os.listdir(mainDir) if os.path.isdir(os.path.join(mainDir, d))]

appender = input("Append folders with the following string: ")

for folder in dirs:
    # rename directories: get the full path of the current, then make the new path. Then rename it!
    os.rename(os.path.join(mainDir, folder), os.path.join(mainDir, folder + '_' + appender))

