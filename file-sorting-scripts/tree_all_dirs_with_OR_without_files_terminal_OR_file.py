#!/usr/bin/python3

import os

#=======================================
# OPTIONS
#=======================================
# Include files?
include_files = False
# Print to terminal?
print_to_terminal = True
# Write to file?
write_to_file = True


def list_files_term(startpath):
    for root, dirs, files in os.walk(startpath):
        level = root.replace(startpath, '').count(os.sep)
        indent = ' ' * 4 * (level)
        print('{}{}'.format(indent, os.path.basename(root)))
        subindent = ' ' * 4 * (level + 1)
        if (include_files == True):
            for f in files:
                print('{}{}'.format(subindent, f))
                
def list_files_file(startpath):
    fileout = open('directory.txt','w')
    for root, dirs, files in os.walk(startpath):
        level = root.replace(startpath, '').count(os.sep)
        indent = ' ' * 4 * (level)
        fileout.write('{}{}'.format(indent, os.path.basename(root))+'\n')
        subindent = ' ' * 4 * (level + 1)
        if (include_files == True):
            for f in files:
                fileout.write('{}{}'.format(subindent, f)+'\n')


cwd = os.getcwd()

if write_to_file == True:
    list_files_file(cwd)

if print_to_terminal == True:
    list_files_term(cwd)
    press_key_to_exit = input()
