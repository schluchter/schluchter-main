import os

# get the current working dir
mainDir = os.getcwd()

for aDir in os.scandir(mainDir):
    # NOTE: "not entry.name.startswith('.')" is for UNIX-based systems (linux only?)
    # that also stops .DS_store files! Ha HA!
    if not aDir.name.startswith('.') and aDir.name != "new_directory" and aDir.is_dir():
        #print(aDir.name)

        for subdir in os.scandir(aDir):
            if subdir.is_dir():
                #print(subdir.name)

                for entry in os.scandir(subdir):
                    if not entry.name.startswith('.') and entry.name.endswith('.stl'):
                        print(entry.name)
                        # RENAME the STL to 4th&5th chars of 'subdir' (.stl)
                        fullPathOld = subdir.path + os.sep + entry.name
                        print(fullPathOld)
                        newName = subdir.name[3:5] + '.stl'
                        print(newName)
                        fullPathNew = subdir.path + os.sep + newName
                        os.rename(fullPathOld, fullPathNew)

