import os, folderstats
import pandas as pd
import numpy as np

#myPath = '/home/zero/Desktop/software/schluchter-repo/schluchter-main/path_max_char_lister'
print("Enter the full path of the dir you wish to map: ")
myPath = input()
print("Enter the max number of path chars: ")
num_chars = int(input())
print("Apply to dirs only? y/n?")
dirs_only = input()
print("Gathering data now...")

# Get all folder and file data and write to output
df = folderstats.folderstats(myPath, ignore_hidden=True)
df.to_csv(myPath+os.sep+'output_dirs_and_files.csv')

# Output all paths exceeding <num_chars> amount
if dirs_only=='n':
    path_list = []
    length_list = []
    for x in df.path:
        if len(x) > num_chars:
            #path_list.append("Length = "+str(len(x))+": "+x)
            length_list.append(len(x))
            path_list.append(x)
    path_header = 'path > '+str(num_chars)
    df_paths = pd.DataFrame({'length': length_list,path_header:path_list})
    df_paths.to_csv(myPath+os.sep+'output_paths_exceeding_x_chars.csv')

# Remove all files from df and write to output
df = df[df.folder]
df.drop('extension', axis=1, inplace=True)
df.drop('folder', axis=1, inplace=True)
df.to_csv(myPath+os.sep+'output_dirs.csv')

# Output all dir paths exceeding <num_chars> amount
if dirs_only=='y':
    path_list = []
    length_list = []
    for x in df.path:
        if len(x) > num_chars:
            #path_list.append("Length = "+str(len(x))+": "+x)
            length_list.append(len(x))
            path_list.append(x)
    path_header = 'path > '+str(num_chars)
    df_paths = pd.DataFrame({'length': length_list,path_header:path_list})
    df_paths.to_csv(myPath+os.sep+'output_dir_paths_exceeding_x_chars.csv')
    
print("Mapping completed.")

# Use this in terminal to gen. .csv of file structure
#folderstats pandas/ -p -i -v -o pandas.csv
# folderstats /home/zero/Desktop/software/schluchter-repo/schluchter-main/path_max_char_lister -p -i -v -o output.csv
