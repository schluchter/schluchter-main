# My first program in R Programming
#myString <- "Hello, World!"
#print (myString)

# INSTALLATION
# REMOVE -> ? #COMMENT OUT AFTER INSTALLATION #install.packages("ICC", dependencies = TRUE)
#COMMENT OUT AFTER INSTALLATION #install.packages("rio", dependencies = TRUE)
#COMMENT OUT AFTER INSTALLATION #install.packages("irr", dependencies = TRUE)

# Import all libs (csv/xlsx, ICC) and set all options
library("rio")
#library("irr")
#options(max.print=1000000)
options(max.print=100)

# Pull in all data
myFile <- read.csv("C:\\Users\\schluchter\\Desktop\\software and programming\\schluchter-repo.git\\R-scripts\\ICCs_for_R_columns.csv")
#myData <- import("C:\\Users\\schluchter\\Desktop\\software and programming\\schluchter-repo.git\\R-scripts\\ICCs_for_R_columns.csv")
#myData <- data.frame(myFile, stringsAsFactors = FALSE)

#print(myFile)

# Perform all ICC calculations

# Print to terminal

# (OPTIONAL) Save to file





# TESTING THE "custom" ICC FUNCTION

myFile <- read.csv("C:\\Users\\schluchter\\Desktop\\software and programming\\schluchter-repo.git\\R-scripts\\ICCs_for_R_columns.csv")
myData <- data.frame(myFile, stringsAsFactors = FALSE)
#data(myData)
irr::icc(myData, model="twoway", type="agreement")

r1 <- round(rnorm(20, 10, 4))
r2 <- round(r1 + 10 + rnorm(20, 0, 2))
r3 <- round(r1 + 20 + rnorm(20, 0, 2))
icc(cbind(r1, r2, r3), "twoway")              # High consistency
icc(cbind(r1, r2, r3), "twoway", "agreement") # Low agreement










#Example 1
#ICC::ICCest(x, y, data = NULL, alpha = 0.05, CI.type = c("THD", "Smith"))
data(ChickWeight)
print(ChickWeight)
ICC::ICCest(Chick, weight, data = ChickWeight, CI.type = "S")


print(myData)
#outData <- ICC::ICCest(observer, volume, data = myData, alpha = 0.05, CI.type = c("THD", "Smith"))
# NOTE: because observers are listed as numbers, MUST use as.factor 
#to NOT try to look at observers as numbers

ICC::ICCest(as.factor(trial), volume, data = myData, alpha = 0.05, CI.type = "S")
#outData <- ICC::ICCest(as.factor(trial), volume, data = myData, alpha = 0.05, CI.type = c("THD", "Smith"))
#print(outData[1])
#print(outData[2])
#print(outData[3])
#print(outData[4])
#print(outData[5])
#print(outData[6])
#print(outData[7])


# using subset function
newdata <- subset(mydata, age >= 20 | age < 10, select=c(ID, Weight)) 