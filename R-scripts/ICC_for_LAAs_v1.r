# INSTALLATION
#COMMENT OUT AFTER INSTALLATION #install.packages("rio", dependencies = TRUE)
#COMMENT OUT AFTER INSTALLATION #install.packages("irr", dependencies = TRUE)

# Import all libs (csv/xlsx, ICC) and set all options
#library("rio")
#library("irr")
#options(max.print=1000000)
options(max.print=1000)

# Pull in all data
#myFile <- read.csv("C:\\Users\\schluchter\\Desktop\\software and programming\\schluchter-repo.git\\R-scripts\\ICCs_for_R_columns.csv")
#myFile <- read.csv("C:\\Users\\schluchter\\Desktop\\software and programming\\schluchter-repo.git\\R-scripts\\ICCs_for_testing.csv")
myFile <- read.csv("C:\\Users\\schluchter\\Desktop\\software and programming\\schluchter-repo.git\\R-scripts\\ICCs_for_python.csv")

#myData <- import("C:\\Users\\schluchter\\Desktop\\software and programming\\schluchter-repo.git\\R-scripts\\ICCs_for_R_columns.csv")
myData <- data.frame(myFile, stringsAsFactors = FALSE)
#print(myData)

# Build data subsets as appropriate
#using subset function - WIP
#obs123t1 <- subset(myData, trial = 1, select=c(ID, Weight)) 
obs1t123 <- subset(myData, select=c(Observer1Trial1, Observer1Trial2, Observer1Trial3)) 
obs2t123 <- subset(myData, select=c(Observer2Trial1, Observer2Trial2, Observer2Trial3)) 
obs3t123 <- subset(myData, select=c(Observer3Trial1, Observer3Trial2, Observer3Trial3)) 
obs123t1 <- subset(myData, select=c(Observer1Trial1, Observer2Trial1, Observer3Trial1)) 
obs123t2 <- subset(myData, select=c(Observer1Trial2, Observer2Trial2, Observer3Trial2)) 
obs123t3 <- subset(myData, select=c(Observer1Trial3, Observer2Trial3, Observer3Trial3)) 

irr::icc(obs1t123, model="twoway", type="agreement")












#print(subset(myData, trial = 1, select=c(volume, observer, trial)) )
print(subset(myData, trial == "1", select=c(volume, observer, trial)) )

# Perform all ICC calculations
#irr::icc(myData, model="twoway", type="agreement")
#outData <- irr::icc(myData, model="twoway", type="agreement")
outData <- irr::icc(obs123t1, model="twoway", type="agreement")
print(outData[1])
print(outData[2])
print(outData[3])
print(outData[4])
print(outData[5])
print(outData[6])
print(outData[7])
print(outData[8])
print(outData[9])
print(outData[10])
print(outData[11])
print(outData[12])
print(outData[13])
print(outData[14])
print(outData[15])





# (OPTIONAL) Save to file


#outData <- ICC::ICCest(observer, volume, data = myData, alpha = 0.05, CI.type = c("THD", "Smith"))
# NOTE: because observers are listed as numbers, MUST use as.factor 
#to NOT try to look at observers as numbers
