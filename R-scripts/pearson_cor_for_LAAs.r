#options(max.print=1000000)
options(max.print=1000)

# Pull in all data
myFile <- read.csv("C:\\Users\\schluchter\\Desktop\\software and programming\\schluchter-repo.git\\R-scripts\\ICCs_for_python.csv")
#myFile <- read.csv("C:\\Users\\schluchter\\Desktop\\software and programming\\schluchter-repo.git\\R-scripts\\ICCs_for_testing.csv")

#myData <- import("C:\\Users\\schluchter\\Desktop\\software and programming\\schluchter-repo.git\\R-scripts\\ICCs_for_R_columns.csv")
myData <- data.frame(myFile, stringsAsFactors = FALSE)

print(myData)

cor.test(myData$Observer1Trial2, myData$Observer1Trial1)

#outData <- cor.test(myData$Observer1Trial2, myData$Observer1Trial1)
#print(outData[1])
#print(outData[2])
#print(outData[3])
#print(outData[4])
#print(outData[5])
#print(outData[6])
#print(outData[7])
#print(outData[8])
#print(outData[9])
#print(outData[10])
#print(outData[11])
#print(outData[12])
#print(outData[13])
#print(outData[14])
#print(outData[15])

# (OPTIONAL) Save to file

#outData <- ICC::ICCest(observer, volume, data = myData, alpha = 0.05, CI.type = c("THD", "Smith"))
# NOTE: because observers are listed as numbers, MUST use as.factor 
#to NOT try to look at observers as numbers
