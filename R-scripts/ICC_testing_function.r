# =========================== 1. PULL IN DATA AND CALCULATE ICC =========================== 
# Pull in all data
myFile <- read.csv("C:\\Users\\schluchter\\Desktop\\software and programming\\schluchter-repo.git\\R-scripts\\ICCs_in_columns_for_R_testing_3obs_9vol.csv")
myData <- data.frame(myFile, stringsAsFactors = FALSE)
# ICC for input data
totalICC <- irr::icc(myData, model="twoway", type="agreement", unit="single", conf.level=0.95)[7]
paste("Total ICC: ",totalICC)

# =========================== 2. DERIVE THE ICC STEP-BY-STEP =========================== 
ratings = myData
model="twoway"
type="agreement"
unit="single"
r0 = 0
conf.level=0.95

ratings <- as.matrix(na.omit(ratings))
#model <- match.arg(model)
#type <- match.arg(type)
#unit <- match.arg(unit)
alpha <- 1 - conf.level
ns <- nrow(ratings)
nr <- ncol(ratings)
SStotal <- var(as.numeric(ratings)) * (ns * nr - 1)
MSr <- var(apply(ratings, 1, mean)) * nr
paste("MSr values: ",MSr)

MSw <- sum(apply(ratings, 1, var)/ns)
MSc <- var(apply(ratings, 2, mean)) * ns
#paste("MSc: ",MSc)
MSe <- (SStotal - MSr * (ns - 1) - MSc * (nr - 1))/((ns - 1) * (nr - 1))

# REST OF CODE SHORTENED SIGNIFICANTLY FROM ORIGINAL
icc.name <- "ICC(A,1)"
coeff <- (MSr - MSe)/(MSr + (nr - 1) * MSe + (nr/ns) * (MSc - MSe))
a <- (nr * r0)/(ns * (1 - r0))
b <- 1 + (nr * r0 * (ns - 1))/(ns * (1 - r0))
Fvalue <- MSr/(a * MSc + b * MSe)
a <- (nr * coeff)/(ns * (1 - coeff))
b <- 1 + (nr * coeff * (ns - 1))/(ns * (1 - coeff))
v <- (a * MSc + b * MSe)^2/((a * MSc)^2/(nr - 1) + (b * MSe)^2/((ns - 1) * (nr - 1)))
df1 <- ns - 1
df2 <- v
p.value <- pf(Fvalue, df1, df2, lower.tail = FALSE)
FL <- qf(1 - alpha/2, ns - 1, v)
FU <- qf(1 - alpha/2, v, ns - 1)
lbound <- (ns * (MSr - FL * MSe))/(FL * (nr * MSc + (nr * ns - nr - ns) * MSe) + ns * MSr)
ubound <- (ns * (FU * MSr - MSe))/(nr * MSc + (nr * ns - nr - ns) * MSe + ns * FU * MSr)

rval <- structure(list(subjects = ns, raters = nr, model = model, 
                       type = type, unit = unit, icc.name = icc.name, value = coeff, 
                       r0 = r0, Fvalue = Fvalue, df1 = df1, df2 = df2, p.value = p.value, 
                       conf.level = conf.level, lbound = lbound, ubound = ubound), 
                  class = "icclist")

paste("Final ICC: ", rval[7])