# =========================== 1. PULL IN DATA AND CALCULATE ICC FIRST =========================== 
# Pull in all data
myFile <- read.csv("C:\\Users\\schluchter\\Desktop\\software and programming\\schluchter-repo.git\\R-scripts\\ICCs_in_columns_for_R_testing_3obs_9vol.csv")
#myFile <- read.csv("C:\\Users\\schluchter\\Desktop\\software and programming\\schluchter-repo.git\\R-scripts\\ICCs_for_R_columns.csv")
myData <- data.frame(myFile, stringsAsFactors = FALSE)
# ICC for input data
totalICC <- irr::icc(myData, model="twoway", type="agreement", unit="single", conf.level=0.95)[7]
paste("Total ICC: ",totalICC)

# =========================== 2. GRAPH THE ORIGINAL DATA =========================== 
# Break up data by observer:
obs1 <- subset(myData, select=c(Observer1Trial1))
obs2 <- subset(myData, select=c(Observer2Trial1))
obs3 <- subset(myData, select=c(Observer3Trial1))
# Plot data
x <- c(1:9)
plot(x, obs1[[1]], ylim=range(c(obs1[[1]],obs2[[1]],obs3[[1]])), xlab = "", ylab = "")
par(new=TRUE)
plot(x, obs2[[1]], ylim=range(c(obs1[[1]],obs2[[1]]),obs3[[1]]), axes = FALSE, xlab = "", ylab = "")
par(new=TRUE)
plot(x, obs3[[1]], ylim=range(c(obs1[[1]],obs2[[1]]),obs3[[1]]), axes = FALSE, xlab = "", ylab = "")
title(main="Volumes", ylab = "Volume (mL)", col.main="red", font.main=4)

# =========================== 3. DERIVE THE ICC STEP-BY-STEP =========================== 
# Insert volumes MANUALLY (as vectors):
obs1 <- c(1, 2, 3, 4, 5, 6, 7, 8, 9)
obs2 <- c(1, 2, 3, 4, 5, 6, 7, 8, 9)
obs3 <- c(1, 2, 3, 4, 5, 6, 7, 8, 9)
test_total <- data.frame(obs1)
test_total$obs2 <- obs2
test_total$obs3 <- obs3

# Override uploaded data with manual "data":
#myData <- test_total

# Build the ICC test
ratings = myData
model="twoway"
type="agreement"
unit="single"
r0 = 0
conf.level=0.95

ratings <- as.matrix(na.omit(ratings))
# graph the init values
#plot(ratings)

alpha <- 1 - conf.level
number_of_rows <- nrow(ratings)
number_of_cols <- ncol(ratings)



paste("var(as.numeric(ratings)): ",var(as.numeric(ratings)))



SStotal <- var(as.numeric(ratings)) * (number_of_rows * number_of_cols - 1)
MSr <- var(apply(ratings, 1, mean)) * number_of_cols
paste("MSr values: ",MSr)

MSw <- sum(apply(ratings, 1, var)/number_of_rows)
MSc <- var(apply(ratings, 2, mean)) * number_of_rows
#paste("MSc: ",MSc)
MSe <- (SStotal - MSr * (number_of_rows - 1) - MSc * (number_of_cols - 1))/((number_of_rows - 1) * (number_of_cols - 1))

# REST OF CODE SHORTENED SIGNIFICANTLY FROM ORIGINAL
icc.name <- "ICC(A,1)"
coeff <- (MSr - MSe)/(MSr + (number_of_cols - 1) * MSe + (number_of_cols/number_of_rows) * (MSc - MSe))
a <- (number_of_cols * r0)/(number_of_rows * (1 - r0))
b <- 1 + (number_of_cols * r0 * (number_of_rows - 1))/(number_of_rows * (1 - r0))
Fvalue <- MSr/(a * MSc + b * MSe)
a <- (number_of_cols * coeff)/(number_of_rows * (1 - coeff))
b <- 1 + (number_of_cols * coeff * (number_of_rows - 1))/(number_of_rows * (1 - coeff))
v <- (a * MSc + b * MSe)^2/((a * MSc)^2/(number_of_cols - 1) + (b * MSe)^2/((number_of_rows - 1) * (number_of_cols - 1)))
df1 <- number_of_rows - 1
df2 <- v
p.value <- pf(Fvalue, df1, df2, lower.tail = FALSE)
FL <- qf(1 - alpha/2, number_of_rows - 1, v)
FU <- qf(1 - alpha/2, v, number_of_rows - 1)
lbound <- (number_of_rows * (MSr - FL * MSe))/(FL * (number_of_cols * MSc + (number_of_cols * number_of_rows - number_of_cols - number_of_rows) * MSe) + number_of_rows * MSr)
ubound <- (number_of_rows * (FU * MSr - MSe))/(number_of_cols * MSc + (number_of_cols * number_of_rows - number_of_cols - number_of_rows) * MSe + number_of_rows * FU * MSr)

rval <- structure(list(subjects = number_of_rows, raters = number_of_cols, model = model, 
                       type = type, unit = unit, icc.name = icc.name, value = coeff, 
                       r0 = r0, Fvalue = Fvalue, df1 = df1, df2 = df2, p.value = p.value, 
                       conf.level = conf.level, lbound = lbound, ubound = ubound), 
                  class = "icclist")

paste("Final ICC: ", rval[7])