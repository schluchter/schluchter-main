# INSTALLATION
#COMMENT OUT AFTER INSTALLATION #install.packages("rio", dependencies = TRUE)
#COMMENT OUT AFTER INSTALLATION #install.packages("irr", dependencies = TRUE)

# Import all libs (csv/xlsx, irr(for ICC)), and set all options
#library("rio")
#library("irr")
#options(max.print=1000000)
options(max.print=1000)
#tidy = TRUE
#tidy.opts = list(comment = FALSE)

# Pull in all data
myFile <- read.csv("C:\\Users\\schluchter\\Desktop\\software and programming\\schluchter-repo.git\\R-scripts\\ICCs_for_python.csv")
myData <- data.frame(myFile, stringsAsFactors = FALSE)
#print(myData)
# Build data subsets as appropriate
obs1t123 <- subset(myData, select=c(Observer1Trial1, Observer1Trial2, Observer1Trial3)) 
obs2t123 <- subset(myData, select=c(Observer2Trial1, Observer2Trial2, Observer2Trial3)) 
obs3t123 <- subset(myData, select=c(Observer3Trial1, Observer3Trial2, Observer3Trial3)) 
obs123t1 <- subset(myData, select=c(Observer1Trial1, Observer2Trial1, Observer3Trial1)) 
obs123t2 <- subset(myData, select=c(Observer1Trial2, Observer2Trial2, Observer3Trial2)) 
obs123t3 <- subset(myData, select=c(Observer1Trial3, Observer2Trial3, Observer3Trial3))

# ICC for all data
# Use the following format for the ICC only:
#totalICC <- irr::icc(myData, model="twoway", type="agreement", unit="single", conf.level=0.95)[7]
totalICC <- irr::icc(myData, model="twoway", type="agreement", unit="single", conf.level=0.95)
paste("Total ICC: ",totalICC)

# ICC for intraobservers
obs1t123ICC <- irr::icc(obs1t123, model="twoway", type="agreement", unit="single", conf.level=0.95)
paste("Observer 1 ICC: ",obs1t123ICC)
obs2t123ICC <- irr::icc(obs2t123, model="twoway", type="agreement", unit="single", conf.level=0.95)
paste("Observer 2 ICC: ",obs2t123ICC)
obs3t123ICC <- irr::icc(obs3t123, model="twoway", type="agreement", unit="single", conf.level=0.95)
paste("Observer 3 ICC: ",obs3t123ICC)

# ICC for interobservers
obs123t1ICC <- irr::icc(obs123t1, model="twoway", type="agreement", unit="single", conf.level=0.95)
paste("Trial 1 ICC: ",obs123t1ICC)
obs123t2ICC <- irr::icc(obs123t2, model="twoway", type="agreement", unit="single", conf.level=0.95)
paste("Trial 2 ICC: ",obs123t2ICC)
obs123t3ICC <- irr::icc(obs123t3, model="twoway", type="agreement", unit="single", conf.level=0.95)
paste("Trial 3 ICC: ",obs123t3ICC)


# Perform all ICC calculations seperately
#outData <- irr::icc(myData, model="twoway", type="agreement", unit="single", conf.level=0.95)
#print(outData[1])
#print(outData[2])
#print(outData[3])
#print(outData[4])
#print(outData[5])
#print(outData[6])
#print(outData[7])
#print(outData[8])
#print(outData[9])
#print(outData[10])
#print(outData[11])
#print(outData[12])
#print(outData[13])
#print(outData[14])
#print(outData[15])

# (OPTIONAL) Save to file

#outData <- ICC::ICCest(observer, volume, data = myData, alpha = 0.05, CI.type = c("THD", "Smith"))
# NOTE: because observers are listed as numbers, MUST use as.factor 
#to NOT try to look at observers as numbers