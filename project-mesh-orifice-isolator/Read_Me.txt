Place mesh files in this directory. And "isolated" folder will emerge while running script.

In the future, if continued use of this script is expected, set up method to check for 
the word "solid" as the first word in each file. If it is not, then, after checking all 
.stl files, print error log showing which .stl files are not in ASCII form, and to 
reformat them.