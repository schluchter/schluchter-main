"""
Anscombe's quartet
==================

_thumb: .4, .4
"""
import os, pandas
cwd = os.getcwd()
# added line here:
#import matplotlib.pyplot as plt OR:
import matplotlib
# maybe try: matplotlib.pyplot.style.use('hollowMarkers')

import seaborn as sns
sns.set(style="ticks")

# Load the example dataset for Anscombe's quartet
# ORIGINAL DATA OF BELOW:
#df = sns.load_dataset("anscombe")
#df = pandas.read_csv('C:\Users\schluchter\Desktop\APRIL 04\testplotdata.csv')
df = pandas.read_csv(cwd+os.sep+'testplotdata.csv')
print(df)

# ORIGINAL DATA OF BELOW:
#sns.lmplot(x="x", y="y", col="dataset", hue="dataset", data=df,
#           col_wrap=2, ci=None, palette="muted", size=4,
#           scatter_kws={"s": 50, "alpha": 1}, markers="o")
sns.lmplot(x="Cardiac Phase (%)", y="Maximum LAA Volume (mL)", col="Patient", hue="Patient", data=df,
           col_wrap=1, ci=None, palette="muted", size=8,
           scatter_kws={"s": 60, "alpha": .6}, markers="_")

# and added line here:
#plt.show() OR:
matplotlib.pyplot.show()
print("done")
