# added line here:
#import matplotlib.pyplot as plt OR:
import matplotlib


#====START ORIGINAL CODE=============================================#

import seaborn as sns
sns.set(style="ticks")

# Load the example dataset for Anscombe's quartet
df = sns.load_dataset("anscombe")

# Show the results of a linear regression within each dataset
sns.lmplot(x="x", y="y", col="dataset", hue="dataset", data=df,
           col_wrap=2, ci=None, palette="muted", size=4,
           scatter_kws={"s": 50, "alpha": 1})

#====END ORIGINAL CODE=============================================#

# and added line here:
#plt.show() OR:
matplotlib.pyplot.show()

import os
print(os.path(df))
