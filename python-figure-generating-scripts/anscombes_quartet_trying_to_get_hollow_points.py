"""
Anscombe's quartet
==================

_thumb: .4, .4
"""
# added line here:
#import matplotlib.pyplot as plt OR:
import matplotlib
# maybe try: matplotlib.pyplot.style.use('hollowMarkers')


import matplotlib.pyplot as plt
IPython_default = plt.rcParams.copy()

'''
# This is cool, but seems to be overridden by seaborn
from matplotlib import cycler
colors = cycler('color',
                ['#EE6666', '#3388BB', '#9988DD',
                 '#EECC55', '#88BB44', '#FFBBBB'])
#plt.rc('axes', facecolor='#E6E6E6', edgecolor='none',
plt.rc('axes', facecolor='none', edgecolor='none',
       axisbelow=True, grid=True, prop_cycle=colors)
plt.rc('grid', color='w', linestyle='solid')
plt.rc('xtick', direction='out', color='gray')
plt.rc('ytick', direction='out', color='gray')
plt.rc('patch', edgecolor='#E6E6E6')
plt.rc('lines', linewidth=2)
'''







import seaborn as sns
sns.set(style="ticks")

# Load the example dataset for Anscombe's quartet
df = sns.load_dataset("anscombe")
#print(df)


# WIP
#matplotlib.plot(facecolors='none')
#matplotlib.markers(facecolors='none')
#matplotlib.markers.MarkerStyle(facecolors='none')
#matplotlib.markers.MarkerStyle(fillstyle='none')
#matplotlib.pyplot.plot(facecolors='none')


# Show the results of a linear regression within each dataset
#sns.lmplot(x="x", y="y", col="dataset", hue="dataset", data=df,
#           col_wrap=2, ci=None, palette="muted", size=4,
#           scatter_kws={"s": 50, "alpha": 1}, markers="o")
sns.lmplot(x="x", y="y", col="dataset", hue="dataset", data=df,
           col_wrap=2, ci=None, palette="muted", size=4,
           scatter_kws={"s": 10, "alpha": .8}, markers="_")



# and added line here:
#plt.show() OR:
matplotlib.pyplot.show()


'''
Optional kwargs control the Collection properties; in particular:

    edgecolors:
        The string ‘none’ to plot faces with no outlines
    facecolors:
        The string ‘none’ to plot unfilled outlines

Try the following:
'''
import matplotlib.pyplot as plt
import numpy as np

x = np.random.randn(60)
y = np.random.randn(60)

plt.scatter(x, y, s=80, facecolors='none', edgecolors='r')
plt.show()




print("done")
# NEED TO FIND A WAY TO ADD IN (as an attribute of matplotlib):
# , facecolors='none'
