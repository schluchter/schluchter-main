import seaborn as sns
import os, pandas
import matplotlib.pyplot as plt
import csv

cwd = os.getcwd()
df = pandas.read_csv(cwd+os.sep+'data'+os.sep+'figuretestdata.csv')
print(df)
figure = sns.factorplot(x="Cardiac Phase (%)", y="Observed LAA Volume (mL)", hue="Patient", data=df, capsize=0,
                   palette="muted", size=6, aspect=1, markers=['o','s','d'], scale=.5, legend=False, hue_order=[10,13,1])
plt.legend(loc="best", edgecolor='None', frameon=True, title="Patient", markerscale=2, prop={'size': 10})
lw = figure.ax.lines[0].get_linewidth() # lw of first line
plt.setp(figure.ax.lines,linewidth=(lw-0.5))  # set lw for all lines of g axes
figure.ax.set_title("   Figure 2. Example LAA Volumes by Cardiac Phase", loc="left", fontsize=10)
plt.gcf().subplots_adjust(top=.92)
my_desktop = r"C:\Users\schluchter\Desktop"
#plt.savefig(my_desktop+os.sep+"output.pdf")
plt.show()
