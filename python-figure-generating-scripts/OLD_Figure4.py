import seaborn as sns
import os, pandas
import matplotlib.pyplot as plt
import csv
import numpy as np

# 1. ========================== GET THE DATA ==========================
cwd = os.getcwd()
df = pandas.read_csv(cwd+os.sep+'data'+os.sep+'figure4data_test.csv', nrows=40)
print(df)

#df2.loc[startrow:endrow, startcolumn:endcolumn]
x_axis = df.loc[: , "x"]
y_axis = df.loc[: , "y"]
df2 = df.set_index("type", drop = False)
x1_axis = df2.loc[ "Intra", "x"]
print(x1_axis)
y1_axis = df2.loc[ "Intra", "y"]
print(y1_axis)
x2_axis = df2.loc[ "Inter", "x"]
print(x2_axis)
y2_axis = df2.loc[ "Inter", "y"]
print(y2_axis)
y1r_axis = df2.loc[ "Intra", "y2"]
print(y1r_axis)
y2r_axis = df2.loc[ "Inter", "y2"]
print(y2r_axis)

"""
fig = plt.figure()
ax = fig.add_subplot(111)
ax.set_ylim(1, 40)
ax2 = fig.add_subplot(111)
ax2.set_ylim(1, 40)
"""
#ax.plot(x1_axis, y1_axis, color='blue', linewidth=3)
#ax2.plot(x2_axis, y2_axis, color='red', linewidth=3)

fig = plt.figure()

#plt.plot(x1_axis, y1_axis, 'r', ax=ax)


# and the first axes using subplot populated with data
ax1 = fig.add_subplot(111)
line1 = ax1.plot([1,3,4,5,2], 'o-')
plt.ylabel("Left Y-Axis Data")
# now, the second axes that shares the x-axis with the ax1
ax2 = fig.add_subplot(111, sharex=ax1, frameon=False)
line2 = ax2.plot([10,40,20,30,50], 'xr-')
ax2.yaxis.tick_right()
ax2.yaxis.set_label_position("right")
plt.ylabel("Right Y-Axis Data")















#plt.plot(x2_axis, y2_axis, 'b')
#ax2 = fig.add_subplot(111, sharex=fig, frameon=False)
#ax2.yaxis.tick_right()
#ax2.yaxis.set_label_position("right")
#plt.plot(x1_axis, y1r_axis, 'r')
#plt.plot(x2_axis, y2r_axis, 'b')


#ax2 = fig1.add_subplot(111, sharex=ax1, frameon=False)
#line2 = ax2.plot([10,40,20,30,50], 'xr-')
#ax2.yaxis.tick_right()
#ax2.yaxis.set_label_position("right")
#ylabel("Right Y-Axis Data")



#plt.xticks(x1_axis, x1_axis, rotation=45, fontsize=6)



# for the legend, remember that we used two different axes so, we need
# to build the legend manually
#legend((line1, line2), ("1", "2"))



# 5. ========================== 5. SAVE PLOT ==========================
# Save the file to the Desktop (well, MY desktop, in this case)
# Note: need the 'r' in front, to escape the escape char's!
my_desktop = r"C:\Users\schluchter\Desktop"
plt.savefig(my_desktop+os.sep+"output.pdf")
#Save transparent figure
#plt.savefig(my_desktop+os.sep+'output_transparent.pdf', transparent=True)

# ========================== 6. SHOW PLOT ==========================
plt.show()
