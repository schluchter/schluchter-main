import matplotlib.pyplot
import numpy as np
import os, pandas
import csv

def bland_altman_plot(data1, data2, *args, **kwargs):
    data1     = np.asarray(data1)
    data2     = np.asarray(data2)
    mean      = np.mean([data1, data2], axis=0)
    #print("data1: ",data1,'\n')
    #print("mean: ",mean,'\n')
    #print("data2: ",data2,'\n')
    diff      = data1 - data2                   # Difference between data1 and data2
    md        = np.mean(diff)                   # Mean of the difference
    sd        = np.std(diff, axis=0)            # Standard deviation of the difference

    matplotlib.pyplot.scatter(mean, diff, *args, **kwargs)
    matplotlib.pyplot.axhline(md,           color='gray', linestyle='--')
    matplotlib.pyplot.axhline(md + 1.96*sd, color='gray', linestyle='--')
    matplotlib.pyplot.axhline(md - 1.96*sd, color='gray', linestyle='--')

cwd = os.getcwd()
df = pandas.read_csv(cwd+os.sep+'python-figure-generating-scripts'+os.sep+'data'+os.sep+'ICCs_for_python.csv')#, nrows=40)
ax1="andrew trial 1"
ax2="andrew trial 3"

mydata1 = df[ax1]
mydata2 = df[ax2]

#graph.set_axis_labels(ax1,ax2)
bland_altman_plot(mydata1, mydata2, s=3)#scatter_kws={"s": 10})
matplotlib.pyplot.title('Bland-Altman Plot')
matplotlib.pyplot.title(ax1+" vs "+ax2)
matplotlib.pyplot.show()
