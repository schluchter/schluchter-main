import matplotlib.pyplot as plt
import numpy as np
#import seaborn # as sns
import os, pandas
import csv

def bland_altman_plot(data1, data2, *args, **kwargs):
    data1     = np.asarray(data1)
    data2     = np.asarray(data2)
    mean      = np.mean([data1, data2], axis=0)
    diff      = data1 - data2                   # Difference between data1 and data2
    md        = np.mean(diff)                   # Mean of the difference
    sd        = np.std(diff, axis=0)            # Standard deviation of the difference

    plt.scatter(mean, diff, *args, **kwargs)
    plt.axhline(md,           color='gray', linestyle='--')
    plt.axhline(md + 1.96*sd, color='gray', linestyle='--')
    plt.axhline(md - 1.96*sd, color='gray', linestyle='--')


cwd = os.getcwd()
df = pandas.read_csv(cwd+os.sep+'python-figure-generating-scripts'+os.sep+'data'+os.sep+'ICCs_for_python.csv')#, nrows=40)
ax1="andrew trial 1"
ax2="andrew trial 3"
graph = seaborn.lmplot(x=ax1, y=ax2, x_ci='ci', ci=0, line_kws={"linewidth": 1}, scatter_kws={"s": 10},
               truncate=True, size=6, aspect=1, data=df)#, fit_reg=False)

# Use more informative axis labels than are provided by default
graph.set_axis_labels(ax1,ax2)
matplotlib.pyplot.title(ax1+" vs "+ax2)
matplotlib.pyplot.show()

#The corresponding elements in data1 and data2 are used to calculate the coordinates for the plotted points.
#Then you can create a plot by running e.g.

#from numpy.random import random
# Example, usin random data:
#bland_altman_plot(random(10), random(10))
bland_altman_plot(ax1, ax2)
plt.title('Bland-Altman Plot')
plt.show()
