import os, pandas
import matplotlib.pyplot as plt
import seaborn as sns

#sns.set(style="ticks")
cwd = os.getcwd()

# Load the dataset
#df = sns.load_dataset("anscombe")
df = pandas.read_csv(cwd+os.sep+'data'+os.sep+'testplotdata.csv')
print(df)

# Set up the plot
sns.lmplot(x="Cardiac Phase (%)", y="Observed LAA Volume (mL)", col="Patient", hue="Patient", data=df,
           col_wrap=1, ci=None, palette="muted", size=8,
           scatter_kws={"s": 60, "alpha": .6}, markers="_")

# Show the result
plt.show()
print("done")
