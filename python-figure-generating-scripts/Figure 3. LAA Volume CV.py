import seaborn as sns
import os, pandas
import matplotlib.pyplot as plt
import csv

cwd = os.getcwd()

# Set the style, if desired
sns.set(style="whitegrid")
#sns.set(style="ticks")

# Set context, if desired
#paper_rc = {'lines.linewidth': 1}#, 'lines.markersize': 10}
#sns.set_context("paper", rc = paper_rc)

# Load the dataset
#df = pandas.read_csv(cwd+os.sep+'data'+os.sep+'figure3data.csv')
df = pandas.read_csv(cwd+os.sep+'data'+os.sep+'figure3data.csv')
print(df)

# Set up the plot
# Includes REMOVING the auto-legend, so that I may define it explicitly below
figure = sns.factorplot(x="Cardiac Phase (%)", y="LAA CV (%)", data=df, capsize=0,
                   palette="muted", size=6, aspect=1, markers=['o'], scale=1, legend=False)

# Generate custom legend
#plt.legend(loc="best", edgecolor='None', frameon=True, title="Patient", markerscale=2, prop={'size': 10})

# Fix those damned error bars
lw = figure.ax.lines[0].get_linewidth() # lw of first line
plt.setp(figure.ax.lines,linewidth=(lw-0.5))  # set lw for all lines of g axes
# Alternatively, set the widths manually

# Title it
#plt.title('add title here')
figure.ax.set_title("   Figure 3. LAA Coefficient of Variation (CV) by Cardiac Phase", loc="left", fontsize=12)
#plt.suptitle("add title here", fontsize=16, fontweight="bold")

# Adjust the layout to fit it
#plt.tight_layout()
plt.gcf().subplots_adjust(top=.92)

# Despine it, since it doesn't start at 0!
#figure.despine(left=True)
# OR
# Set the minimum y value to 0, and max to 10 [best setting to compare SDs as well as means]
figure.ax.set_ylim(ymin=0)
figure.ax.set_ylim(ymax=10)

# Save the file to the Desktop (well, MY desktop, in this case)
# Note: need the 'r' in front, to escape the escape char's!
my_desktop = r"C:\Users\schluchter\Desktop"
#figure.savefig(my_desktop+os.sep+"output.pdf")
plt.savefig(my_desktop+os.sep+"output.pdf")

# Show the result
plt.show()
#print("done")
