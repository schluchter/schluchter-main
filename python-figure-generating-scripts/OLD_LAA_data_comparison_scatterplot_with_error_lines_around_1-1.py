import seaborn # as sns
import matplotlib.pyplot # as plt
import os, pandas
import csv
import numpy as np
import math

# Take in and set up the data
seaborn.set()
cwd = os.getcwd()
df = pandas.read_csv(cwd+os.sep+'python-figure-generating-scripts'+os.sep+'data'+os.sep+'ICCs_for_python.csv')#, nrows=40)
#print(df)

ax1="andrew trial 1"
ax2="andrew trial 2"

graph = seaborn.lmplot(x=ax1, y=ax2, x_ci='ci', ci=0, line_kws={"linewidth": 1}, scatter_kws={"s": 10},
               truncate=True, size=7, aspect=1, data=df, fit_reg=False)
# Set up data for making error lines
mydata1 = df[ax1]
mydata2 = df[ax2]
data1     = np.asarray(mydata1)
data2     = np.asarray(mydata2)
mean      = np.mean([data1, data2], axis=0)
#print("data1: ",data1,'\n')
#print("mean: ",mean,'\n')
#print("data2: ",data2,'\n')
diff      = data1 - data2                   # Difference between data1 and data2
md        = np.mean(diff)                   # Mean of the difference
sd        = np.std(diff, axis=0)            # Standard deviation of the difference

def graph_error_bars(formula, x_range):
    x = np.array(x_range)
    y = eval(formula)
    matplotlib.pyplot.plot(x, y, color='gray', linewidth=1, linestyle='--')
def graph_correlation_line(formula, x_range):
    x = np.array(x_range)
    y = eval(formula)
    matplotlib.pyplot.plot(x, y, color='black', linewidth=1, linestyle=':')

maxnum = int(math.ceil(max(max(data1), max(data2))))
#print(maxnum)
#minnum = int(round(min(min(data1), min(data2))))
# Data from Excel! Formula for AT1 vs AT2: y = 0.9567x + 0.3594
# EXAMPLE OF ERROR BAR: formula_high = 'x+1.96*sd'
#formula_high = '0.9567*x + 0.3594 + 1.96*sd'
#formula_low = '0.9567*x + 0.3594 - 1.96*sd'
formula_high = 'x + 1.96*sd'
formula_low = 'x - 1.96*sd'
#md + 1.96*sd
graph_error_bars(formula_high, range(1, maxnum))
graph_error_bars(formula_low, range(1, maxnum))
graph_correlation_line('x', range(1, maxnum))

# Label everything and show it
graph.set_axis_labels(ax1,ax2)
matplotlib.pyplot.title(ax1+" vs "+ax2)
matplotlib.pyplot.tight_layout()
matplotlib.pyplot.show()
