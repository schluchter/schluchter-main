import seaborn as sns
import os, pandas
import matplotlib.pyplot as plt
import csv
import numpy as np

cwd = os.getcwd()

#df = pandas.read_csv(cwd+os.sep+'data'+os.sep+'figure4data_test.csv')
#df = pandas.DataFrame({"a": np.random.random(100), "b": np.random.random(100), "id": np.arange(100)})
#print(df)

df = pandas.read_csv(cwd+os.sep+'data'+os.sep+'figure4data_test.csv', nrows=40)
#print(df)

#df2.loc[startrow:endrow, startcolumn:endcolumn]
x_axis = df.loc[: , "x"]
y_axis = df.loc[: , "y"]
df2 = df.set_index("type", drop = False)
x1_axis = df2.loc[ "Intra", "x"]
print(x1_axis)
y1_axis = df2.loc[ "Intra", "y"]
print(y1_axis)
x2_axis = df2.loc[ "Inter", "x"]
print(x2_axis)
y2_axis = df2.loc[ "Inter", "y"]
print(y2_axis)

# width of the bars
barWidth = 0.3
"""
# The x position of bars
r1 = np.arange(len(y1_axis))
r2 = [x + barWidth for x in r1]
# Create blue bars
plt.bar(r1, y1_axis, width = barWidth, color = 'blue', edgecolor = 'black', capsize=7, label='Intra')
# Create cyan bars
plt.bar(r2, y2_axis, width = barWidth, color = 'cyan', edgecolor = 'black', capsize=7, label='Inter')
"""

r1 = np.arange(len(y1_axis))
print(r1)
r2 = [x + barWidth for x in r1]
print(r2)
# Create blue bars
plt.bar(r1, y1_axis, width = barWidth, color = 'blue', edgecolor = 'black', capsize=7, label='Intra')
plt.bar(r2, y2_axis, width = barWidth, color = 'cyan', edgecolor = 'black', capsize=7, label='Inter')


# Add title and axis names
plt.title('My title')
plt.xlabel('categories')
plt.ylabel('values')

# Limits for the Y axis
plt.ylim(0,50)

# Create names
#plt.xticks(rotation=70)
#plt.xticks(y_axis, x_axis, rotation=45, fontsize=6)
#plt.xticks([r + barWidth for r in range(len(bars1))], ['cond_A', 'cond_B', 'cond_C'])
#plt.xticks([r + barWidth for r in range(len(bars1))], x_axis, rotation=45, fontsize=6)

plt.legend(loc="best", edgecolor='None', frameon=True, title="Observation", markerscale=2, prop={'size': 10})
# general layout
#plt.xticks([r + barWidth for r in range(len(y_axis))], ['cond_A', 'cond_B', 'cond_C'])

# Show graphic
plt.show()
