import seaborn as sns
import os, pandas
import matplotlib.pyplot as plt
import csv
import numpy as np

cwd = os.getcwd()

# Get the data
df = pandas.read_csv(cwd+os.sep+'data'+os.sep+'figure4data_test.csv', nrows=40)
print(df)

# Make the plot
#fig, ax = plt.subplots()
#sns.factorplot(x='x', y='y', hue='type', data=df, kind='bar', legend=False, size=5, aspect=1.5, ax=ax)
#ax2 = ax.twinx()
#sns.factorplot(x='x', y='y2', hue='type', data=df, kind='bar', legend=False, size=5, aspect=1.5, ax=ax2)

#fig, ax = plt.subplots()
#ax.plot(np.random.rand(10))
color1 = sns.color_palette("muted", 2)
color2 = sns.color_palette("muted", 4)
sns.factorplot(x='x', y='y', hue='type', data=df, kind='bar', legend=False, size=5, aspect=1.5, palette=color1)
#sns.factorplot(x='x', y='y', hue='type', data=df, kind='bar', legend=False, size=5, aspect=1.5, palette='muted')
plt.title('Figure 4. Intraclass Correlation Coefficients for Intra and Inter-observations')
plt.xlabel('Intraclass Correlation Coefficient (ICC) Values')
plt.xticks(rotation=45, fontsize=7)
plt.ylabel('Number of ICC Datapoints')
plt.legend(loc="best", edgecolor='None', frameon=True, title="Observation", markerscale=2, prop={'size': 10})
#ax2 =ax.twinx()
#ax2.plot(100*np.random.rand(10))
sns.factorplot(x='x', y='y2', hue='type', data=df, kind='point', markers="None", legend=False, size=5, aspect=1.5, palette=color2)
plt.title('Figure 4. Intraclass Correlation Coefficients for Intra and Inter-observations')
plt.xlabel('Intraclass Correlation Coefficient (ICC) Values')
plt.xticks(rotation=45, fontsize=7)
plt.ylabel('Number of ICC Datapoints')
#plt.legend(loc="best", edgecolor='None', frameon=True, title="Observation", markerscale=2, prop={'size': 10})

"""
fig, ax = plt.subplots()
    sb.regplot(x='round', y='money', data=firm, ax=ax)
    ax2 = ax.twinx()
    sb.regplot(x='round', y='dead', data=firm, ax=ax2, color='r')
    sb.plt.show()
"""


# Show graphic
plt.show()
