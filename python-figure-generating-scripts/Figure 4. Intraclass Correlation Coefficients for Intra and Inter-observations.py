import seaborn as sns
import os, pandas
import matplotlib.pyplot as plt
import csv
import numpy as np

# 1. ========================== GET THE DATA ==========================
cwd = os.getcwd()
df = pandas.read_csv(cwd+os.sep+'data'+os.sep+'figure4data_test2.csv', nrows=40)
print(df)

#df2.loc[startrow:endrow, startcolumn:endcolumn]
x_axis = df.loc[: , "x"]
y_axis = df.loc[: , "y"]
df2 = df.set_index("type", drop = False)
x1_axis = df2.loc[ "Intra", "x"]
print(x1_axis)
y1_axis = df2.loc[ "Intra", "y"]
print(y1_axis)
x2_axis = df2.loc[ "Inter", "x"]
print(x2_axis)
y2_axis = df2.loc[ "Inter", "y"]
print(y2_axis)
y1r_axis = df2.loc[ "Intra", "y2"]
print(y1r_axis)
y2r_axis = df2.loc[ "Inter", "y2"]
print(y2r_axis)

"""
fig = plt.figure()
ax = fig.add_subplot(111)
ax.set_ylim(1, 40)
ax2 = fig.add_subplot(111)
ax2.set_ylim(1, 40)
"""
#ax.plot(x1_axis, y1_axis, color='blue', linewidth=3)
#ax2.plot(x2_axis, y2_axis, color='red', linewidth=3)

fig = plt.figure()

#plt.plot(x1_axis, y1_axis, 'r', ax=ax)

# and the first axes using subplot populated with data
ax1 = fig.add_subplot(111)
bargraph1 = ax1.bar(x1_axis-.01, y1_axis, width=0.02, color='blue', align='center') # , 'og-'
ax1.xaxis.set_visible(False) # same for y axis.
bargraph2 = ax1.bar(x1_axis+.01, y2_axis, width=0.02, color='green', align='center') # , 'og-'
ax1.yaxis.tick_left()
ax1.yaxis.set_label_position("left")
plt.ylabel("Number of ICC Datapoints")

#plt.ylabel("Left Y-Axis Data")
# now, the second axes that shares the x-axis with the ax1
ax2 = fig.add_subplot(111, sharex=ax1, frameon=False, )
linegraph1 = ax2.plot(x1_axis, y1r_axis, 'darkblue')
linegraph2 = ax2.plot(x1_axis, y2r_axis, 'darkgreen')
ax2.yaxis.tick_right()
ax2.yaxis.set_label_position("right")
plt.ylabel(r"% of ICC Datapoints of Higher Quality")


#ax1.set_ylim(1, 50) # PROBLEM: SHRINKS DOWN Y-VALUES TOO LOW
ax2.set_ylim(1, 105)
plt.xticks(x1_axis, x1_axis, rotation=40, fontsize=7)
plt.xlabel('Intraclass Correlation Coefficient (ICC) Values')
plt.title('Figure 4. Intraclass Correlation Coefficients for Intra and Inter-observations')

# for the legend, remember that we used two different axes so, we need
# to build the legend manually
#plt.legend((ax1, ax2), ("1", "2"))
#ax2.legend(loc='best') # No overlapping plot elements



# 5. ========================== 5. SAVE PLOT ==========================
# Save the file to the Desktop (well, MY desktop, in this case)
# Note: need the 'r' in front, to escape the escape char's!
my_desktop = r"C:\Users\schluchter\Desktop"
plt.savefig(my_desktop+os.sep+"output.pdf")
#Save transparent figure
#plt.savefig(my_desktop+os.sep+'output_transparent.pdf', transparent=True)

# ========================== 6. SHOW PLOT ==========================
plt.show()
