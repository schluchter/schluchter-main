import seaborn # as sns
import matplotlib.pyplot # as plt
import os, pandas
import csv

# Take in and set up the data
seaborn.set()
cwd = os.getcwd()
df = pandas.read_csv(cwd+os.sep+'python-figure-generating-scripts'+os.sep+'data'+os.sep+'ICCs_for_python.csv')#, nrows=40)
#print(df)
"""
# EXAMPLES
ax1="<DATA 1>"
ax2="<DATA 2>"
graph = seaborn.lmplot(x="andrew trial 1", y="andrew trial 2", x_ci='ci', ci=95,
               truncate=True, size=5, data=df)
"""
ax1="andrew trial 1"
ax2="andrew trial 3"
#graph_dims = (6, 6) # figsize=graph_dims, ...
#graph = seaborn.lmplot(x=ax1, y=ax2, x_ci='ci', ci=95,
#               truncate=True, size=5, data=df)
graph = seaborn.lmplot(x=ax1, y=ax2, x_ci='ci', ci=0, line_kws={"linewidth": 1}, scatter_kws={"s": 10},
               truncate=True, size=6, aspect=1, data=df)#, fit_reg=False)

# Use more informative axis labels than are provided by default
graph.set_axis_labels(ax1,ax2)
matplotlib.pyplot.title(ax1+" vs "+ax2)
matplotlib.pyplot.show()
