import seaborn as sns
import os, pandas
import matplotlib.pyplot as plt
import csv

cwd = os.getcwd()

# Set the style, if desired
#sns.set(style="whitegrid")
#sns.set(style="ticks")

# Set context, if desired
#paper_rc = {'lines.linewidth': 1}#, 'lines.markersize': 10}
#sns.set_context("paper", rc = paper_rc)

# Load the dataset
df = pandas.read_csv(cwd+os.sep+'data'+os.sep+'figure2data.csv')
print(df)

# Set up the plot
# Includes REMOVING the auto-legend, so that I may define it explicitly below
figure = sns.factorplot(x="Cardiac Phase (%)", y="Observed LAA Volume (mL)", hue="Patient", data=df, capsize=0,
                   palette="muted", size=6, aspect=1, markers=['o','s','d'], scale=.5, legend=False, hue_order=[10,13,1])

# Generate custom legend
#plt.legend(loc="best", edgecolor='None', frameon=True, title="Patient", markerscale=2, prop={'size': 10})

# Fix those damned error bars
lw = figure.ax.lines[0].get_linewidth() # lw of first line
plt.setp(figure.ax.lines,linewidth=(lw-0.5))  # set lw for all lines of g axes
# Alternatively, set the widths manually

# Title it
#plt.title('add title here')
figure.ax.set_title("   Figure 2. Example LAA Volumes by Cardiac Phase", loc="left", fontsize=10)
#plt.suptitle("add title here", fontsize=16, fontweight="bold")

# Adjust the layout to fit it
#plt.tight_layout()
plt.gcf().subplots_adjust(top=.92)

# Save the file to the Desktop (well, MY desktop, in this case)
# Note: need the 'r' in front, to escape the escape char's!
my_desktop = r"C:\Users\schluchter\Desktop"
#figure.savefig(my_desktop+os.sep+"output.pdf")
plt.savefig(my_desktop+os.sep+"output.pdf")

# Show the result
plt.show()
#print("done")
